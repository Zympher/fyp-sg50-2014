﻿using UnityEngine;
using System.Collections;

public class NewStallPowerup : MonoBehaviour {

    public PointsCarry pointsCarry;
    public bool activated = false;
    public bool displaySkill = false;

    IEnumerator Active()
    {
        GameObject go = GameObject.Find("PowerupShop");
        PowerupShop powerupShop = go.GetComponent<PowerupShop>();

        activated = true;

        return true;
    }

	// Use this for initialization
	void Start () 
    {
        
	}
	
	// Update is called once per frame
	void Update () 
    {
        GameObject go = GameObject.Find("PowerupShop");
        GameObject go2 = GameObject.Find("PointsCarrying");
        GameObject go3 = GameObject.Find("ShiokBarUI");

        PowerupShop powerupShop = go.GetComponent<PowerupShop>();
        PointsCarry moneyNow = go2.GetComponent<PointsCarry>();
        ShiokBar shiokBar = go3.GetComponent<ShiokBar>();

        if (shiokBar.lvl == 2 && shiokBar.buttonOpen && moneyNow.money >= 100)//powerupShop.
        {
            collider.enabled = true;
        }
        else
        {
            collider.enabled = false;
        }

        //if (displaySkill == true)
        //{
        //    //StartCorouti
        //}
	}

    void OnMouseDown()
    {
        GameObject go = GameObject.Find("PowerupShop");
        PowerupShop powerupShop = go.GetComponent<PowerupShop>();

        StartCoroutine(Active());

        collider.enabled = false;


    }
}
