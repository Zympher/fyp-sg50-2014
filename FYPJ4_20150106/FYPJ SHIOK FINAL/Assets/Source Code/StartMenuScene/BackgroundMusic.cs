﻿using UnityEngine;
using System.Collections;

public class BackgroundMusic : MonoBehaviour 
{
	void Awake () 
	{
		GameObject gameMusic = GameObject.Find ("GameMusic");
		if(gameMusic)
		{
			// kill game music
			Destroy(gameMusic.gameObject);
		}

		DontDestroyOnLoad (gameObject);
	}

}