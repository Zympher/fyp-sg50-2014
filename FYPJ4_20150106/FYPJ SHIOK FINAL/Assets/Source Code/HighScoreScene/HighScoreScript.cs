﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


//using Facebook.MiniJSON;
//#define DEBUG

public class HighScoreScript : MonoBehaviour
{
    //[SerializeField]
    private const int highscore_list = 10;
    //private PointsCarry pp;

    [SerializeField]
    private Texture first_Texture;
    [SerializeField]
    private Texture second_Texture;
    [SerializeField]
    private Texture third_Texture;



    [SerializeField]
    public List<float> money_list = new List<float>(10);

    [SerializeField]
    public List<float> point_list = new List<float>(10);

    [SerializeField]
    public List<string> point_name_list = new List<string>(10);

    [SerializeField]
    public List<string> money_name_list = new List<string>(10);

    private string name;
    //current highscore
    private float points;

    //current highcurrency
    private float monies;


    public float test_money;
    public float test_points;


    //private bool m_bPrint = false;
    //private bool m_bMStringSave = false;
    private bool m_bPTStringSave = false;

    private Vector2 scrollPosition;

    public const int max_text_length = 15;

    [SerializeField]
    private Rect pointlistRect = new Rect(40, 143, 420, 325);
    [SerializeField]
    private Rect moneylistRect = new Rect(540, 143, 420, 325);



    public Vector2 moneylistVscroll = Vector2.zero;
    public Vector2 pointlistVscroll = Vector2.zero;



    public Vector2 startTouch = Vector2.zero;



    public PlayerMapScore list_Display = new PlayerMapScore();
    //public PlayerList list_LPS;



    public float imageAspect = 1.0f;

    private float NativeWidth = 1050;
    private float NativeHeight = 600;

    public GUISkin test_font;
    GUIStyle largeFont;

    public Texture scrollBG_texture;
    public Texture BG_texture;

    GameObject userData;
    userDataScript user_data;

    GameObject mapCheck;
    SelectedMap getMap;

    [SerializeField]
    private int Y_text_spacing = 70;


    [SerializeField]
    private int AR_COUNT;

    [SerializeField]
    private int LPS_COUNT;

    //public Texture backscroll_texture;

    void Awake()
    {
        for (int i = 0; i < 10; i++)
        {
            point_list.Add(i);
            money_list.Add(i);

            point_name_list.Add("point" + i.ToString());
            money_name_list.Add("money" + i.ToString());
        }

        name = "rand_val";


        largeFont = new GUIStyle();
        largeFont.fontSize = 23;
        largeFont.normal.textColor = Color.white;

        userData = GameObject.Find("Empty User");
        user_data = userData.GetComponent<userDataScript>();

        mapCheck = GameObject.Find("selectedMap");
        getMap = mapCheck.GetComponent<SelectedMap>();


    }

    void Start()
    {
        user_data.Load();

        if (getMap.getSelectedMap())
            //list_Display = user_data.score_AR;
            DataToList(user_data.score_AR);
        else
            //list_Display = user_data.score_LPS;
            DataToList(user_data.score_LPS);


    }

    void DataToList(PlayerMapScore temp)
    {
        if (temp.point.Count < 11)
        {
            for (int i = 0; i < temp.point.Count; i++)
            {
                if (temp.point[i] != null)
                {
                    point_list[i] = temp.point[i];
                    point_name_list[i] = temp.point_namelist[i];
                }


            }
            for (int r = 0; r < temp.money.Count; r++)
            {
                if (temp.money[r] != null)
                {
                    money_list[r] = temp.money[r];
                    money_name_list[r] = temp.money_namelist[r];
                }
            }
        }
    }
    void drawScores()
    {
        if (point_list.Count > 0)
        {
            for (int i = 0; i < point_list.Count; i++)
            {
                //for top 3 have images for them
                if (i < 3 && point_list[i] != null)
                {

                    //F2 for float precision
                    GUI.Label(new Rect(350, (480 + i * Y_text_spacing) - 450, 50, 50), point_list[i].ToString("F2"), largeFont);
                    GUI.Label(new Rect(160, (480 + i * Y_text_spacing) - 450, 50, 50), point_name_list[i], largeFont);
                    if (i == 0)
                    {
                        GUI.DrawTexture(new Rect(90, (480 + i * Y_text_spacing) - 460, 70, 40), first_Texture, ScaleMode.ScaleToFit, true, imageAspect);
                    }
                    if (i == 1)
                    {
                        GUI.DrawTexture(new Rect(100, (480 + i * Y_text_spacing) - 450, 50, 30), second_Texture, ScaleMode.ScaleToFit, true, imageAspect);
                    }
                    if (i == 2)
                    {
                        GUI.DrawTexture(new Rect(100, (480 + i * Y_text_spacing) - 450, 50, 30), third_Texture, ScaleMode.ScaleToFit, true, imageAspect);
                    }

                }
                else if (point_list[i] != null)
                {
                    GUI.Label(new Rect(120, (480 + i * Y_text_spacing) - 460, 50, 50), point_name_list[i], largeFont);
                    GUI.Label(new Rect(350, (480 + i * Y_text_spacing) - 460, 200, 50), point_list[i].ToString("F2"), largeFont);
                }


            }
        }
    }

    void drawMonies()
    {
        if (money_list.Count > 0)
        {
            for (int i = 0; i < money_list.Count; i++)
            {
                if (money_list[i] != null)
                {
                    //for top 3 have images for them
                    if (i < 3)
                    {

                        //F2 for float precision
                        GUI.Label(new Rect(350, (480 + i * Y_text_spacing) - 450, 50, 50), money_list[i].ToString("F2"), largeFont);
                        GUI.Label(new Rect(160, (480 + i * Y_text_spacing) - 450, 50, 50), money_name_list[i], largeFont);
                        if (i == 0)
                        {
                            GUI.DrawTexture(new Rect(90, (480 + i * Y_text_spacing) - 460, 70, 40), first_Texture, ScaleMode.ScaleToFit, true, imageAspect);
                        }
                        if (i == 1)
                        {
                            GUI.DrawTexture(new Rect(100, (480 + i * Y_text_spacing) - 450, 50, 30), second_Texture, ScaleMode.ScaleToFit, true, imageAspect);
                        }
                        if (i == 2)
                        {
                            GUI.DrawTexture(new Rect(100, (480 + i * Y_text_spacing) - 450, 50, 30), third_Texture, ScaleMode.ScaleToFit, true, imageAspect);
                        }

                    }
                    else
                    {
                        GUI.Label(new Rect(120, (480 + i * Y_text_spacing) - 460, 50, 50), money_name_list[i], largeFont);
                        GUI.Label(new Rect(350, (480 + i * Y_text_spacing) - 460, 200, 50), money_list[i].ToString("F2"), largeFont);
                    }
                }

            }
        }
    }


    void DoMyWindow(int windowID)
    {

        //GUI.DrawTexture (new Rect (-200, -100, 1000, 1000), backscroll_texture, ScaleMode.ScaleToFit, true, imageAspect);



        pointlistVscroll = GUI.BeginScrollView(new Rect(-20, 5, 450, 700), pointlistVscroll, new Rect(0, 0, 450, 1050));
        GUI.DrawTexture(new Rect(0, -0, 500, 750), scrollBG_texture, ScaleMode.StretchToFill, true, imageAspect);
        drawScores();

        GUI.EndScrollView();


        //GUI.DragWindow (new Rect (0, 0, 10000, 10000));
        //GUI.EndGroup ();
    }


    void DoMyWindow2(int windowID)
    {

        moneylistVscroll = GUI.BeginScrollView(new Rect(-20, 5, 450, 700), moneylistVscroll, new Rect(0, 0, 450, 1050));
        GUI.DrawTexture(new Rect(0, -0, 500, 750), scrollBG_texture, ScaleMode.StretchToFill, true, imageAspect);
        drawMonies();

        GUI.EndScrollView();
    }



    //	public void SaveToList () 
    //	{
    //		int temp = user_data.score_AR.point.Count;
    //
    //		//MAP LPS
    //		if (getMap.getSelectedMap() == true && temp <11 ) 
    //		{
    //						user_data.score_AR.point.Add (user_score.points);
    //						user_data.score_AR.money.Add (user_score.money);
    //						user_data.score_AR.point_namelist .Add (name);
    //						user_data.score_AR.money_namelist .Add (name);
    //
    //		}
    //		//MAP AR
    //		else if(getMap.getSelectedMap() == false && temp <11)
    //		{
    //			user_data.score_LPS.point.Add (user_score.points);
    //			user_data.score_LPS.money.Add (user_score.money);
    //			user_data.score_LPS.point_namelist .Add (name);
    //			user_data.score_LPS.money_namelist .Add (name);
    //		}
    //
    //
    //	}

    void OnGUI()
    {

        GUI.skin = test_font;
        //test_font.label (new Rect (100, 100, 100, 100), "some string");
        //test_font.f
        float rx = Screen.width / NativeWidth;
        float ry = Screen.height / NativeHeight;


        // Scale width the same as height - cut off edges to keep ratio the same
        GUI.matrix = Matrix4x4.TRS(new Vector3(0, 0, 0), Quaternion.identity, new Vector3(rx, ry, 1));
        // Get width taking into account edges being cut off or extended

        float adjustedWidth = NativeWidth * (rx / ry);

        //draw back button
        //GUI.DrawTexture (new Rect(0, Y_text_spacing, 235, 235), back_btn);

        //GUILayout.Label (getMap.getSelectedMap ().ToString ());


        pointlistRect = GUI.Window(1, pointlistRect, DoMyWindow, "Point List");
        moneylistRect = GUI.Window(0, moneylistRect, DoMyWindow2, "Money List");

        //for (int i = 0; i<10; i++) {
        //	GUILayout.Label("point" + i+":" +user_data.score_AR.point_namelist[i] + "   " + user_data.score_AR.point[i].ToString());
        //GUILayout.Label("money" + i+":" +money_name_list[i] + "   " + money_list[i].ToString());
        //	}



    }

    void Update()
    {



        if (getMap.getSelectedMap())
            DataToList(user_data.score_AR);
        else
            DataToList(user_data.score_LPS);
        //
        //
        //		if (user_data.enabled) 
        //		{
        //			print ("userdata_exist");
        //		}
        //Touch touch = Input.touches[0];
        //if (Input.touchCount > 0)
        //{
        //    if (touch.phase == TouchPhase.Moved)
        //    {
        //        scrollPosition.y += touch.deltaPosition.y;
        //    }
        //}
    }


}
