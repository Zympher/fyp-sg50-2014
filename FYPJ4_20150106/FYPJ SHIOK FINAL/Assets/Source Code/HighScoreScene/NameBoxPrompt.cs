﻿using UnityEngine;
using System.Collections;

public class NameBoxPrompt : MonoBehaviour {

	static int guiDepth = -3;
	//GameObject UserData;
	//userDataScript user_data;

	//get a name by prompting a textbox
	public string user_name;

	//saves score and money from points carry

	public float user_score;
	public float user_money;


	GameObject pointsCarry;
	PointsCarry pt_carry;

	public bool time_to_activate = false;
	public bool time_to_transfer = false;

	public bool JustOnce = true;


	//public Texture[] enterbtn;

	public GUIStyle customBtn;

	public GUIContent EnterName;

	private const int max_text_len = 10;

	private static NameBoxPrompt _instance = null;

	[SerializeField]
	private int btnsize_x;
	[SerializeField]
	private int btnsize_y;


	[SerializeField]
	private int textbox_x;

	[SerializeField]
	private int textbox_y;

	[SerializeField]
	private int box_size_x;

	[SerializeField]
	private int box_size_y;


	[SerializeField]
	private Texture namebox_tex;

	[SerializeField]
	private Texture input_tex;

	[SerializeField]
	private GUIStyle namebox_style;
	// Use this for initialization
	void Start () {

		//get user data script
		//UserData = GameObject.Find ("Empty User");
		//user_data = UserData.GetComponent <userDataScript> ();

		//btnsize_x = 350;
		//btnsize_y = 200;

		pointsCarry = GameObject.Find ("PointsCarrying");
		pt_carry = pointsCarry.GetComponent<PointsCarry> ();


		//box_size_x = namebox_tex.width;
		//box_size_y = namebox_tex.height;
	
	}

	void Awake ()
	{
		if (_instance == null) 
		{
			//If I am the first instance, make me the Singleton
			_instance = this;
			DontDestroyOnLoad (this);
		}

		if(_instance != this)
		{
			//if another instance exists delete them
			Destroy (gameObject);
		}
	}
	// Update is called once per frame
	void Update () {

		if (Application.loadedLevelName == "fast_highscore_build") {
						time_to_activate = true;
						//time_to_transfer = false;
						//time_to_activate = false;
						//JustOnce = true;
				}
		else {
			time_to_activate = false;
				}


	
	}

	void OnGUI()
	{

		//print ("time to transfer: " + time_to_tr

		GUI.depth = guiDepth;

		//textfield text are white
		GUI.skin.textArea.normal.textColor = Color.black;
		GUI.skin.textArea.normal.background =(Texture2D) input_tex;
		GUI.skin.textArea.hover.background = (Texture2D) input_tex;
		GUI.skin.textArea.active.background = (Texture2D) input_tex;
		GUI.skin.textArea.focused.background = (Texture2D) input_tex;

		GUI.skin.textArea.onNormal.background =(Texture2D) input_tex;
		GUI.skin.textArea.onHover.background = (Texture2D) input_tex;
		GUI.skin.textArea.onActive.background = (Texture2D) input_tex;
		GUI.skin.textArea.onFocused.background = (Texture2D) input_tex;

		if (!time_to_transfer && time_to_activate) 
        {
		    //if (Application.loadedLevelName == "fast_highscore_build" && JustOnce) {					
			user_score = pt_carry.points;
			user_money = pt_carry.money;			

			if (JustOnce) 
            {
				GUI.DrawTexture(new Rect((Screen.width / 2) - box_size_x /2 , (Screen.height / 2) - box_size_y/2 ,box_size_x,box_size_y),namebox_tex);
										
				//GUI.color = Color.white;

				user_name = GUI.TextArea (new Rect ((Screen.width / 2)-textbox_x/2, (Screen.height / 2) - textbox_y/2, textbox_x, textbox_y), user_name, max_text_len,namebox_style );

				if (GUI.Button (new Rect ((Screen.width / 2)-btnsize_x/2, (Screen.height / 2) -btnsize_y/2 , btnsize_x, btnsize_y),EnterName,customBtn)) 
                {
					//time_to_transfer = true;
					JustOnce = false;
					time_to_activate = false;
					//JustOnce = false;
					//time_to_activate = true;
					//time_to_activate = true;
				}
			}
		}
		//GUILayout.Label ("Activate: " + time_to_activate.ToString ());
		//GUILayout.Label ("Transfer: " +time_to_transfer.ToString ());
	}
}
