﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;

using System;

public class MenuController_V2 : MonoBehaviour 
{
	public Vector2 startTouch = Vector2.zero;
    
	public GUISkin mySkin;

	public Texture2D onceButton;

	private Rect SG50BtnPos, PlayBtnPos, TutorialBtnPos, CreditsBtnPos, HawkerGuideBtnPos;

	private GUIStyle Play, SG50, Tutorial, Credits, HawkerGuide;

	private float scaleWidth, scaleHeight;

    public int JustOnce = 1;

    GameObject bg;

	// Use this for initialization
	void Start () 
	{
        JustOnce = 1;

		scaleWidth = Screen.width / 1920.0f;
		scaleHeight = Screen.height / 1080.0f;
		
        print (PlayerPrefs.GetInt("tutOnce"));

		Play = mySkin.FindStyle("Play");
		SG50 = mySkin.FindStyle("SG50");
		Tutorial = mySkin.FindStyle("Tutorial");
		Credits = mySkin.FindStyle("Credits");
		HawkerGuide = mySkin.FindStyle("HawkerGuide");

		SG50BtnPos = new Rect(170 * scaleWidth, 80 * scaleHeight, SG50.fixedWidth * scaleWidth, SG50.fixedHeight * scaleHeight);
		PlayBtnPos = new Rect(300 * scaleWidth, 520 * scaleHeight, Play.fixedWidth * scaleWidth, Play.fixedHeight * scaleHeight);
		TutorialBtnPos = new Rect(820 * scaleWidth, 670 * scaleHeight, Tutorial.fixedWidth * scaleWidth, Tutorial.fixedHeight * scaleHeight);
		CreditsBtnPos = new Rect(1320 * scaleWidth, 520 * scaleHeight, Credits.fixedWidth * scaleWidth, Credits.fixedHeight * scaleHeight);
		HawkerGuideBtnPos = new Rect(1460 * scaleWidth, 90 * scaleHeight, HawkerGuide.fixedWidth * scaleWidth, HawkerGuide.fixedHeight * scaleHeight);
	}

	
	// Update is called once per frame
	void Update () 
	{
		Resize();
	}

    void Resize()
    {
        bg = GameObject.Find("MainMenu");
        SpriteRenderer sr = bg.GetComponent<SpriteRenderer>();

        sr.transform.localScale = new Vector3(1, 1, 1);

        float worldScreenHeight = Camera.main.orthographicSize * 2f;
        float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;

        sr.transform.localScale = new Vector3(worldScreenWidth / sr.sprite.bounds.size.x,
                                           worldScreenHeight / sr.sprite.bounds.size.y, 1);
    }

    void OnGUI()
    {
        GameObject sound = GameObject.Find("AudioManager");
        SoundManager buttonSound = sound.GetComponent<SoundManager>();

		//Play Button
		if (GUI.Button(SG50BtnPos, GUIContent.none, mySkin.GetStyle("SG50")))
		{
			buttonSound.MenuButtons();
			Application.OpenURL("http://data.gov.sg/common/Map.aspx?Theme=HAWKERCENTRE");
		}
		else if(GUI.Button(PlayBtnPos, GUIContent.none, mySkin.GetStyle("Play")))
		{
			buttonSound.MenuButtons();
			Application.LoadLevel("MapScene");
		}
		else if(GUI.Button(HawkerGuideBtnPos, GUIContent.none, mySkin.GetStyle("HawkerGuide")))
		{
			buttonSound.MenuButtons();
			Application.LoadLevel("HawkerGuideScene");
		}
		else if(GUI.Button(TutorialBtnPos, GUIContent.none, mySkin.GetStyle("Tutorial")))
		{
			buttonSound.MenuButtons();
			Application.LoadLevel("TutorialScene");
		}
		else if(GUI.Button(CreditsBtnPos, GUIContent.none, mySkin.GetStyle("Credits")))
		{
			buttonSound.MenuButtons();
			Application.LoadLevel("CreditsScene");
		}
    }

}
