﻿using UnityEngine;
using System.Collections;

public enum STALL_TYPE
{
	CHINESE,
	MALAY,
	DRINK,
    SATAY,
    ROJAK
    //TEHTARIK
}

public class StallController : MonoBehaviour 
{
	public STALL_TYPE Type;
	private ArrayList StallCustomerQueueList;
	public GameObject[] StallPointPrefabs;
	private const float COOKING_TIME = 1.2f;
	private float fCookingTime_Time;

	// Use this for initialization
	void Start () 
	{
		fCookingTime_Time = -1;
		StallCustomerQueueList = new ArrayList();
	}

	public bool IsQueueFull()
	{
		if ( StallCustomerQueueList.Count < StallPointPrefabs.Length )
			return false;
		return true;
	}

	public void CustomerEnterStall(GameObject customer)
	{
		StallCustomerQueueList.Add (customer);
	}

	public void TheFirstCustomerLeaveStall()
	{
		//if(StallCustomerQueueList
		StallCustomerQueueList.RemoveAt (0);
	}

	public void StartCookingFood()
	{
		fCookingTime_Time = Time.time;
	}

	public int GetCustomerQueueNumber(GameObject customer)
	{
		return StallCustomerQueueList.IndexOf (customer);
	}

    public int GetLastQueueNumberWithCustomer()
    {
        return StallCustomerQueueList.Count;
    }

	public int GetTheLastQueueNumber()
	{
		if (StallPointPrefabs.Length - 1 <= 0) 
		{
			return 0;
		}

		else 

		return StallPointPrefabs.Length-1;
	}

	public Vector3 GetStallQueuePoint(int index)
	{
		return StallPointPrefabs [index].transform.position;
	}

	public bool IsFoodFinished()
	{
		if (Time.time > fCookingTime_Time + COOKING_TIME)
			return true;
		return false;
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}


}
