﻿using UnityEngine;
using System.Collections;

public enum SEAT_STATE
{
	EMPTY,
	CHOPE,
	TAKEN,
    TRAY,
};

public class TableController : MonoBehaviour 
{
	public int MAX_SEAT = 4;
	public SEAT_STATE[] seatState;
	public GameObject ChopePrefab;
    public GameObject TrayPrefab;
	public GameObject ChopeObj;
    public GameObject TrayObj;
	public GameObject TableMarker;
	public GameObject EatingSpritePrefab;
	private GameObject EatingSpriteObj;
	public int NumOfCustomerAreEating;
	public bool IsTakenByDurainUncle;

    GameObject mapCheck;
    SelectedMap getMap;

	// Use this for initialization
	void Start () 
	{
		NumOfCustomerAreEating = 0;
		seatState = new SEAT_STATE[MAX_SEAT];
		for ( int i= 0 ; i < MAX_SEAT ; i++ )
		{
			seatState[i] = SEAT_STATE.EMPTY;
		}
		EatingSpriteObj = null;
		IsTakenByDurainUncle = false;
		ChopeObj = null;
        TrayObj = null;

        mapCheck = GameObject.Find("selectedMap");
        getMap = mapCheck.GetComponent<SelectedMap>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if ( NumOfCustomerAreEating > 0 )
		{
			if ( EatingSpriteObj == null )
			{
				CreateAndShowEatingSprite();
			}
		}
		else
		{
			DestroyEatingSprite();
		}
	}

    public void CreateAndShowEatingSprite()
    {
        if (getMap.getSelectedMap())
        {
            EatingSpriteObj = (GameObject)Instantiate(EatingSpritePrefab,
                                                      gameObject.transform.position,
                                                      gameObject.transform.rotation);
        }
        else
        {
            EatingSpriteObj = (GameObject)Instantiate(EatingSpritePrefab,
                                                   gameObject.transform.position + new Vector3(0,1.0f),
                                                   gameObject.transform.rotation);
        }
    }

	public void CreateAndShowChopeObj()
	{
		DestroyChopeObj ();
		ChopeObj = (GameObject)Instantiate(ChopePrefab,
		                                   gameObject.transform.position + new Vector3(0,0,-1.0f),
		                                   gameObject.transform.rotation);
	}

    public void CreateAndShowTrayObj()
    {
        DestroyTrayObj();
        TrayObj = (GameObject)Instantiate(TrayPrefab,
                                            gameObject.transform.position + new Vector3(0, 0, -1.0f),
                                            gameObject.transform.rotation);
    }

	public void DestroyChopeObj()
	{
		if ( ChopeObj != null )
			Destroy (ChopeObj);
		ChopeObj = null;
	}

    public void DestroyTrayObj()
    {
        if (TrayObj != null)
            Destroy(TrayObj);
        TrayObj = null;
    }
	
	public Vector3 GetSitDownPoint()
	{
        if (getMap.getSelectedMap())
        {
            return gameObject.transform.position + new Vector3(0, 0.5f, 0);//0.5f
        }
        else
        {
            return gameObject.transform.position + new Vector3(0, 2.0f, 0);//0.5f
        }
	}

	public Vector3 GetStandUpPoint()
	{
        if (getMap.getSelectedMap())
        {
            return gameObject.transform.position + new Vector3(0, 3.0f, 0);//3.0f
        }
        else
        {
            return gameObject.transform.position + new Vector3(0, 3.8f, 0);//3.0f
			//changed
        }
	}

	public void LeaveSeat(int groupID,int SeatIndex)
	{
		seatState [SeatIndex] = SEAT_STATE.EMPTY;
		TableMarker.GetComponent<TableMarkerController> ().RemoveColor (groupID);
	}

	public void ChopeSeat(int SeatIndex)
	{
		seatState [SeatIndex] = SEAT_STATE.CHOPE;
	}

    public void TraySeat(int SeatIndex)
    {
        seatState[SeatIndex] = SEAT_STATE.TAKEN;
    }
	
	public void TakeSeat(int groupID,int SeatIndex)
	{
		seatState [SeatIndex] = SEAT_STATE.TAKEN;
		TableMarker.GetComponent<TableMarkerController> ().AddColor (groupID);
	}
	
	public Vector3 GetSeatPosition(int SeatIndex)
	{
        if (getMap.getSelectedMap())    //true = adam road else = LPS
        {
            if (gameObject.name.Contains("1"))
            {
                if (SeatIndex < MAX_SEAT / 2)
                    return transform.position + new Vector3(-1.7f - 0.3f * (SeatIndex % (MAX_SEAT / 2)), -SeatIndex % (MAX_SEAT / 2) + 1.5f, 0);
                else
                    return transform.position + new Vector3(1.7f - 0.3f * (SeatIndex % (MAX_SEAT / 2)), -SeatIndex % (MAX_SEAT / 2) + 1.5f, 0);
            }
            else if (gameObject.name.Contains("2"))
            {
                if (SeatIndex < MAX_SEAT / 2)
                    return transform.position + new Vector3(-1.3f - 0.3f * (SeatIndex % (MAX_SEAT / 2)), -SeatIndex % (MAX_SEAT / 2) + 1.3f, 0);
                else
                    return transform.position + new Vector3(1.3f - 0.3f * (SeatIndex % (MAX_SEAT / 2)), -SeatIndex % (MAX_SEAT / 2) + 1.3f, 0);
            }
            else if (gameObject.name.Contains("3"))
            {
                if (SeatIndex < MAX_SEAT / 2)
                    return transform.position + new Vector3(-1.3f + 0.3f * (SeatIndex % (MAX_SEAT / 2)), -SeatIndex % (MAX_SEAT / 2) + 1.3f, 0);
                else
                    return transform.position + new Vector3(1.3f + 0.3f * (SeatIndex % (MAX_SEAT / 2)), -SeatIndex % (MAX_SEAT / 2) + 1.3f, 0);
            }
            else
            {
                if (SeatIndex < MAX_SEAT / 2)
                    return transform.position + new Vector3(-1.7f + 0.3f * (SeatIndex % (MAX_SEAT / 2)), -SeatIndex % (MAX_SEAT / 2) + 1.3f, 0);
                else
                    return transform.position + new Vector3(1.5f + 0.3f * (SeatIndex % (MAX_SEAT / 2)), -SeatIndex % (MAX_SEAT / 2) + 1.3f, 0);
            }
        }
        else
        {
            if (gameObject.name.Contains("1"))
            {
                if (SeatIndex < MAX_SEAT / 2)
                    return transform.position + new Vector3(-1.55f - 0.3f * (SeatIndex % (MAX_SEAT / 2)), -SeatIndex % (MAX_SEAT / 2) * 1.3f + 2.3f, 0);
                else
                    return transform.position + new Vector3(1.8f - 0.3f * (SeatIndex % (MAX_SEAT / 2)), -SeatIndex % (MAX_SEAT / 2) * 1.3f + 2.3f, 0);
            }
            else if (gameObject.name.Contains("2"))
            {
                if (SeatIndex < MAX_SEAT / 2)
                    return transform.position + new Vector3(-1.15f - 0.3f * (SeatIndex % (MAX_SEAT / 2)), -SeatIndex % (MAX_SEAT / 2) * 1.0f + 2.0f, 0);
                else
                    return transform.position + new Vector3(1.6f - 0.3f * (SeatIndex % (MAX_SEAT / 2)), -SeatIndex % (MAX_SEAT / 2) * 1.0f + 2.0f, 0);
            }
            else if (gameObject.name.Contains("3"))
            {
                if (SeatIndex < MAX_SEAT / 2)
                    return transform.position + new Vector3(-1.65f + 0.3f * (SeatIndex % (MAX_SEAT / 2)), -SeatIndex % (MAX_SEAT / 2) * 1.0f + 2.0f, 0);
                else
                    return transform.position + new Vector3(1.25f + 0.3f * (SeatIndex % (MAX_SEAT / 2)), -SeatIndex % (MAX_SEAT / 2) * 1.0f + 2.0f, 0);
            }
            else
            {
                if (SeatIndex < MAX_SEAT / 2)
                    return transform.position + new Vector3(-1.95f + 0.3f * (SeatIndex % (MAX_SEAT / 2)), -SeatIndex % (MAX_SEAT / 2) * 1.3f + 2.5f, 0);
                else
                    return transform.position + new Vector3(1.55f + 0.3f * (SeatIndex % (MAX_SEAT / 2)), -SeatIndex % (MAX_SEAT / 2) * 1.3f + 2.5f, 0);
            }
       }
	}
	
	public bool IsTaken()
	{
		for ( int i= 0 ; i < MAX_SEAT ; i++ )
		{
			if ( seatState[i] != SEAT_STATE.EMPTY )
				return true;
		}
		return false;
	}

    //public void TableTrayed()
    //{
    //    for (int i = 0; i < MAX_SEAT; i++)
    //    {
    //        if (seatState[i] == SEAT_STATE.TRAY)
    //        {
    //            for (int j = 0; j < MAX_SEAT; j++)
    //            {
    //                seatState[j] == SEAT_STATE.TAKEN;
    //            }
    //        }
    //    }
    //}

    public void ClearTrays()
    {
        Debug.Log("clear");
    }

    public int GetAvailableSeatIndex()
	{
		for ( int i= 0 ; i < MAX_SEAT ; i++ )
		{
			if ( seatState[i] == SEAT_STATE.EMPTY )
				return i;
		}
		return -1;
	}
	
	public int GetNumOfSeatLeft()
	{
		int NumOfSeatLeft = 0;
		for ( int i= 0 ; i < MAX_SEAT ; i++ )
		{
			if ( seatState[i] == SEAT_STATE.EMPTY )
				NumOfSeatLeft++;
		}
		return NumOfSeatLeft;
	}

	public void DestroyEatingSprite()
	{
		if ( EatingSpriteObj != null )
			Destroy (EatingSpriteObj);
		EatingSpriteObj = null;
	}

    //public int NumberOfChattyAunt() 
    //{
    //    return auntie_count;
    //}
}
