﻿using UnityEngine;
using System.Collections;

public enum CUSTOMER_AI_STATE
{
    WAIT_FOR_ENTER_PREMISSION,
    ENTER_HAWKER_CENTER,
    WALK_AROUND,
    CHOPE_A_TABLE,
    WAIT_FOR_GROUP_MEMBER,
    CHOPE,
    GO_TO_TABLE,
    BUY_FOOD,
    CHOOSE_STALL,
    GO_TO_STALL,
    QUEUE,
    ORDERING,
    WAIT_FOR_FOOD,
    GO_TO_SEAT,
    EAT,
    LEAVE,
    REMOVE,
};

public class ShiokAI : MonoBehaviour
{
    //Points
    Vector2 spawnPointLeft;
    Vector2 spawnPointRight;
    Vector2[,] StallPoints;

    //Map2D
    Map2D map;
    const float MAP_WIDTH = 36.0f;
    const float MAP_HEIGHT = 20.0f;
    float GRID_SIZE = 1;

    //Customer
    public GameObject GroupSpawnerLeft;
    public GameObject GroupSpawnerRight;
    public GameObject CustomerPrefab;
    public GameObject DurainUncleWarningPrefab;

    //hy added
    //public GameObject ChattyAuntieWarningPrefab;//created

    float MAX_WALK_AROUND_RANGE = 11.0f;
    const int DURAIN_UNCLE_SPAWN_CHANCE = 40;

    //hy added 
    //const int CHATTY_AUNTIE_SPAWN_CHANCE = 30;

    //ShiokBubble
    public GameObject ShiokBubbleObj;

    //Group
    int MAX_CUSTOMER_IN_A_GROUP = 3;
    int MAX_CUSTOMER_GROUP_MARKER = 3;
    float GROUP_SPAWN_DELAY = 10;
    float fCustomerSpawn_Time;
    int[] NumOfGroupMember;

    //Stall
    const int NUM_OF_STALL = 5;

    //Player Control
    public bool IsInMiniGameMode = false;
    GameObject theSelectedCustomer = null;

    //IceKacang
    public GameObject IceKacangPrefab;
    private GameObject IceKacangObj;
    private const int ICE_KACANG_REWARD_POINTS = 3000;

    //ChickenRice
    public GameObject ChickenRicePrefab;
    private GameObject ChickenRiceObj;
    private const int CHICKEN_RICE_REWARD_POINTS = 3000;

    //NasiLemak
    public GameObject NasiLemakPrefab;
    private GameObject NasiLemakObj;
    private const int NASILEMAK_REWARD_POINTS = 3000;

    //Satay
    public GameObject SatayPrefab;
    private GameObject SatayObj;
    private const int SATAY_REWARD_POINTS = 3000;

    //#Rojak
    public GameObject RojakPrefab;
    private GameObject RojakObj;
    private const int ROJAK_REWARD_POINTS = 3000;

    //TehTarik
    public GameObject TehTarikPrefab;
    private GameObject TehTarikObj;
    private const int TEHTARIK_REWARD_POINTS = 3000;

    //Minigame Feedback
    public GameObject MGFeedbackPrefab;
    private GameObject MGFeedbackObj;

    //DayNight Timer
    bool peakBool = false;
    double timerAngle;

    //newshop check
    bool shopOpen = false;
    // extra money check
    bool two_times = false;

    //satay game checl
    public bool isSatay = false;

    GameObject MGtimeCheck;
    Timer getTime;

    GameObject mapCheck;
    SelectedMap getMap;

    //upgrade activated check
    bool extraMoneyOpen = false;
    //Upgrade active
    public bool UpgradeActive = false;

    GameObject upgradeCheck;
    ContainsUpgrade getUpgrade;

    GameObject peakCheck;

    //Bench
    const float BENCH_Z_VALUE = 12.0f;

    //init map
    private void initMap2D()
    {
        map = new Map2D();
        map.Init(MAP_WIDTH, MAP_HEIGHT, GRID_SIZE);
    }

    //init points
    private void initPoints()
    {
        spawnPointLeft = new Vector2(-16.0f, -6.0f);
        spawnPointRight = new Vector2(16.0f, -6.0f);
    }

    private void initCustomerGroupSpawner()
    {
        NumOfGroupMember = new int[MAX_CUSTOMER_GROUP_MARKER];
        for (int i = 0; i < MAX_CUSTOMER_GROUP_MARKER; i++)
        {
            NumOfGroupMember[i] = 0;
        }
        fCustomerSpawn_Time = Time.time - GROUP_SPAWN_DELAY;
    }

    private void initBenches()
    {
        foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Bench"))
        {
            Vector3 tempPosition = new Vector3(obj.transform.position.x,
                                               obj.transform.position.y,
                                               BENCH_Z_VALUE);
            obj.transform.position = tempPosition;
        }
    }

    //convert world position to grid position
    private int PositionXToGridX(float X)
    {
        return (int)(X / map.gridSize + map.numOfCol / 2);
    }

    //convert world position to grid position
    private int PositionXToGridY(float Y)
    {
        return (int)(Y / map.gridSize + map.numOfRow / 2);
    }

    //Customer will follow the path till the last grid, then he will go to the target point
    private void CustomerFindPathAndMoveTo(GameObject Customer, float X, float Y)
    {
        CustomerController theController = Customer.GetComponent<CustomerController>();
        ArrayList path = map.FindPath(PositionXToGridX(theController.GetFeetPosition().x),
                                       PositionXToGridY(theController.GetFeetPosition().y),
                                       PositionXToGridX(X),
                                       PositionXToGridY(Y));
        if (path != null)
        {
            theController.StartToFollowThePathAndMoveTo(path, X, Y);
        }
        else
        {
            theController.StopMoving();
        }
    }

    private void CreateDurainUncleWarning(bool IsInLeftSide)
    {
        Vector2 tempPosition;
        Vector3 finalPosition;

        Quaternion quaternion = new Quaternion();
        if (IsInLeftSide)
        {
            tempPosition = spawnPointLeft + new Vector2(((Screen.width * 0.15f) / 100), 3.0f);
            finalPosition = new Vector3(tempPosition.x, tempPosition.y, -30);
            quaternion.y = 180.0f;
        }
        else
        {
            tempPosition = spawnPointRight + new Vector2((-(Screen.width * 0.15f) / 100), 3.0f);
            finalPosition = new Vector3(tempPosition.x, tempPosition.y, -30);
            quaternion.y = 0.0f;
        }

        Instantiate(DurainUncleWarningPrefab,
                    finalPosition,
                    quaternion);
    }

    private void DestoryDurainUncleWarning(bool IsInLeftSide)
    {
        foreach (GameObject obj in GameObject.FindGameObjectsWithTag("DurainUncleWarning"))
        {
            if (IsInLeftSide && obj.transform.position.x < 0)
            {
                Destroy(obj);
                break;
            }
            if (!IsInLeftSide && obj.transform.position.x > 0)
            {
                Destroy(obj);
                break;
            }
        }
    }

    IEnumerator timerWait()
    {
        Debug.Log("entered");
        yield return new WaitForSeconds(1.0f);
    }

    void PeakHour()
    {
        //timerAngle = peakCheck.GetComponent<DayNightTimer>().getAngle();

        timerAngle = System.Math.Floor(timerAngle * 100.0f) / 100.0f;

        //Debug.Log("TimerAngle: " + timerAngle);

        if (!peakBool)
        {
            GROUP_SPAWN_DELAY = 10; //(original)
            //MAX_CUSTOMER_GROUP_MARKER = 3; //(original)
            if (timerAngle % 90 == 0)
            {
                if (timerAngle > 1)
                    peakBool = true;
            }

        }

        if (peakBool)
        {
            GROUP_SPAWN_DELAY = 1; //(original = 10)
            //MAX_CUSTOMER_GROUP_MARKER = 5; //(original = 3)

            if (timerAngle % 180 == 0)
            {
                peakBool = false;
            }
        }
    }

    private void CreateCustomerGroup(int GroupID)
    {
        int iWhichSide = Random.Range(0, 2);

        int numOfCustomer = Random.Range(1, MAX_CUSTOMER_IN_A_GROUP + 1);
        NumOfGroupMember[GroupID] = numOfCustomer;

        for (int i = 0; i < numOfCustomer; i++)
        {
            Vector3 temp_position;
            GameObject newCustomer;
            CustomerController theController;
            CUSTOMER_TYPE type;

            // init customer position
            double angle = (double)i * 2 * System.Math.PI / numOfCustomer;
            Vector3 position_offset = new Vector3((float)System.Math.Cos(angle), (float)System.Math.Sin(angle), 0);
            temp_position = (iWhichSide == 0 ?
                              GroupSpawnerLeft.transform.position + position_offset :
                              GroupSpawnerRight.transform.position + position_offset);

            // create customer
            newCustomer = (GameObject)Instantiate(CustomerPrefab,
                                                  temp_position,
                                                  CustomerPrefab.transform.rotation);
            theController = newCustomer.GetComponent<CustomerController>();

            // init customer type
            type = (CUSTOMER_TYPE)Random.Range(0, theController.GetNumOfCustomerType());
            //type = (CUSTOMER_TYPE)Random.Range(6, 6);
            //Debug.Log(type.ToString());

            if (numOfCustomer == 1)
            {
                if (Random.Range(0, 100) < DURAIN_UNCLE_SPAWN_CHANCE)
                {
                    type = CUSTOMER_TYPE.DURAIN_UNCLE;
                }
            }
            theController.Init(type, MAP_HEIGHT, BENCH_Z_VALUE);
            if (type == CUSTOMER_TYPE.DURAIN_UNCLE)
            {
                CreateDurainUncleWarning(theController.IsEnterFromLeftSide);
                fCustomerSpawn_Time += theController.GetDurainUncleDelay();
            }

            //hy added
            /*if( numOfCustomer == 2) 
            {
                if(Random.Range (0, 30) < CHATTY_AUNTIE_SPAWN_CHANCE ) 
                {
                    type = CUSTOMER_TYPE.CHATTY_AUNTIE;
                }
            }
            theController.Init (type,MAP_HEIGHT,BENCH_Z_VALUE);
            if( type == CUSTOMER_TYPE.CHATTY_AUNTIE ) 
            {
                CreateChattyAuntieWarning(theController.IsEnterFromLeftSide);
                fCustomerSpawn_Time += theController.GetDurainUncleDelay();
            }*/

            // init group info
            theController.GroupInit(GroupID, i, numOfCustomer);
        }
    }

    private int GetAvailableGroupID()
    {
        for (int i = 0; i < MAX_CUSTOMER_GROUP_MARKER; i++)
        {
            if (NumOfGroupMember[i] == 0)
                return i;
        }
        return -1;
    }

    private void SpawnCustomerGroup()
    {
        if (Time.time > fCustomerSpawn_Time + GROUP_SPAWN_DELAY)
        {
            fCustomerSpawn_Time = Time.time;
            int GroupID = GetAvailableGroupID();
            if (GroupID != -1)
            {
                CreateCustomerGroup(GroupID);
            }
        }
    }

    private Vector3 CalcRandomPoint()
    {
        Vector3 temp_targetPoint = new Vector3(0, map.mapHeight / 2 * -1, 0);
        //0 to 180
        double angle = Random.Range(0, 1.0f) * (float)System.Math.PI;
        Vector3 temp_direction = new Vector3((float)System.Math.Cos(angle), (float)System.Math.Sin(angle), 0);
        temp_targetPoint += temp_direction * Random.Range(0, MAX_WALK_AROUND_RANGE);
        return temp_targetPoint;
    }

    private void UpgradeActivated()
    {
        if (getMap.getSelectedMap())
        {
            if (upgradeCheck)
            {


            }
        }
    }

    // Use this for initialization
    private void Start()
    {
        IsInMiniGameMode = false;
        IceKacangObj = null;
        ChickenRiceObj = null;
        NasiLemakObj = null;
        SatayObj = null;
        RojakObj = null; //#rojak
        TehTarikObj = null;
        MGFeedbackObj = null;

        initBenches();
        initPoints();
        initMap2D();
        initCustomerGroupSpawner();

        MGtimeCheck = GameObject.Find("Pause");
        getTime = MGtimeCheck.GetComponent<Timer>();

        mapCheck = GameObject.Find("selectedMap");
        getMap = mapCheck.GetComponent<SelectedMap>();

        //upgradeCheck = GameObject.Find("UpgradesObject");
        //getUpgrade = upgradeCheck.GetComponent<ContainsUpgrade>();

        // get extra money upgrade
        //extraMoneyCheck = GameObject.Find("ExtraMoneyObject");
        //getUpgrade = extraMoneyCheck.GetComponent<ContainsUpgrade>();

        peakCheck = GameObject.Find("TimerBackground");
        timerAngle = 0;

        //shopOpen = getUpgrade.getBuy1();
        //two_times = getUpgrade.getBuy3();
        //extraMoneyOpen = getUpgrade.getBuy2();
    }

    private void UpdateAI()
    {
        if (shopOpen)
        {
            GameObject obj;

            obj = GameObject.Find("LockShop");

            obj.renderer.enabled = false;
        }

        foreach (GameObject CustomerObj in GameObject.FindGameObjectsWithTag("Customer"))
        {
            CustomerController customerController = CustomerObj.GetComponent<CustomerController>();
            if (customerController.currentState == CUSTOMER_AI_STATE.WAIT_FOR_ENTER_PREMISSION)
            {
                if (customerController.IsAllowEnterHawkerCenter())
                {
                    if (getMap.getSelectedMap())
                    {
                        // customer start to enter hawker center
                        double angle = (double)customerController.groupIndex * 2 * System.Math.PI / customerController.numOfCustomerInTheGroup;
                        Vector2 position_offset = new Vector2((float)System.Math.Cos(angle), (float)System.Math.Sin(angle));
                        if (customerController.IsEnterFromLeftSide)
                            customerController.MoveTo(spawnPointLeft.x + position_offset.x + 1.0f, spawnPointLeft.y + position_offset.y + 1.0f);
                        else
                            customerController.MoveTo(spawnPointRight.x + position_offset.x - 2.0f, spawnPointRight.y + position_offset.y + 2.0f);

                        if (customerController.type == CUSTOMER_TYPE.DURAIN_UNCLE)
                            DestoryDurainUncleWarning(customerController.IsEnterFromLeftSide);
                        customerController.currentState = CUSTOMER_AI_STATE.ENTER_HAWKER_CENTER;
                    }
                    else
                    {

                        // customer start to enter hawker center
                        double angle = (double)customerController.groupIndex * 2 * System.Math.PI / customerController.numOfCustomerInTheGroup;
                        Vector2 position_offset = new Vector2((float)System.Math.Cos(angle), (float)System.Math.Sin(angle));
                        if (customerController.IsEnterFromLeftSide)
                            customerController.MoveTo(spawnPointLeft.x + position_offset.x + 2.5f, spawnPointLeft.y + position_offset.y);
                        else
                            customerController.MoveTo(spawnPointRight.x + position_offset.x - 2.5f, spawnPointRight.y + position_offset.y - 2.0f);

                        if (customerController.type == CUSTOMER_TYPE.DURAIN_UNCLE)
                            DestoryDurainUncleWarning(customerController.IsEnterFromLeftSide);
                        customerController.currentState = CUSTOMER_AI_STATE.ENTER_HAWKER_CENTER;
                    }
                }
            }
            else if (customerController.currentState == CUSTOMER_AI_STATE.ENTER_HAWKER_CENTER)
            {
                if (customerController.IsReachTargetPoint())
                {
                    // customer has entered hawker center
                    customerController.CreateAndShowGroupMark();
                    customerController.currentState = CUSTOMER_AI_STATE.WALK_AROUND;
                    customerController.CreateAndShowPatienceIndicator();
                }
            }
            else if (customerController.currentState == CUSTOMER_AI_STATE.WALK_AROUND)
            {
                if (customerController.IsReachTargetPoint())
                {
                    if (!customerController.IsStartLookingForSeat)
                    {
                        customerController.LookingForSeat();
                    }
                    else
                    {
                        if (!customerController.IsLookingForSeat())
                        {
                            Vector3 RandomPoint = CalcRandomPoint();
                            CustomerFindPathAndMoveTo(CustomerObj, RandomPoint.x, RandomPoint.y);
                            customerController.IsStartLookingForSeat = false;
                        }
                    }
                }
            }
            else if (customerController.currentState == CUSTOMER_AI_STATE.CHOPE_A_TABLE)
            {
                if (customerController.IsReachTargetPoint())
                {
                    TableController tableController = customerController.TargetTable.GetComponent<TableController>();
                    // is table empty
                    if (!tableController.IsTaken())
                    {
                        customerController.TableTaken = customerController.TargetTable;
                        customerController.currentState = CUSTOMER_AI_STATE.WAIT_FOR_GROUP_MEMBER;
                    }
                    else
                    {
                        // is seat enough for whole group
                        if (tableController.GetNumOfSeatLeft() >= customerController.numOfCustomerInTheGroup)
                        {
                            customerController.TableTaken = customerController.TargetTable;
                            customerController.currentState = CUSTOMER_AI_STATE.WAIT_FOR_GROUP_MEMBER;

                        }
                        else
                        {
                            customerController.setPosition(tableController.GetStandUpPoint());
                            customerController.currentState = CUSTOMER_AI_STATE.WALK_AROUND;
                        }
                    }
                }
            }
            else if (customerController.currentState == CUSTOMER_AI_STATE.WAIT_FOR_GROUP_MEMBER)
            {
                // is the customer not a single customer
                if (customerController.numOfCustomerInTheGroup > 1)
                {
                    // sit down and wait for group member to buy food
                    customerController.DestroyGroupMarker();
                    customerController.StopMoving();
                    customerController.SitDown();
                    customerController.DestroyPatienceIndicator();
                    GameObject[] Group = GameObject.FindGameObjectsWithTag("Customer");

                    // ask all group member to go to buy food
                    for (int i = 0; i < Group.Length; i++)
                    {
                        GameObject member = Group[i];
                        CustomerController memberController = member.GetComponent<CustomerController>();
                        if (memberController.groupID == customerController.groupID && member != CustomerObj && member.tag != "Uncle Durain")
                        {
                            memberController.StopMoving();
                            memberController.DestroyGroupMarker();
                            memberController.TableTaken = customerController.TableTaken;
                            memberController.ChopeSeat();
                            memberController.DestroyPatienceIndicator();
                            memberController.currentState = CUSTOMER_AI_STATE.CHOOSE_STALL;
                        }

                        if (memberController.groupID == customerController.groupID && member.GetComponent<CustomerController>().currentState != CUSTOMER_AI_STATE.GO_TO_TABLE)
                        {
                            DeselectCustomer();
                        }
                    }
                    customerController.currentState = CUSTOMER_AI_STATE.CHOPE;
                }
                else
                {
                    // chope seat for single customer
                    customerController.DestroyGroupMarker();
                    customerController.ChopeTableForSingleCustomer();
                    customerController.DestroyPatienceIndicator();
                    customerController.currentState = CUSTOMER_AI_STATE.CHOOSE_STALL;
                }
            }
            else if (customerController.currentState == CUSTOMER_AI_STATE.CHOOSE_STALL)
            {
                // randomly choose a stall
                int StallIndex = Random.Range(0, NUM_OF_STALL);
                foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Stall"))
                {
                    StallController stallController = obj.GetComponent<StallController>();
                    if ((int)stallController.Type == StallIndex)
                    {
                        if (!stallController.IsQueueFull())
                        {
                            customerController.currentState = CUSTOMER_AI_STATE.GO_TO_STALL;
                            customerController.TargetStall = obj;
                            customerController.CurrentQueueNumber = stallController.GetTheLastQueueNumber();

                            if (stallController.GetLastQueueNumberWithCustomer() <= 1)
                            {
                                if (StallIndex == 3)
                                {
                                    CustomerFindPathAndMoveTo(CustomerObj,
                                                                stallController.GetStallQueuePoint(customerController.CurrentQueueNumber).x,
                                                                stallController.GetStallQueuePoint(customerController.CurrentQueueNumber).y);

                                }
                                else
                                {
                                    CustomerFindPathAndMoveTo(CustomerObj,
                                                               stallController.GetStallQueuePoint(0).x,
                                                               stallController.GetStallQueuePoint(0).y);
                                }
                            }
                            else
                            {
                                CustomerFindPathAndMoveTo(CustomerObj,
                                    stallController.GetStallQueuePoint(customerController.CurrentQueueNumber).x,
                                    stallController.GetStallQueuePoint(customerController.CurrentQueueNumber).y);

                            }
                        }
                    }
                }
            }
            else if (customerController.currentState == CUSTOMER_AI_STATE.GO_TO_STALL)
            {
                // go to stall
                if (customerController.IsReachTargetPoint() && customerController.IsMoving)
                {
                    StallController stallController = customerController.TargetStall.GetComponent<StallController>();
                    customerController.SetAnimation(CUSTOMER_ANIMATION_TYPE.IDLE);
                    if (!stallController.IsQueueFull())
                    {
                        stallController.CustomerEnterStall(CustomerObj);
                        customerController.currentState = CUSTOMER_AI_STATE.QUEUE;
                    }
                    else
                    {
                        customerController.currentState = CUSTOMER_AI_STATE.CHOOSE_STALL;
                    }
                }
            }
            else if (customerController.currentState == CUSTOMER_AI_STATE.QUEUE)
            {
                if (customerController.IsReachTargetPoint())
                {
                    StallController stallController = customerController.TargetStall.GetComponent<StallController>();
                    customerController.SetAnimation(CUSTOMER_ANIMATION_TYPE.IDLE);
                    if (customerController.CurrentQueueNumber > stallController.GetCustomerQueueNumber(CustomerObj))
                    {
                        customerController.CurrentQueueNumber = stallController.GetCustomerQueueNumber(CustomerObj);
                        customerController.StopMoving();

                        customerController.MoveTo(stallController.GetStallQueuePoint(customerController.CurrentQueueNumber).x,
                                                  stallController.GetStallQueuePoint(customerController.CurrentQueueNumber).y);
                    }
                    else
                    {
                        if (customerController.CurrentQueueNumber == 0)
                        {
                            customerController.currentState = CUSTOMER_AI_STATE.BUY_FOOD;
                        }
                    }
                }
            }
            else if (customerController.currentState == CUSTOMER_AI_STATE.BUY_FOOD)
            {
                if (customerController.IsReachTargetPoint())
                {
                    customerController.CreateAndShowOrder();
                    customerController.SetAnimation(CUSTOMER_ANIMATION_TYPE.IDLE);
                    customerController.currentState = CUSTOMER_AI_STATE.ORDERING;
                }
            }
            else if (customerController.currentState == CUSTOMER_AI_STATE.ORDERING)
            {
                OrderController orderController = customerController.OrderObj.GetComponent<OrderController>();

                orderController.isExtraMoneyActivated = two_times;
                
                if (orderController.IsOrderConfirmed)
                {
                    if (orderController.type == ORDER_TYPE.ICE_KACHANG)
                    {
                        //print ("icekacang");
                        IceKacangObj = (GameObject)Instantiate(IceKacangPrefab,
                                                               IceKacangPrefab.transform.position,
                                                               IceKacangPrefab.transform.rotation);
                        /*ChickenRiceObj = (GameObject)Instantiate(ChickenRicePrefab,
                                                                 ChickenRicePrefab.transform.position,
                                                                 ChickenRicePrefab.transform.rotation);*/
                        isSatay = false;
                    }
                    else if (orderController.type == ORDER_TYPE.CHICKEN_RICE)
                    {
                        ChickenRiceObj = (GameObject)Instantiate(ChickenRicePrefab,
                                                                 ChickenRicePrefab.transform.position,
                                                                 ChickenRicePrefab.transform.rotation);
                        isSatay = false;
                    }
                    else if (orderController.type == ORDER_TYPE.NASI_LEMAK)
                    {
                        NasiLemakObj = (GameObject)Instantiate(NasiLemakPrefab,
                                                               NasiLemakPrefab.transform.position,
                                                               NasiLemakPrefab.transform.rotation);
                        isSatay = false;
                    }
                    else if (orderController.type == ORDER_TYPE.SATAY)
                    {
                        SatayObj = (GameObject)Instantiate(SatayPrefab,
                                                           SatayPrefab.transform.position,
                                                           SatayPrefab.transform.rotation);

                        isSatay = true;
                    }
                    else if (orderController.type == ORDER_TYPE.ROJAK) //#rojak
                    {
                        //ChickenRiceObj = (GameObject)Instantiate(ChickenRicePrefab,
                        //                                         ChickenRicePrefab.transform.position,
                        //                                         ChickenRicePrefab.transform.rotation);
                        RojakObj = (GameObject)Instantiate(RojakPrefab,
                                                           RojakPrefab.transform.position,
                                                           RojakPrefab.transform.rotation);

                        isSatay = false;
                    }
                    //else if (orderController.type == ORDER_TYPE.TEHTARIK)
                    //{
                    //    TehTarikObj = (GameObject)Instantiate(TehTarikPrefab,
                    //                                       TehTarikPrefab.transform.position,
                    //                                       TehTarikPrefab.transform.rotation);
                    //    isSatay = false;
                    //}

                    if (orderController.type != ORDER_TYPE.NORMAL)
                        IsInMiniGameMode = true;

                    customerController.currentState = CUSTOMER_AI_STATE.WAIT_FOR_FOOD;
                    customerController.SendOrder();
                    customerController.DestroyOrder();

                }
            }
            else if (customerController.currentState == CUSTOMER_AI_STATE.WAIT_FOR_FOOD)
            {
                if (IsInMiniGameMode)
                {
                    if (IceKacangObj != null)
                    {
                        IceKaCangMiniGame iceKaCangMiniGame = IceKacangObj.GetComponent<IceKaCangMiniGame>();
                        if (iceKaCangMiniGame.IsMiniGameFinish())
                        {
                            IsInMiniGameMode = false;

                            //Visual Feedback
                            MGFeedbackObj = (GameObject)Instantiate(MGFeedbackPrefab,
                                                                    MGFeedbackPrefab.transform.position,
                                                                    MGFeedbackPrefab.transform.rotation);

                            minigameFeedback temp = MGFeedbackObj.GetComponent<minigameFeedback>();

                            temp.m_brender = true;
                            temp.m_bsuccess = iceKaCangMiniGame.order_fulfilled;

                            if (iceKaCangMiniGame.order_fulfilled)
                            {
                                GameObject.Find("PointsCarrying").GetComponent<PointsCarry>().points += ICE_KACANG_REWARD_POINTS;
                            }
                        }
                    }
                    else if (ChickenRiceObj != null)
                    {
                        ChickenRiceMiniGame chickenRiceMiniGame = ChickenRiceObj.GetComponent<ChickenRiceMiniGame>();


                        if (chickenRiceMiniGame.IsMiniGameFinish())
                        {
                            IsInMiniGameMode = false;

                            //Visual Feedback
                            MGFeedbackObj = (GameObject)Instantiate(MGFeedbackPrefab,
                                                                    MGFeedbackPrefab.transform.position,
                                                                    MGFeedbackPrefab.transform.rotation);

                            minigameFeedback temp = MGFeedbackObj.GetComponent<minigameFeedback>();

                            temp.m_brender = true;
                            temp.m_bsuccess = chickenRiceMiniGame.order_fulfilled;

                            print(chickenRiceMiniGame.order_fulfilled.ToString());
                            print(temp.m_bsuccess.ToString());
                            if (chickenRiceMiniGame.order_fulfilled)
                            {

                                GameObject.Find("PointsCarrying").GetComponent<PointsCarry>().points += CHICKEN_RICE_REWARD_POINTS;
                            }
                        }
                    }
                    else if (NasiLemakObj != null)
                    {
                        NasiLemakMiniGame nasiLemakMiniGame = NasiLemakObj.GetComponent<NasiLemakMiniGame>();

                        if (nasiLemakMiniGame.IsMiniGameFinish())
                        {
                            IsInMiniGameMode = false;

                            //Visual Feedback
                            MGFeedbackObj = (GameObject)Instantiate(MGFeedbackPrefab,
                                                                    MGFeedbackPrefab.transform.position,
                                                                    MGFeedbackPrefab.transform.rotation);

                            minigameFeedback temp = MGFeedbackObj.GetComponent<minigameFeedback>();

                            temp.m_brender = true;
                            temp.m_bsuccess = nasiLemakMiniGame.order_fulfilled;

                            print(nasiLemakMiniGame.order_fulfilled.ToString());
                            print(temp.m_bsuccess.ToString());

                            if (nasiLemakMiniGame.order_fulfilled)
                                GameObject.Find("PointsCarrying").GetComponent<PointsCarry>().points += NASILEMAK_REWARD_POINTS;
                        }
                    }
                    else if (SatayObj != null)
                    {
                        SatayGrillMiniGame satayMiniGame = SatayObj.GetComponent<SatayGrillMiniGame>();
                        if (satayMiniGame.IsMiniGameFinish())
                        {
                            IsInMiniGameMode = false;

                            //Visual Feedback
                            MGFeedbackObj = (GameObject)Instantiate(MGFeedbackPrefab,
                                                                    MGFeedbackPrefab.transform.position,
                                                                    MGFeedbackPrefab.transform.rotation);

                            minigameFeedback temp = MGFeedbackObj.GetComponent<minigameFeedback>();

                            temp.m_brender = true;
                            temp.m_bsuccess = satayMiniGame.order_fulfilled;

                            print(satayMiniGame.order_fulfilled.ToString());
                            print(temp.m_bsuccess.ToString());

                            if (satayMiniGame.order_fulfilled)
                            {
                                //Add Points
                                GameObject.Find("PointsCarrying").GetComponent<PointsCarry>().points += SATAY_REWARD_POINTS;

                            }


                        }
                    }
                    else if (RojakObj != null)//#rojak
                    {
                        //ChickenRiceMiniGame chickenRiceMiniGame = ChickenRiceObj.GetComponent<ChickenRiceMiniGame>();
                        RojakMiniGame rojakMiniGame = RojakObj.GetComponent<RojakMiniGame>();

                        if (rojakMiniGame.IsMiniGameFinish())
                        {
                            IsInMiniGameMode = false;

                            //Visual Feedback
                            MGFeedbackObj = (GameObject)Instantiate(MGFeedbackPrefab,
                                                                    MGFeedbackPrefab.transform.position,
                                                                    MGFeedbackPrefab.transform.rotation);

                            minigameFeedback temp = MGFeedbackObj.GetComponent<minigameFeedback>();

                            temp.m_brender = true;
                            temp.m_bsuccess = rojakMiniGame.order_fulfilled;

                            print(rojakMiniGame.order_fulfilled.ToString());
                            print(temp.m_bsuccess.ToString());
                            if (rojakMiniGame.order_fulfilled)
                            {

                                GameObject.Find("PointsCarrying").GetComponent<PointsCarry>().points += ROJAK_REWARD_POINTS;
                            }
                        }
                    }
                    //else if (TehTarikObj != null)
                    //{
                    //    TehTarikMiniGame tehTarikMiniGame = TehTarikObj.GetComponent<TehTarikMiniGame>();
                    //    if (tehTarikMiniGame.IsMiniGameFinish())
                    //    {
                    //        IsInMiniGameMode = false;



                    //        //Visual Feedback
                    //        MGFeedbackObj = (GameObject)Instantiate(MGFeedbackPrefab,
                    //                                                MGFeedbackPrefab.transform.position,
                    //                                                MGFeedbackPrefab.transform.rotation);

                    //        minigameFeedback temp = MGFeedbackObj.GetComponent<minigameFeedback>();

                    //        temp.m_brender = true;
                    //        temp.m_bsuccess = tehTarikMiniGame.order_fulfilled;

                    //        print(tehTarikMiniGame.order_fulfilled.ToString());
                    //        print(temp.m_bsuccess.ToString());

                    //        if (tehTarikMiniGame.order_fulfilled)
                    //        {
                    //            //Add Points
                    //            GameObject.Find("PointsCarrying").GetComponent<PointsCarry>().points += TEHTARIK_REWARD_POINTS;

                    //        }
                    //    }
                    //}
                }
                else
                {
                    if (customerController.CheckIsFoodFinished())
                    {
                        customerController.TargetStall.GetComponent<StallController>().TheFirstCustomerLeaveStall();
                        customerController.currentState = CUSTOMER_AI_STATE.GO_TO_SEAT;
                        CustomerFindPathAndMoveTo(CustomerObj, customerController.GetTableSitDownPoint().x, customerController.GetTableSitDownPoint().y);
                    }
                }
            }
            else if (customerController.currentState == CUSTOMER_AI_STATE.GO_TO_SEAT)
            {
                if (customerController.IsReachTargetPoint())
                {
                    customerController.StopMoving();
                    customerController.SitDown();
                    customerController.currentState = CUSTOMER_AI_STATE.EAT;
                    customerController.Eat();
                    if (customerController.numOfCustomerInTheGroup <= 1)
                    {
                        customerController.RemoveChopeObj();
                    }
                }
            }
            else if (customerController.currentState == CUSTOMER_AI_STATE.CHOPE)
            {
                GameObject[] Group = GameObject.FindGameObjectsWithTag("Customer");

                if (Group.Length > 0)
                {
                    for (int i = 0; i < Group.Length; i++)
                    {
                        if (Group[i] != null)
                        {
                            GameObject member = Group[i];
                            CustomerController memberController = member.GetComponent<CustomerController>();
                            if (memberController.groupID == customerController.groupID && member != CustomerObj)
                            {
                                if (memberController.currentState == CUSTOMER_AI_STATE.EAT)
                                {
                                    customerController.StandUp();
                                    customerController.currentState = CUSTOMER_AI_STATE.CHOOSE_STALL;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            else if (customerController.currentState == CUSTOMER_AI_STATE.EAT)
            {
                if (customerController.CheckIsShiokBubbleSpawnDelayEnded())
                {
                    if (!customerController.IsSitWithDurainUncle() || customerController.type == CUSTOMER_TYPE.DURAIN_UNCLE)
                        customerController.CreateAndShowShiokBubble();
                }

                if (customerController.IsFinishEating())
                {
                    customerController.TableTaken.GetComponent<TableController>().NumOfCustomerAreEating--;
                    customerController.StandUp();
                    //pointone
                    customerController.TrayTableForSingleCustomer();
                    customerController.currentState = CUSTOMER_AI_STATE.LEAVE;
                }
            }
            else if (customerController.currentState == CUSTOMER_AI_STATE.LEAVE)
            {
                customerController.StopMoving();
                NumOfGroupMember[customerController.groupID]--;
                if (customerController.IsEnterFromLeftSide)
                    CustomerFindPathAndMoveTo(CustomerObj, spawnPointLeft.x, spawnPointLeft.y);
                else
                    CustomerFindPathAndMoveTo(CustomerObj, spawnPointRight.x, spawnPointRight.y);
                customerController.currentState = CUSTOMER_AI_STATE.REMOVE;
            }
            else if (customerController.currentState == CUSTOMER_AI_STATE.REMOVE)
            {
                if (customerController.IsReachThePoint(spawnPointLeft))
                {
                    customerController.StopMoving();
                    customerController.MoveTo(GroupSpawnerLeft.transform.position.x,
                                              GroupSpawnerLeft.transform.position.y);
                }
                else if (customerController.IsReachThePoint(spawnPointRight))
                {
                    customerController.StopMoving();
                    customerController.MoveTo(GroupSpawnerRight.transform.position.x,
                                              GroupSpawnerRight.transform.position.y);
                }
                else if (customerController.IsReachThePoint(GroupSpawnerLeft.transform.position))
                {
                    customerController.DestroySelf();
                }
                else if (customerController.IsReachThePoint(GroupSpawnerRight.transform.position))
                {
                    customerController.DestroySelf();
                }
            }
        }
    }

    private void TryChopingTheTable(GameObject Table)
    {
        GameObject sound = GameObject.Find("AudioManager");
        SoundManager selectTSound = sound.GetComponent<SoundManager>();

        if (theSelectedCustomer)
        {
            selectTSound.SelectTableSound();
            CustomerController customerController = theSelectedCustomer.GetComponent<CustomerController>();
            customerController.currentState = CUSTOMER_AI_STATE.CHOPE_A_TABLE;
            TableController tableController = Table.GetComponent<TableController>();
            customerController.TargetTable = Table;
            customerController.StopMoving();
            CustomerFindPathAndMoveTo(theSelectedCustomer, tableController.GetSitDownPoint().x, tableController.GetSitDownPoint().y);
        }
    }

    private void SelectCustomer(GameObject Customer)
    {
        GameObject sound = GameObject.Find("AudioManager");
        SoundManager selectCSound = sound.GetComponent<SoundManager>();

        CustomerController customerController = Customer.GetComponent<CustomerController>();
        if (customerController.currentState == CUSTOMER_AI_STATE.WALK_AROUND)
        {
            selectCSound.SelectCustomerSound();
            theSelectedCustomer = Customer;
            customerController.CreateAndShowTapIndicator();
            customerController.controlState = CUSTOMER_CONTROL_STATE.PLAYER_SELECT;
        }
    }

    private void DeselectCustomer()
    {
        if (theSelectedCustomer != null)
        {
            CustomerController customerController = theSelectedCustomer.GetComponent<CustomerController>();
            customerController.DestoryTapIndicator();
            customerController.controlState = CUSTOMER_CONTROL_STATE.AI_CONTROL;
            theSelectedCustomer = null;
        }
    }

    private void PlayerControl()
    {


        if (Input.GetMouseButtonDown(0) && !IsInMiniGameMode)
        {
            RaycastHit hitInfo = new RaycastHit();
            bool hit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo);
            if (hit)
            {
                if (hitInfo.transform.gameObject.tag == "Tray")
                {
                    Destroy(hitInfo.transform.gameObject);
                }
                else if (hitInfo.transform.gameObject.tag == "Customer")
                {
                    DeselectCustomer();
                    SelectCustomer(hitInfo.transform.gameObject);
                }
                else if (hitInfo.transform.gameObject.name.Contains("Table"))
                {
                    if (theSelectedCustomer != null)
                    {
                        //todo2
                        TryChopingTheTable(hitInfo.transform.gameObject);
                        DeselectCustomer();

                    }
                }
                else if (hitInfo.transform.gameObject.tag == "Order")
                {
                    hitInfo.transform.gameObject.GetComponent<OrderController>().ConfirmOrder();
                }

            }
            else
            {
                DeselectCustomer();
            }
        }
    }

    // Update is called once per frame
    private void Update()
    {
        //Debug.Log("Delay: " + GROUP_SPAWN_DELAY);

        SpawnCustomerGroup();
        PlayerControl();
        UpdateAI();
        PeakHour();
    }
}