﻿using UnityEngine;
using System.Collections;

public enum ORDER_TYPE
{
    ICE_KACHANG,
    NASI_LEMAK,
    CHICKEN_RICE,
    SATAY,
    ROJAK,
    TEHTARIK,
    NORMAL
};

public class OrderController : MonoBehaviour
{
    private const int SPECIAL_ORDER_CHANCE = 90;
    public Sprite[] NormalOrderSprites;
    public Sprite[] SpecialOrderSprites;
    private GameObject theCustomer;
    public ORDER_TYPE type;

    
    public bool isExtraMoneyActivated = false;

    //Order
    public bool IsOrderConfirmed;

    // Use this for initialization
    public void Set(GameObject Customer)
    {
        theCustomer = Customer;
        type = ORDER_TYPE.NORMAL;
        gameObject.GetComponent<SpriteRenderer>().sprite = NormalOrderSprites[Random.Range(0, 3)];
        if (Random.Range(0, 100) < SPECIAL_ORDER_CHANCE)
        {
            CustomerController customerController = theCustomer.GetComponent<CustomerController>();
            StallController stallController = customerController.TargetStall.GetComponent<StallController>();
            if (stallController.Type == STALL_TYPE.DRINK)
            {
                type = ORDER_TYPE.ICE_KACHANG;
            }
            else if (stallController.Type == STALL_TYPE.CHINESE)
            {
                type = ORDER_TYPE.CHICKEN_RICE;
            }
            else if (stallController.Type == STALL_TYPE.MALAY)
            {
                type = ORDER_TYPE.NASI_LEMAK;
            }
            else if (stallController.Type == STALL_TYPE.SATAY)
            {
                type = ORDER_TYPE.SATAY;
            }
            else if (stallController.Type == STALL_TYPE.ROJAK)
            {
                type = ORDER_TYPE.ROJAK;
            }
            //else if (stallController.Type == STALL_TYPE.TEHTARIK)
            //{
            //    type = ORDER_TYPE.TEHTARIK;
            //}
            gameObject.GetComponent<SpriteRenderer>().sprite = SpecialOrderSprites[(int)type];
        }
        //gameObject.transform.position = theCustomer.GetComponent<CustomerController>().GetOverHeadPosition();
        gameObject.transform.position = theCustomer.GetComponent<CustomerController>().GetSideHeadPosition();
    }

    public void ConfirmOrder()
    {
        IsOrderConfirmed = true;

        GameObject go = GameObject.Find("ShiokBarUI");
        GameObject go2 = GameObject.Find("PointsCarrying");
        GameObject sound = GameObject.Find("AudioManager");
        ShiokBar shiokBar = go.GetComponent<ShiokBar>();
        PointsCarry moneyNow = go2.GetComponent<PointsCarry>();
        SoundManager orderSound = sound.GetComponent<SoundManager>();

        shiokBar.combo++;
        shiokBar.comboImage++;
        if (!isExtraMoneyActivated)
            moneyNow.money += 100;
        else
            moneyNow.money += 200;

        orderSound.GameOrders();

        if (shiokBar.combo > 15)
        {
            shiokBar.combo = 15;
            shiokBar.comboImage = 14;
        }
    }

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
