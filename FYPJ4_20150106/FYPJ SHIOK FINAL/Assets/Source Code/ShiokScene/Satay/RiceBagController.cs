﻿using UnityEngine;
using System.Collections;

public class RiceBagController : MonoBehaviour
{

    private GameObject satayPlate;

    public bool IsInsideSet = false;
    // Use this for initialization
    void Start()
    {
        satayPlate = GameObject.Find("plateObj(Clone)");
        collider.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (satayPlate != null)
        {
            if (satayPlate.collider.bounds.Contains(gameObject.transform.position))
            {
                IsInsideSet = true;

            }
            else 
            {
                IsInsideSet = false;
            }
        }
    }
}
