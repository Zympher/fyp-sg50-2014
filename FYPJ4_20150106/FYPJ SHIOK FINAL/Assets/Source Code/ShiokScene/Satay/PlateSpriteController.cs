﻿using UnityEngine;
using System.Collections;

public class PlateSpriteController : MonoBehaviour {

	private SpriteRenderer sr;

	[SerializeField]
	private GameObject satay_mini;

	[SerializeField]
	private Sprite[] plate_update;

	// Use this for initialization
	void Start () {

		satay_mini = GameObject.Find ("Satay(Clone)");
		sr = GetComponent<SpriteRenderer> ();
		sr.sprite = plate_update [satay_mini.GetComponent<SatayGrillMiniGame> ().num_of_satay_in_plate];

	}
	
	// Update is called once per frame
	void Update () 
	{
		// if (satay_mini.GetComponent<SatayGrillMiniGame> ().num_of_satay_in_plate) 
		//{
			//print ("here");
			sr.sprite = plate_update [satay_mini.GetComponent<SatayGrillMiniGame> ().num_of_satay_in_plate];
		//}
//		else if (satay_mini.GetComponent<SatayGrillMiniGame> ().satay_order_type == SATAY_SET_TYPE.NO_RICE && satay_mini.GetComponent<SatayGrillMiniGame>().NUM_OF_ORDERED_SATAY == 5) 
//			{
//				sr.sprite = plate_update [0];
//			}
//
//		else if ( satay_mini.GetComponent<SatayGrillMiniGame> ().satay_order_type == SATAY_SET_TYPE.WITH_RICE && satay_mini.GetComponent<SatayGrillMiniGame>().NUM_OF_ORDERED_SATAY == 5 
//		           && satay_mini.GetComponent<SatayGrillMiniGame> ().num_of_satay_in_plate == satay_mini.GetComponent<SatayGrillMiniGame> ().NUM_OF_ORDERED_SATAY 
//		           && satay_mini.GetComponent<SatayGrillMiniGame> ().rice_placed == true  )
//		{
//			sr.sprite = plate_update [3];
//		}
//
//		else if (satay_mini.GetComponent<SatayGrillMiniGame> ().satay_order_type == SATAY_SET_TYPE.WITH_RICE && satay_mini.GetComponent<SatayGrillMiniGame>().NUM_OF_ORDERED_SATAY == 5) 
//		{
//			sr.sprite = plate_update [2];
//		} 
//
//
//		else if (satay_mini.GetComponent<SatayGrillMiniGame> ().satay_order_type == SATAY_SET_TYPE.NO_RICE && satay_mini.GetComponent<SatayGrillMiniGame>().NUM_OF_ORDERED_SATAY == 10
//		         && satay_mini.GetComponent<SatayGrillMiniGame> ().num_of_satay_in_plate == satay_mini.GetComponent<SatayGrillMiniGame> ().NUM_OF_ORDERED_SATAY) 
//		{
//			sr.sprite = plate_update [5];
//		}
//		else if (satay_mini.GetComponent<SatayGrillMiniGame> ().satay_order_type == SATAY_SET_TYPE.NO_RICE && satay_mini.GetComponent<SatayGrillMiniGame>().NUM_OF_ORDERED_SATAY == 10) 
//		{
//			sr.sprite = plate_update [4];
//		}
//
//
//		else if (satay_mini.GetComponent<SatayGrillMiniGame> ().satay_order_type == SATAY_SET_TYPE.WITH_RICE && satay_mini.GetComponent<SatayGrillMiniGame>().NUM_OF_ORDERED_SATAY == 10 
//		           && satay_mini.GetComponent<SatayGrillMiniGame> ().num_of_satay_in_plate == satay_mini.GetComponent<SatayGrillMiniGame> ().NUM_OF_ORDERED_SATAY 
//		           && satay_mini.GetComponent<SatayGrillMiniGame> ().rice_placed == true )
//		{
//			sr.sprite = plate_update [7];
//		}
//
//		else if (satay_mini.GetComponent<SatayGrillMiniGame> ().satay_order_type == SATAY_SET_TYPE.WITH_RICE && satay_mini.GetComponent<SatayGrillMiniGame>().NUM_OF_ORDERED_SATAY == 10) 
//		{
//			sr.sprite = plate_update [6];
//		} 

	}
}
