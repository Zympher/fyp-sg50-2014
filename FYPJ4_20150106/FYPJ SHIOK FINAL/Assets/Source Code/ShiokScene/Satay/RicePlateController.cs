﻿using UnityEngine;
using System.Collections;

public class RicePlateController : MonoBehaviour {

	private GameObject temp;

	private SpriteRenderer sr;

	[SerializeField]
	private Sprite[] sprite_list;
	
	// Use this for initialization
	void Start () {
		sr = GetComponent<SpriteRenderer> ();
		temp = GameObject.Find ("Satay(Clone)");
	
	}
	
	// Update is called once per frame
	void Update () {
	if (temp.GetComponent<SatayGrillMiniGame> ().satay_order_type == SATAY_SET_TYPE.WITH_RICE) {
						if (temp.GetComponent<SatayGrillMiniGame> ().rice_placed == true) {
								sr.sprite = sprite_list [1];
						} else {
								sr.sprite = sprite_list [0];
						}
				}
		else {
			sr.renderer.enabled = false;
				}
	}
}
