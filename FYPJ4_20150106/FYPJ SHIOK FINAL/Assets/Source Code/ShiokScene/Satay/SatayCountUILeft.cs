﻿using UnityEngine;
using System.Collections;

public class SatayCountUILeft : MonoBehaviour {

	private SpriteRenderer sr;
	
	private GameObject SatayMini;
	private SatayGrillMiniGame satay_temp;
	
	public Sprite [] cooked_satay_UI;

	// Use this for initialization
	void Start () {
	
		sr = GetComponent<SpriteRenderer> ();

		SatayMini = GameObject.Find ("Satay(Clone)");
		satay_temp = SatayMini.GetComponent<SatayGrillMiniGame> ();

	}
	
	// Update is called once per frame
	void Update () {
		sr.sprite = cooked_satay_UI [satay_temp.num_of_satay_in_plate];
	}
}
