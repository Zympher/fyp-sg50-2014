﻿using UnityEngine;
using System.Collections;

public class ShiokBar : MonoBehaviour
{
    //Size of shiok bar
    public float maxShiok = 1000;
    public float curShiok = 0;

    private float shiokBarMain = 517f;

	//Win/Lose popup textures
	public Texture2D gameWin, gameLose;
    //SHIOK MODE TEXTURE
    public Texture2D shiokModeTex;
    //textures for shiok meter
    public Texture2D bgImage, fgImage, frame, comboFrame;
	// Skill bar icon textures
	public Texture2D skillbarTex, trayTex, orderTex, kallangTex, fillTex;

	// Skill bar node fill positions
	private Rect TrayNodePos, OrderNodePos, FourthNodePos, FifthNodePos, KWNodePos, skillbarPos;
	private Rect TrayTexPos, OrderTexPos, KWTexPos;

	// Variables for skill bar node fill scaling
	private float scaleWidth, scaleHeight;

    //Combo textures
    public int combo = 1;
    public Texture2D[] multiplier;
    public int comboImage = 0;

    // lvl textures
    public Texture2D[] lvlBase;
    public int lvlup = 0;

    //Game over counter
    public Texture2D gameOverCount;
    public int gameOverText;

    //public KallangWave kgW;
    public ShiokAI sAI;
    public PointsCarry pp;

    // Power up control
    public bool buttonOpen = true;
    public bool buttonOpen2 = true;

    // points
    //public int points = 0;
    public Texture2D scoreBase;
    public int lvl = 1;
    public bool shiokMode = false;

    //font manipulation
    public GUISkin guiSkin;
    private GUIStyle largeFont, largeScore, gameOverNum;
	private GUIStyle TrayBtn, OrderBtn, KWBtn, Btn4, Btn5, skillBar;
	private GUIStyle TrayTexGUI, OrderTexGUI, KWTexGUI;

    //shiok mode timer
    public int shiokTimer;
    public float counter;

    private int screenW = Screen.width;
    private int screenH = Screen.height;

    GameObject mapCheck;
    SelectedMap getMap;

    GameObject getTimer;
    Timer timingNow;

    GameObject userData;
    userDataScript user_data;

    GameObject NameBoxPrompter;
	NameBoxPrompt name_box_pt;



    // Shiok mode waiting time
    IEnumerator Wait()
    {
        yield return new WaitForSeconds(2.0f);

        // reset combo multiplier to 1
        combo = 1;
        comboImage = 0;

        curShiok -= 600 * Time.deltaTime;

        if ((lvl == 1) && (curShiok < 0))
        {
            shiokMode = false;
        }
        else if (curShiok < 0)
        {
            curShiok = 1000;
            lvl--;
            lvlup--;
        }

        if (lvl < 1)
        {
            lvl = 1;
            lvlup = 0;
        }
    }

    // Use this for initialization
    void Start()
    {

		scaleWidth = Screen.width / 1920.0f;
		scaleHeight = Screen.height / 1080.0f;

		TrayBtn = guiSkin.FindStyle("TrayBtn");
		OrderBtn = guiSkin.FindStyle("OrderBtn");
		KWBtn = guiSkin.FindStyle("KWBtn");
		Btn4 = guiSkin.FindStyle("Btn4");
		Btn5 = guiSkin.FindStyle("Btn5");
		skillBar = guiSkin.FindStyle("skillBar");

		TrayTexGUI = guiSkin.FindStyle("TrayTexGUI");
		OrderTexGUI = guiSkin.FindStyle("OrderTexGUI");
		KWTexGUI = guiSkin.FindStyle("KWTexGUI");

		// Initialise skill bar node fill positions
		TrayNodePos = new Rect(622 * scaleWidth, 952 * scaleHeight, TrayBtn.fixedWidth * scaleWidth, TrayBtn.fixedHeight * scaleHeight);
		OrderNodePos = new Rect(759 * scaleWidth, 827 * scaleHeight, OrderBtn.fixedWidth * scaleWidth, OrderBtn.fixedHeight * scaleHeight);
		KWNodePos = new Rect(905 * scaleWidth, 953 * scaleHeight, KWBtn.fixedWidth * scaleWidth, KWBtn.fixedHeight * scaleHeight);
		FourthNodePos = new Rect(1042 * scaleWidth, 827 * scaleHeight, Btn4.fixedWidth * scaleWidth, Btn5.fixedHeight * scaleHeight);
		FifthNodePos = new Rect(1190 * scaleWidth, 953 * scaleHeight, Btn5.fixedWidth * scaleWidth, Btn5.fixedHeight * scaleHeight);
		skillbarPos = new Rect(615 * scaleWidth, 825 * scaleHeight, skillBar.fixedWidth * scaleWidth, skillBar.fixedHeight * scaleHeight);

		TrayTexPos = new Rect(625 * scaleWidth, 920 * scaleHeight, TrayTexGUI.fixedWidth * scaleWidth, TrayTexGUI.fixedHeight * scaleHeight);
		OrderTexPos = new Rect(778 * scaleWidth, 836 * scaleHeight, OrderTexGUI.fixedWidth * scaleWidth, OrderTexGUI.fixedHeight * scaleHeight);
		KWTexPos = new Rect(920 * scaleWidth, 944 * scaleHeight, KWTexGUI.fixedWidth * scaleWidth, KWTexGUI.fixedHeight * scaleHeight);

        //fonts
        largeFont = new GUIStyle();
        largeScore = new GUIStyle();
        gameOverNum = new GUIStyle();
        largeScore.fontSize = 33;
        gameOverNum.fontSize = 33;
        largeFont.normal.textColor = Color.white;
        largeScore.normal.textColor = Color.blue;
        gameOverNum.normal.textColor = Color.white;

        shiokTimer = 0;
        gameOverText = 14;

        if (screenW == 1920 && screenH == 1080)
        {
            largeScore.fontSize = 33;
        }
        mapCheck = GameObject.Find("selectedMap");
        getMap = mapCheck.GetComponent<SelectedMap>();

        getTimer = GameObject.Find("Pause");
        timingNow = getTimer.GetComponent<Timer>();

        userData = GameObject.Find("Empty User");
        user_data = userData.GetComponent<userDataScript>();

        NameBoxPrompter = GameObject.Find("NameBoxPrompt");
        name_box_pt = NameBoxPrompter.GetComponent<NameBoxPrompt>();

	}

    // Update is called once per frame
    void Update()
    {

        AdjustCurrentShiok(0);





        if (Input.GetKeyDown(KeyCode.L))
        {
            curShiok += (combo * 5);

            // Conditions for shiok bar
            if ((curShiok > maxShiok) && (lvl == 3))
            {
                curShiok = 1000;
            }
            else if (curShiok > maxShiok)
            {
                curShiok = 0;
                lvl++;
                lvlup++;
            }

            // set level amount
            if (lvl > 3)
            {
                lvl = 3;
                lvlup = 2;
            }

        }

        if (Input.GetKey(KeyCode.A))
        {
            curShiok += (combo * 5);

            // Conditions for shiok bar
            if ((curShiok > maxShiok) && (lvl == 3))
            {
                curShiok = 1000;
            }
            else if (curShiok > maxShiok)
            {
                curShiok = 0;
                lvl++;
                lvlup++;
            }

            // set level amount
            if (lvl > 3)
            {
                lvl = 3;
                lvlup = 2;
            }
        }

        //For Shiok Mode
        // input codes OR call functions that you want shiok mode to be used on
        // E.G: Auto Serve, Instant cleaned tables, etc
        if ((lvl == 3) && (curShiok == 1000)) // Key to activate shiok mode
        {
            if (shiokMode == false)
            {
                shiokMode = true;
            }
        }

        // shiok mode activated
        if (shiokMode)
        {
            StartCoroutine(Wait());
        }
    }

    void GetNode(int node)  // Renders skill bar fill (Red background)
    {
        switch (node)
        {
            case 1: GUI.DrawTexture(TrayNodePos, fillTex);
                break;
            case 2: GUI.DrawTexture(OrderNodePos, fillTex);
                break;
            case 3: GUI.DrawTexture(KWNodePos, fillTex);
                break;
            case 4: GUI.DrawTexture(FourthNodePos, fillTex);
                break;
            case 5: GUI.DrawTexture(FifthNodePos, fillTex);
                break;
            default:
                Debug.Log("Skill bar fill : Rendering error");
                break;
        }
    }

    void RenderSkillbarNodes()
    {
        if ( curShiok >= 50 && curShiok <= 200 )
        {
            GetNode(1);

			if (GUI.Button(TrayTexPos, GUIContent.none, guiSkin.GetStyle("TrayBtn")))
			{
				DestroyTrays();
			}
        }
        else if ( curShiok >= 200 && curShiok <= 350 )
        {
            GetNode(1);
            GetNode(2);

			if (GUI.Button(TrayTexPos, GUIContent.none, guiSkin.GetStyle("TrayBtn")))
			{
				DestroyTrays();
			}
			if (GUI.Button(OrderTexPos, GUIContent.none, guiSkin.GetStyle("OrderBtn")))
			{
			}
        }
        else if ( curShiok >= 350 && curShiok <= 500 )
        {
            GetNode(1);
            GetNode(2);
            GetNode(3);
			if (GUI.Button(TrayTexPos, GUIContent.none, guiSkin.GetStyle("TrayBtn")))
			{
				DestroyTrays();
			}
			if (GUI.Button(OrderTexPos, GUIContent.none, guiSkin.GetStyle("OrderBtn")))
			{
			}
			if (GUI.Button(KWTexPos, GUIContent.none, guiSkin.GetStyle("KWBtn")))
			{
			}
        }
        else if (curShiok >= 500 && curShiok <= 650)
        {
            GetNode(1);
            GetNode(2);
            GetNode(3);
            GetNode(4);
        }
        else if (curShiok >= 650 && curShiok <= 900)
        {
            GetNode(1);
            GetNode(2);
            GetNode(3);
            GetNode(4);
            GetNode(5);
        }
    }

    void RenderSkillBar()
    {
      
        //Skill Bar background
        //GUI.DrawTexture(new Rect((Screen.width * 560) / 1920, (Screen.height * 820) / 1080, (Screen.width * 780) / 1920, (Screen.height * 250) / 1080), skillbarTex);
		GUI.DrawTexture(skillbarPos, skillbarTex);

        //Skill bar fill
        RenderSkillbarNodes();

        //Skill bar icons
        GUI.DrawTexture(TrayTexPos, trayTex);
        GUI.DrawTexture(OrderTexPos, orderTex);
		GUI.DrawTexture(KWTexPos, kallangTex);
    }

    //GUI
    void OnGUI()
    {
        ////////////////////////////////////////
        ///                TEXT              ///
        ////////////////////////////////////////

        //For custom fonts
        GUI.skin = guiSkin;

        if (sAI.IsInMiniGameMode == false)
        {
            RenderSkillBar();

            //Score Base
            GUI.DrawTexture(new Rect((Screen.width * 21) / 1920, (Screen.height * 9) / 1080, (Screen.width * 256) / 1920, (Screen.height * 130) / 1080), scoreBase);

            // Points
            GUI.Label(new Rect((Screen.width * 60) / 1920, (Screen.height * 27) / 1080, (Screen.width * 120) / 1920, (Screen.height * 50) / 1080), "" + pp.points, largeScore);

            //Game over Counter
            GUI.DrawTexture(new Rect((Screen.width * 1671) / 1920, (Screen.height * 8) / 1080, (Screen.width * 246) / 1920, (Screen.height * 188) / 1080), gameOverCount);

            // Text game over counter
            GUI.Label(new Rect((Screen.width * 1800) / 1920, (Screen.height * 45) / 1080, (Screen.width * 120) / 1920, (Screen.height * 50) / 1080), gameOverText + "/15", gameOverNum);
        }




        GameObject moneyHave = GameObject.Find("PointsCarrying");
        PointsCarry moneyNow = moneyHave.GetComponent<PointsCarry>();
        pp = moneyNow;

        if (getMap.getSelectedMap())
        {
            if (shiokMode)
            {
                buttonOpen = true;
                buttonOpen2 = true;
            }
            else if (lvl == 2)
            {
                GameObject goTo2X = GameObject.Find("TwoXPower");
                GameObject lvl2Lock = GameObject.Find("Level2LOCKED");
                TwoXController twoX = goTo2X.GetComponent<TwoXController>();

                if (buttonOpen)
                {
                    twoX.renderer.enabled = true;
                }
                else if (twoX.activated == true && moneyNow.money >= 500)
                {
                }

                if (twoX.displaySkill == true)
                {
                    twoX.renderer.enabled = false;
                }

                if (twoX.renderer.enabled == false)
                {
                    lvl2Lock.renderer.enabled = true;
                }
                else
                {
                    lvl2Lock.renderer.enabled = false;
                }
            }
            else if (lvl == 3)
            {

                GameObject goToTimeEx = GameObject.Find("ExtendedTimePower");
                GameObject lvl3Lock = GameObject.Find("Level3LOCKED");
                ExtendedTime exTime = goToTimeEx.GetComponent<ExtendedTime>();

                if (buttonOpen2)
                {
                    exTime.renderer.enabled = true;
                }
                else if (moneyNow.money >= 1000)
                {
                    exTime.activated = true;
                }

                if (exTime.displaySkill == true)
                {
                    exTime.renderer.enabled = false;
                }

                if (exTime.renderer.enabled == false)
                {
                    lvl3Lock.renderer.enabled = true;
                }
                else
                {
                    lvl3Lock.renderer.enabled = false;
                }
            }
        }


        //Shiok Mode conditions
        if ((lvl == 3) && (curShiok == 1000) && shiokTimer < 100)
        {

            GUI.depth = -20;
            // SHIOK MODE
            GUI.DrawTexture(new Rect((Screen.width * 1) / 1920, (Screen.height * 1) / 1080, (Screen.width * 1920) / 1920, (Screen.height * 1080) / 1080), shiokModeTex);
            shiokTimer = shiokTimer + 1;

        }
        else
        {
            shiokTimer = 0;
        }

        // Kallang Wave UI
        GameObject getTimer = GameObject.Find("Pause");
        Timer timingNow = getTimer.GetComponent<Timer>();

        //when game ends,add user earned amount to user total
        if (gameOverText >= 15 || timingNow.getTimer() <= 0)
        {
            //player failed
            //user_data.m_bgame_success = false;
            

            // if counter hasn't reached 10
            if (counter <= 20)
            {   // Render "Lose" screen
                GUI.DrawTexture(new Rect((Screen.width * 825) / 1920, (Screen.height * 500) / 1080, (Screen.width * 256) / 1920, (Screen.height * 130) / 1080), gameWin);
                counter = counter + 1;
            }

            // after counter has reached limit
            else
            {   // transition to highscore screen
                Application.LoadLevel("fast_highscore_build");
                //FadeTransition.LoadLevel("fast_highscore_build", 0.5f, 0.5f, Color.black);
                counter = 0;
            }

        }

        // when timer ends and the timer hits zero
        else if (gameOverText < 15 && timingNow.getTimer() <= 0)
        {
            // player succeeded
            //user_data.m_bgame_success = true;

            if (counter <= 20)
            {   // render "Win" screen
                GUI.DrawTexture(new Rect((Screen.width * 1) / 1920, (Screen.height * 1) / 1080, (Screen.width * 1920) / 1920, (Screen.height * 1080) / 1080), gameWin);

                counter = counter + 1;
            }
            else
            {   // transition to highscore screen
                FadeTransition.LoadLevel("fast_highscore_build", 0.5f, 0.5f, Color.black);
                counter = 0;
            }
        }

        if (timingNow.getTimer() <= 60.0f && timingNow.getTimer() >= 53.0f)
        {
            //kgW.enabled= true;

            //GUI.DrawTexture (new Rect ((Screen.width*1) / 1920, (Screen.height*1) / 1080, (Screen.width*1920) / 1920, (Screen.height*1080) / 1080), bgKW);
        }
        else
        {
            //kgW.enabled = false;
        }
    }

	private void DestroyTrays()
	{
		GameObject[] trayno;

		trayno = GameObject.FindGameObjectsWithTag("Tray");

		for (int i = 0; i < trayno.Length; i++)
		{
			Destroy(trayno[i]);
		}
	}

    public void AdjustCurrentShiok(float adj)
    {
        curShiok += adj;

        if (curShiok < 0)
        {
            curShiok = 0;
        }

        if (curShiok > maxShiok)
        {
            curShiok = maxShiok;
        }

        if (maxShiok < 1)
        {
            maxShiok = 1;
        }
    }
}
