﻿using UnityEngine;
using System.Collections;

public class DottedLineController : MonoBehaviour 
{
	Vector3 StartPoint;
	private const int DISTANCE = 2;
	private int ID;
	private bool IsCut;

	public void Init(int id)
	{
		ID = id;
		IsCut = false;
	}
	// Use this for initialization
	void Start () 
	{

	}
	
	// Update is called once per frame
	void Update () 
	{
		//set start point when tap/mouse button down inside this dotted line sprite
		if ( Input.GetMouseButtonDown(0) )
		{
			StartPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			StartPoint.z = gameObject.transform.position.z;
			if ( !gameObject.collider.bounds.Contains (StartPoint) )
			{
				StartPoint = Vector3.zero;
			}
		}
		//set end point when mouse release 
		if ( Input.GetMouseButtonUp(0) && StartPoint != Vector3.zero )
		{
			Vector3 EndPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			EndPoint.z = gameObject.transform.position.z;
			if ( EndPoint.x <= gameObject.transform.position.x + 5 && EndPoint.x >= gameObject.transform.position.x - 5/*gameObject.collider.bounds.Contains (EndPoint)*/ )
			{
				//IsCut = true;
				//calculate distance
				if ((EndPoint - StartPoint).sqrMagnitude > DISTANCE*DISTANCE)
				{
					IsCut = true;
				}
			}
		}
	}

	public int GetID()
	{
		return ID;
	}

	public bool GetIsCut()
	{
		return IsCut;
	}

	public void DestorySelf()
	{
		Destroy (gameObject);
	}
}
