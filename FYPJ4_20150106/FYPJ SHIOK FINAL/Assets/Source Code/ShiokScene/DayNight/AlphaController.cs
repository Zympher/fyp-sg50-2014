﻿using UnityEngine;
using System.Collections;

public class AlphaController : MonoBehaviour
{
    GameObject NightBG;
    GameObject DNtimer;

    SpriteRenderer nightSprite;
    DayNightTimer DNscript;

    private float fullDay = 360;      //one revolution = 360 degrees

    private float nightAlpha = 0.0f;

    private bool isDay = true;

    private int lastCall;

    private int secondsCounter;

	// Use this for initialization
	void Start ()
    {
        NightBG = GameObject.Find("Night");

        DNtimer = GameObject.Find("TimerBackground");

        DNscript = DNtimer.GetComponent<DayNightTimer>();

        nightSprite = NightBG.GetComponent<SpriteRenderer>();

        lastCall = (int)Time.time;

        secondsCounter = 0;
	}
	
	// Update is called once per frame
	void Update () 
    {
        if ((int)Time.time - lastCall == 1)
        {
            if (DNscript.getAngle() <= (fullDay / 2) && isDay)
            {
                Color color = nightSprite.color;
                color.a = 1 / (fullDay / 2) * DNscript.getAngle();
                nightSprite.color = color;

                if (nightSprite.color.a == 1.0f)
                {
                    isDay = false;
                }
            }

            if (!isDay)
            {
                Color color = nightSprite.color;
                color.a = 1 / (fullDay / 2) * (fullDay - DNscript.getAngle());
                nightSprite.color = color;

                if (nightSprite.color.a == 0.0f)
                {
                    isDay = true;
                }
            }

            lastCall = (int)Time.time;
            secondsCounter++;
        }
    }
}
