using UnityEngine;
using System.Collections;

public class TehTarikMiniGame : MonoBehaviour
{

		//Gyroscope and gyrobool for storing whether a gyroscope is present
		private Gyroscope gyro;
		public bool gyrobool;

		private GameObject tutorial_phone_anim;

		//Progressbar for pulled level of teh
		//public Rect ProgressBar;
		//public Texture ProgressTex;

		//public Rect ProgressBarInner;
		//public Texture ProgressTexInner;
		//public Rect ProgressInnerCoords;

		//private float progressRatio = 0.001f;

		//for switching between objects
		public GameObject currentmovingObj;
		//for showing buttons
		private GameObject showbtnObj;

		//get user touch
		public Vector2 usertouch;
		public Vector2 usertouch2;

		//Cup Objects
		public GameObject cupLeft;
		public GameObject cupRight;

		//for when clearing minigame

		private GameObject backgroundObj;

		//private bool m_bTestRotation = false;

		public Vector3 touch_temp;

		//screen resolution to * to other resolutions
		private float NativeHeight = 600;
		private float NativeWidth = 1024;

		//progress bar to show pulled val of teh
		public float teh_pulled_val = 0;

		private const float MINIGAME_FINISH_DELAY = 0.0f;
		private float fMiniGameFinished_Time;
		public bool mini_game_over;

		private bool JustOnce_Arrow = true;
		
		//Timer getTime;
		private GUIText DisplayAtStart; 
		private GUIText Display_2;


		//spilled amt teh
		public float spilled_amt;
		public float total_spilled_amt;

		//UI related
		public bool m_bgameStart = false;
		private bool m_bDoArrow = true;
		private bool m_bDoPhone = false;

		private GameObject arrowObj;
		private GameObject glowObj;

		private const int ui_pour_count = 3;
		public int user_pour_count = 0;

		private GameObject UI_teh_display;
		
		[SerializeField]
		public float
				teh_rate_of_change = 0.005f;
		//amt needed to be transferred before one round concludes
		public float round_transfered_amt = 100;

		public float user_transferred_amt = 0;
		[SerializeField]
		private int
				user_round_count = 0;

		public bool m_bSpilling = false;

		private GameObject UI_countdown;

		[SerializeField]
		private Sprite[]
				UI_countdown_sprites;

		[SerializeField]
		private Sprite[]
				UI_teh_display_sprites;



		GameObject obj;
		Timer getTime;

		GameObject obj2;
		pauseinMG setTs;

		[SerializeField]
		private bool
				OnDebug = false;


		private bool JustOnce_Phone = false;

		public bool order_fulfilled = false;
		//amt that is spilled per round to reflect back into ui
		//private float spilt_amt;

		pauseinMG setTS;
		// Use this for initialization
		void Start ()
		{

				mini_game_over = false;

				Screen.orientation = ScreenOrientation.LandscapeLeft;
				//check if system supports gyro
				gyrobool = SystemInfo.supportsGyroscope;

				//Random which side teh starts with each time
				float random_side = Random.Range (1, 2);
				if (random_side == 1) {
						cupLeft.GetComponent<CupGyroController> ().with_teh = true;
						cupLeft.GetComponent<CupGyroController> ().filled_val = 100;
				} else {
						cupRight.GetComponent<CupGyroController> ().with_teh = true;
						cupRight.GetComponent<CupGyroController> ().filled_val = 100;
				}

				//if there is gyro,assign and enable
				if (gyrobool) {
						gyro = Input.gyro;
						gyro.enabled = true;
						gyro.updateInterval = 0.0001F;
				}

				//DisplayAtStart = GameObject.Find ("Tap To Start");
				cupLeft = GameObject.Find ("TehTarikCup_Left");
				cupRight = GameObject.Find ("TehTarikCup_Right");
	
				arrowObj = GameObject.Find ("Arrow_Aid");
				//glowObj = GameObject.Find ("Cup_Highlight");
				//glowObj.renderer.enabled = false;

				backgroundObj = GameObject.Find ("BackgroundObj");

				UI_countdown = GameObject.Find ("UI_countdown");
				UI_teh_display = GameObject.Find ("UI_display_teh");

				fMiniGameFinished_Time = -1;

				obj = GameObject.Find ("Pause");
				//getTime = obj.GetComponent<Timer>();


				DisplayAtStart = GameObject.Find ("Tap To Start Text").guiText;
				Display_2 = GameObject.Find ("Follow The Arrow Text").guiText;

				
				obj = GameObject.Find ("Pause");
				getTime = obj.GetComponent<Timer> ();

				obj2 = GameObject.Find ("PauseInMiniGame");
				setTs = obj2.GetComponent<pauseinMG> ();
				
				currentmovingObj = null;
		}
	
		// Update is called once per frame
		void Update ()
		{
	

				if (getTime.isDone) {
						//DestroyMiniGame();
						fMiniGameFinished_Time = Time.time;
				}
				//Total spilled amount
				//spilled_amt = cupLeft.GetComponent<CupGyroController>().animation + cupRight.GetComponent<CupGyroController>().animation;

//		if( cupLeft.transform.localEulerAngles.z != 0.0f )
//		{
//			m_bDoOnce = false;
//		}
//
//		if(cupRight.transform.localEulerAngles.z != 0.0f)
//		{
//			m_bDoOnce = false;
//		}

				//sprite change for ui //this checks for out of bound errors
	
				if (user_round_count > 2) {
						UI_countdown.GetComponent<SpriteRenderer> ().sprite = UI_countdown_sprites [2];
				} else {
						UI_countdown.GetComponent<SpriteRenderer> ().sprite = UI_countdown_sprites [user_round_count];
				}

				//if (total_spilled_amt > 10) {
						UI_teh_display.GetComponent<SpriteRenderer> ().sprite = UI_teh_display_sprites [(int)total_spilled_amt % 10];
				//} //else if (total_spilled_amt > 20) {
//						UI_teh_display.GetComponent<SpriteRenderer> ().sprite = UI_teh_display_sprites [2];
//				} else if (total_spilled_amt > 30) {
//						UI_teh_display.GetComponent<SpriteRenderer> ().sprite = UI_teh_display_sprites [3];
//				} else {
//						UI_teh_display.GetComponent<SpriteRenderer> ().sprite = UI_teh_display_sprites [0];
//				}

				if (m_bgameStart && m_bDoArrow) {
						//whichever cup holds the teh,place arrow there
						if (cupLeft.GetComponent<CupGyroController> ().with_teh == true) {
								arrowObj.GetComponent<ArrowController> ().transform.position = cupLeft.GetComponent<CupGyroController> ().transform.position;
								arrowObj.GetComponent<ArrowController> ().transform.position = new Vector3 (arrowObj.GetComponent<ArrowController> ().transform.position.x - arrowObj.GetComponent<ArrowController> ().transform.position.x / 2, arrowObj.GetComponent<ArrowController> ().transform.position.y, arrowObj.GetComponent<ArrowController> ().transform.position.z);
								arrowObj.GetComponent<ArrowController> ().m_bArrowActive = true;
				
						}
						if (cupRight.GetComponent<CupGyroController> ().with_teh == true) {
								arrowObj.GetComponent<ArrowController> ().transform.position = cupRight.GetComponent<CupGyroController> ().transform.position;
								arrowObj.GetComponent<ArrowController> ().transform.position = new Vector3 (arrowObj.GetComponent<ArrowController> ().transform.position.x - arrowObj.GetComponent<ArrowController> ().transform.position.x / 2, arrowObj.GetComponent<ArrowController> ().transform.position.y, arrowObj.GetComponent<ArrowController> ().transform.position.z);
								arrowObj.GetComponent<ArrowController> ().m_bArrowActive = true;
						}
				}

				if (m_bgameStart == false) {


						if (Input.GetMouseButton (0)) {
								Display_2.enabled = false;
								DisplayAtStart.enabled = false;
								m_bgameStart = true;
						}
				}

				if (m_bgameStart && !mini_game_over) {

					
						total_spilled_amt = spilled_amt;

						if (mini_game_over) {
								cupLeft.GetComponent<CupGyroController> ().transform.rotation = Quaternion.identity;
								cupLeft.GetComponent<CupGyroController> ().transform.position = cupLeft.GetComponent<CupGyroController> ().starting_pos;
								cupLeft.collider.enabled = false;

								cupRight.GetComponent<CupGyroController> ().transform.rotation = Quaternion.identity;
								cupRight.GetComponent<CupGyroController> ().transform.position = cupRight.GetComponent<CupGyroController> ().starting_pos;
								cupRight.collider.enabled = false;
						}
						if (currentmovingObj != null) {
								if (currentmovingObj == cupLeft || currentmovingObj == cupRight) {
										if (currentmovingObj.GetComponent<CupGyroController> ().filled_val >= 50) {
												currentmovingObj.GetComponent<CupGyroController> ().with_teh = true;
										}
								}
						}

					

						if (total_spilled_amt >= 50) {
								//game is over
								mini_game_over = true;
								GameLost ();

								//fMiniGameFinished_Time = Time.time;
						}
						//one round has ended
						if (user_transferred_amt >= round_transfered_amt && JustOnce_Arrow) {
								user_round_count += 1;
								spilled_amt = 0;

				print ("registered");
								//JustOnce= false;

								//reset cup positions and rotations 
								ResetGame ();

								//new rounds total amount
								round_transfered_amt = cupRight.GetComponent<CupGyroController> ().filled_val + cupLeft.GetComponent<CupGyroController> ().filled_val - spilled_amt;
								user_transferred_amt = 0;

								if (user_round_count >= 3) {
										
										
										mini_game_over = true;
										cupLeft.GetComponent<CupGyroController> ().m_bBubble = true;
										cupLeft.GetComponent<CupGyroController> ().pouring_anim.renderer.enabled = false;


										cupRight.GetComponent<CupGyroController> ().m_bBubble = true;
										cupRight.GetComponent<CupGyroController> ().pouring_anim.renderer.enabled = false;
								}
						}

						//Checking on Spilling states  
						if ((cupLeft.transform.localEulerAngles.z >= 120 && cupLeft.transform.localEulerAngles.z <= 170) 
								|| (cupLeft.transform.localEulerAngles.z >= 200 && cupLeft.transform.localEulerAngles.z <= 265)) {
								//Checking on Spilling states  
								if (cupLeft.GetComponent<CupGyroController> ().pouring_anim.GetComponent<PouringController> ().pour_check.GetComponent<PouringCollisionController> ().m_bRightCollided == true) {
										cupLeft.GetComponent<CupGyroController> ().m_bSpill = false;
										cupLeft.GetComponent<CupGyroController> ().m_bPouring = true;
								}
				
				
								if ((cupLeft.transform.localEulerAngles.z >= 120 && cupLeft.transform.localEulerAngles.z <= 170) 
										&& (cupLeft.GetComponent<CupGyroController> ().pouring_anim.GetComponent<PouringController> ().pour_check.GetComponent<PouringCollisionController> ().m_bRightCollided == false 
										&& cupLeft.GetComponent<CupGyroController> ().pouring_anim.GetComponent<PouringController> ().pour_check.GetComponent<PouringCollisionController> ().m_bLeftCollided == false)) {
										cupLeft.GetComponent<CupGyroController> ().m_bSpill = true;
										cupLeft.GetComponent<CupGyroController> ().pouring_anim.GetComponent<PouringController> ().GetComponent<Animator> ().SetBool ("IsSpilling", true);
										cupLeft.GetComponent<CupGyroController> ().pouring_anim.transform.localPosition = new Vector3 (-2.55f, 2.25f, 1.7f);
								} else if ((cupLeft.transform.localEulerAngles.z >= 120 && cupLeft.transform.localEulerAngles.z <= 170) 
										&& (cupLeft.GetComponent<CupGyroController> ().pouring_anim.GetComponent<PouringController> ().pour_check.GetComponent<PouringCollisionController> ().m_bRightCollided == true 
										|| cupLeft.GetComponent<CupGyroController> ().pouring_anim.GetComponent<PouringController> ().pour_check.GetComponent<PouringCollisionController> ().m_bLeftCollided == true)) {
										cupLeft.GetComponent<CupGyroController> ().m_bSpill = false;
										cupLeft.GetComponent<CupGyroController> ().m_bPouring = true;
								}
				
								if ((cupLeft.transform.localEulerAngles.z >= 200 && cupLeft.transform.localEulerAngles.z <= 265) 
										&& (cupLeft.GetComponent<CupGyroController> ().pouring_anim.GetComponent<PouringController> ().pour_check.GetComponent<PouringCollisionController> ().m_bRightCollided == false
										&& cupLeft.GetComponent<CupGyroController> ().pouring_anim.GetComponent<PouringController> ().pour_check.GetComponent<PouringCollisionController> ().m_bLeftCollided == false)) {
										cupLeft.GetComponent<CupGyroController> ().m_bSpill = true;
										cupLeft.GetComponent<CupGyroController> ().pouring_anim.GetComponent<PouringController> ().GetComponent<Animator> ().SetBool ("IsSpilling", true);
										cupLeft.GetComponent<CupGyroController> ().pouring_anim.transform.localPosition = new Vector3 (1.55f, 2.25f, 1.7f);
								} else if ((cupLeft.transform.localEulerAngles.z >= 200 && cupLeft.transform.localEulerAngles.z <= 265) 
										&& (cupLeft.GetComponent<CupGyroController> ().pouring_anim.GetComponent<PouringController> ().pour_check.GetComponent<PouringCollisionController> ().m_bRightCollided == true 
										|| cupLeft.GetComponent<CupGyroController> ().pouring_anim.GetComponent<PouringController> ().pour_check.GetComponent<PouringCollisionController> ().m_bLeftCollided == true)) {
										cupLeft.GetComponent<CupGyroController> ().m_bSpill = false;
										cupLeft.GetComponent<CupGyroController> ().m_bPouring = true;
								}
				
						} else {
								cupLeft.GetComponent<CupGyroController> ().m_bSpill = false;
								cupLeft.GetComponent<CupGyroController> ().m_bPouring = false;
						}

			
						if ((cupRight.transform.localEulerAngles.z >= 120 && cupRight.transform.localEulerAngles.z <= 170) 
								|| (cupRight.transform.localEulerAngles.z >= 200 && cupRight.transform.localEulerAngles.z <= 265)) {
				
								if (cupRight.GetComponent<CupGyroController> ().pouring_anim.GetComponent<PouringController> ().pour_check.GetComponent<PouringCollisionController> ().m_bLeftCollided == true) {
										cupRight.GetComponent<CupGyroController> ().m_bSpill = false;
										cupRight.GetComponent<CupGyroController> ().m_bPouring = true;
								}
				
				
								if ((cupRight.transform.localEulerAngles.z >= 120 && cupRight.transform.localEulerAngles.z <= 170) 
										&& (cupRight.GetComponent<CupGyroController> ().pouring_anim.GetComponent<PouringController> ().pour_check.GetComponent<PouringCollisionController> ().m_bRightCollided == false 
										&& cupRight.GetComponent<CupGyroController> ().pouring_anim.GetComponent<PouringController> ().pour_check.GetComponent<PouringCollisionController> ().m_bLeftCollided == false)) {
										cupRight.GetComponent<CupGyroController> ().m_bSpill = true;
										cupRight.GetComponent<CupGyroController> ().pouring_anim.GetComponent<PouringController> ().GetComponent<Animator> ().SetBool ("IsSpilling", true);
										cupRight.GetComponent<CupGyroController> ().pouring_anim.transform.localPosition = new Vector3 (-2.55f, 2f, 5f);
								} else if ((cupRight.transform.localEulerAngles.z >= 120 && cupRight.transform.localEulerAngles.z <= 170) 
										&& (cupRight.GetComponent<CupGyroController> ().pouring_anim.GetComponent<PouringController> ().pour_check.GetComponent<PouringCollisionController> ().m_bRightCollided == true 
										|| cupRight.GetComponent<CupGyroController> ().pouring_anim.GetComponent<PouringController> ().pour_check.GetComponent<PouringCollisionController> ().m_bLeftCollided == true)) {
										cupRight.GetComponent<CupGyroController> ().m_bSpill = false;
										cupRight.GetComponent<CupGyroController> ().m_bPouring = true;
								}
				
								if ((cupRight.transform.localEulerAngles.z >= 200 && cupRight.transform.localEulerAngles.z <= 265) 
										&& (cupRight.GetComponent<CupGyroController> ().pouring_anim.GetComponent<PouringController> ().pour_check.GetComponent<PouringCollisionController> ().m_bRightCollided == false
										&& cupRight.GetComponent<CupGyroController> ().pouring_anim.GetComponent<PouringController> ().pour_check.GetComponent<PouringCollisionController> ().m_bLeftCollided == false)) {
										//Debug.Log ("REGISTERED");
										cupRight.GetComponent<CupGyroController> ().m_bSpill = true;
										cupRight.GetComponent<CupGyroController> ().pouring_anim.GetComponent<PouringController> ().GetComponent<Animator> ().SetBool ("IsSpilling", true);
										cupRight.GetComponent<CupGyroController> ().pouring_anim.transform.localPosition = new Vector3 (1.45f, 2f, 5f);
								} else if ((cupRight.transform.localEulerAngles.z >= 200 && cupRight.transform.localEulerAngles.z <= 265) 
										&& (cupRight.GetComponent<CupGyroController> ().pouring_anim.GetComponent<PouringController> ().pour_check.GetComponent<PouringCollisionController> ().m_bRightCollided == true 
										|| cupRight.GetComponent<CupGyroController> ().pouring_anim.GetComponent<PouringController> ().pour_check.GetComponent<PouringCollisionController> ().m_bLeftCollided == true)) {
										//Debug.Log("ANOTHER REGISTER");
										cupRight.GetComponent<CupGyroController> ().m_bSpill = false;
										cupRight.GetComponent<CupGyroController> ().m_bPouring = true;
								}
						} else {
								cupRight.GetComponent<CupGyroController> ().m_bSpill = false;
								cupRight.GetComponent<CupGyroController> ().m_bPouring = false;
						}
		

						if (cupLeft.GetComponent<CupGyroController> ().m_bPouring || cupRight.GetComponent<CupGyroController> ().m_bPouring) {
								cupRight.GetComponent<CupGyroController> ().m_bFirst = false;
								cupLeft.GetComponent<CupGyroController> ().m_bFirst = false;
						}

	
							
			//Check win lose and end
						if (!IsMiniGameFinish ()) {
								//check win condition
								if (user_round_count >= 3) {
										order_fulfilled = true;
					fMiniGameFinished_Time = Time.time;
					DestroyMiniGame ();
										
								} else if (total_spilled_amt >= 50) {
										order_fulfilled = false;
					fMiniGameFinished_Time = Time.time;
					DestroyMiniGame ();
								}


						} else {

								if (Time.time > fMiniGameFinished_Time + MINIGAME_FINISH_DELAY) {
										order_fulfilled = false;
										print ("time out");
										DestroyMiniGame ();
								}
						}
					
						foreach (Touch touch  in Input.touches) {



								Ray ray = Camera.main.ScreenPointToRay (touch.position);
								RaycastHit hit;
								//TouchPhase phase = touch.phase;
			
								switch (touch.phase) {
				
								case TouchPhase.Began:
										{
												JustOnce_Arrow = true;
				
												usertouch = touch.position;

												if (Physics.Raycast (ray, out hit)) {
														if (hit.collider.gameObject == cupLeft) {
																currentmovingObj = cupLeft;
																cupLeft.GetComponent<CupGyroController> ().m_bSelected = true;
														
														} else {
																cupLeft.GetComponent<CupGyroController> ().m_bSelected = false;
														}
														if (hit.collider.gameObject == cupRight) {
																currentmovingObj = cupRight;
																cupRight.GetComponent<CupGyroController> ().m_bSelected = true;
															
														} else {
																cupRight.GetComponent<CupGyroController> ().m_bSelected = false;
														}
												}
												//arrowObj.GetComponent<ArrowController>().m_bArrowActive = false;
												break;
				
										}
				
								case TouchPhase.Moved:
										{
												m_bDoArrow = false;
												arrowObj.GetComponent<ArrowController> ().m_bArrowActive = false;

												//if user click on screen buttons are set to false
												cupLeft.GetComponent<CupGyroController> ().m_bCreatebtn = false;
												cupRight.GetComponent<CupGyroController> ().m_bCreatebtn = false;

												//z set to 10 because camera is offset by - 10
												currentmovingObj.transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (touch.position.x, touch.position.y + 166f / 2, 10));
												touch_temp = Camera.main.WorldToScreenPoint (new Vector3 (currentmovingObj.transform.position.x, currentmovingObj.transform.position.y, 0));
												
												
												break;
										}

								case TouchPhase.Ended: 
										{

												//When finger released,release current obl
												currentmovingObj = null;
												
												//cupLeft.GetComponent<CupGyroController>().m_bSelected = false;
												//cupRight.GetComponent<CupGyroController>().m_bSelected = false;
												break;
										}
				
								}
			
						}

				}//brace of m_bgameStart

	
		}

		void ResetGame ()
		{
				currentmovingObj = null;
				cupLeft.transform.position = cupLeft.GetComponent<CupGyroController> ().starting_pos;
				cupLeft.transform.rotation = Quaternion.identity;
		
				cupRight.transform.position = cupRight.GetComponent<CupGyroController> ().starting_pos;
				cupRight.transform.rotation = Quaternion.identity;
		}

		void GameLost ()
		{
				
				order_fulfilled = false;

				currentmovingObj = null;
				cupLeft.transform.position = cupLeft.GetComponent<CupGyroController> ().starting_pos;
				cupLeft.GetComponent<CupGyroController> ().pouring_anim.renderer.enabled = false;
				cupLeft.GetComponent<CupGyroController> ().tutorial_phone_anim.renderer.enabled = false;
				cupLeft.transform.rotation = Quaternion.identity;
		
				cupRight.transform.position = cupRight.GetComponent<CupGyroController> ().starting_pos;
				cupRight.GetComponent<CupGyroController> ().pouring_anim.renderer.enabled = false;
				cupRight.GetComponent<CupGyroController> ().tutorial_phone_anim.renderer.enabled = false;
				cupRight.transform.rotation = Quaternion.identity;
		}
		void OnGUI ()
		{
				//Scale to 1024 600
				float rx = Screen.width / NativeWidth;
				float ry = Screen.height / NativeHeight;
				// Scale width the same as height - cut off edges to keep ratio the same
				GUI.matrix = Matrix4x4.TRS (new Vector3 (0, 0, 0), Quaternion.identity, new Vector3 (ry, ry, 1));
				// Get width taking into account edges being cut off or extended
				float adjustedWidth = NativeWidth * (rx / ry);
			
				//GUILayout.Label (Time.time.ToString ());
				//GUILayout.Label (fMiniGameFinished_Time.ToString ());

				if (OnDebug) {
						GUILayout.Label ("Spill:" + cupRight.GetComponent<CupGyroController> ().m_bSpill.ToString ());
						GUILayout.Label ("Pour:" + cupRight.GetComponent<CupGyroController> ().m_bPouring.ToString ());
						if (currentmovingObj != null) {
								GUILayout.Label (currentmovingObj.ToString ());
						}

				
						GUILayout.Label ("round_transfer_amt:" + round_transfered_amt.ToString ());
						GUILayout.Label ("Which round:" + user_round_count.ToString ());
						GUILayout.Label ("User Transferred amount:" + user_transferred_amt.ToString ());
						GUILayout.Label ("Round Required transfer amount:" + round_transfered_amt.ToString ());
						GUILayout.Label ("Spilled Amount:" + spilled_amt.ToString ());
						GUILayout.Label ("Total Spilled Amount:" + total_spilled_amt.ToString ());
						GUILayout.Label ("Game Over:" + mini_game_over.ToString ());



			
						//GUILayout.Label ("Spill:" + cupLeft.GetComponent<CupGyroController> ().pouring_anim.GetComponent<PouringController> ().pour_check.GetComponent<PouringCollisionController>().Spill.ToString ());

//				if (cupLeft.GetComponent<CupGyroController> ().with_teh) {
//						GUILayout.Label ("left with teh");	
//						GUILayout.Label ((cupLeft.GetComponent<CupGyroController> ().elapsed_time - cupLeft.GetComponent<CupGyroController> ().TIME_SINCE_POUR).ToString ());
//				}
//
//				if (cupRight.GetComponent<CupGyroController> ().with_teh) {
//						GUILayout.Label ("right with teh");	
//						GUILayout.Label ((cupRight.GetComponent<CupGyroController> ().elapsed_time - cupRight.GetComponent<CupGyroController> ().TIME_SINCE_POUR).ToString ());
//				}

						//GUILayout.Label (teh_pulled_val.ToString ());
//		GUILayout.Label ("Left position:" + cupLeft.GetComponent<CupGyroController> ().transform.position.x.ToString ());
//		GUILayout.Label ("Left min_x:"+cupLeft.GetComponent<CupGyroController> ().min_x.ToString ());
//		GUILayout.Label ("Left max_x:"+cupLeft.GetComponent<CupGyroController> ().max_x.ToString ());
//		GUILayout.Label ("Left position:" + cupRight.GetComponent<CupGyroController> ().transform.position.x.ToString ());
//		GUILayout.Label ("Right min_x:"+cupRight.GetComponent<CupGyroController> ().min_x.ToString ());
//		GUILayout.Label ("Right max_x:"+cupRight.GetComponent<CupGyroController> ().max_x.ToString ());
						GUILayout.Label ("cupLeft:" + cupLeft.GetComponent<CupGyroController> ().filled_val.ToString ());
						GUILayout.Label ("cupRight: " + cupRight.GetComponent<CupGyroController> ().filled_val.ToString ());

//		if (cupRight.GetComponent<CupGyroController> ().m_bWithinRange) 
//		{GUILayout.Label ("Right is within");}
//		if (cupLeft.GetComponent<CupGyroController> ().m_bWithinRange) 
//		{GUILayout.Label ("Left is within");}
//
//
//		GUILayout.Label ("gyro enabled:" + gyro.enabled.ToString ());
//		if (gyro.userAcceleration.x > gyro.userAcceleration.y && gyro.userAcceleration.x > gyro.userAcceleration.z) {GUILayout.Label ("gyro user_accelX:" + gyro.userAcceleration.x.ToString ());}
//
//		if(gyro.userAcceleration.y > gyro.userAcceleration.x && gyro.userAcceleration.y > gyro.userAcceleration.z){GUILayout.Label ("gyro user_accelY:" + gyro.userAcceleration.y.ToString ());}
//
//		if (gyro.userAcceleration.z > gyro.userAcceleration.x && gyro.userAcceleration.z > gyro.userAcceleration.y) {GUILayout.Label ("gyro user_accelZ:" + gyro.userAcceleration.z.ToString ());}

						//GUILayout.Label ("cupleft_X:" +cupLeft.GetComponent<CupGyroController>().transform.position.x.ToString ());
						//GUILayout.Label ("cupleft_Y:" +cupLeft.GetComponent<CupGyroController>().transform.position.y.ToString ());
						//GUILayout.Label ("cupleft_Z:" +cupLeft.GetComponent<CupGyroController>().transform.position.z.ToString ());

						//GUILayout.Label ("cupright_X:" +cupRight.GetComponent<CupGyroController>().transform.position.x.ToString ());
						//GUILayout.Label ("cupright_Y:" +cupRight.GetComponent<CupGyroController>().transform.position.y.ToString ());
						//GUILayout.Label ("cupright_Z:" +cupRight.GetComponent<CupGyroController>().transform.position.z.ToString ());
//		GUILayout.Label ("anim_X:"+pouringAnimObj.GetComponent<PouringController> ().transform.position.x.ToString ());
//		GUILayout.Label ("anim_Y:" + pouringAnimObj.GetComponent<PouringController> ().transform.position.y.ToString ());
//		GUILayout.Label ("anim_Z:" + pouringAnimObj.GetComponent<PouringController> ().transform.position.z.ToString ());
						//GUILayout.Label (currentmovingObj.ToString ());
						//GUILayout.Label (Input.gyro.attitude.z.ToString ());
						//GUILayout.Label (m_bTestRotation.ToString());

//		GUI.BeginGroup(new Rect (pos.x ,pos.y, size.x, size.y));
//		GUI.Box(new Rect(0, 0, size.x, size.y), pulledBarEmpty);
//		
//		// draw the filled-in part:
//		GUI.BeginGroup(new Rect ((size.x* (teh_pulled_val/100) - (size.x)),0 , size.x * (teh_pulled_val/100), size.y ));
//		GUI.Label (new Rect ((-size.x * teh_pulled_val + (size.x))+ 10, 0, size.x, size.y), teh_pulled_val.ToString());
//		GUI.Box(new Rect ((-size.x* (teh_pulled_val/100) + (size.x)), 0, size.x, size.y), pulledBarFull);
//		GUI.EndGroup();
//		GUI.EndGroup ();
				}
		}

		public bool IsMiniGameFinish ()
		{
				if (fMiniGameFinished_Time > 0) {
						return true;
				}

				return false;
		}

		void AddTagRecursively (Transform trans, string tag)
		{
				trans.gameObject.tag = tag;
				if (trans.GetChildCount () > 0)
						foreach (Transform t in trans)
								AddTagRecursively (t, tag);
		}

		public void DestroyMiniGame ()
		{

				AddTagRecursively (gameObject.transform, "TehTarikMiniGame");

				GameObject[] temp;
				temp = GameObject.FindGameObjectsWithTag ("TehTarikMiniGame");

				for (int i = 0; i < temp.Length; i++) {
						Destroy (temp [i]);
				}
				

		}
}
