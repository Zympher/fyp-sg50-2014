﻿using UnityEngine;
using System.Collections;

public class CupExtendedController : MonoBehaviour {

	public GameObject opposingCup;
	private bool m_bCollision = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Debug.Log (m_bCollision.ToString ());
	}

	void OnTriggerEnter(Collider col) 
	{
		if (col.gameObject  == opposingCup) 
		{
			m_bCollision = true;
		}
	}

	void OnCollisionEnter(Collision col) 
	{

	}

	void OnGUI ()
	{
		if (m_bCollision) 
		{
			GUILayout.Label ("Collision happening");
		}
	}
}
