﻿using UnityEngine;
using System.Collections;

public enum ICE_KACANG_INGREDIENT_TYPE
{
	ATAPCHI,
	CHENDOL,
	CORN,
	RED_BEAN,
	MILK,
	GULA,
	SYRUP_GREEN,
	SYRUP_RED,
	SYRUP_YELLOW,
}

public enum ICE_KACANG_SET_TYPE
{
	SET_A,
	SET_B,
	SET_C,
}

public class IceKaCangMiniGame : MonoBehaviour 
{
	// basic obj
	public GameObject backGroundObj;
	public GameObject BaseShavedIce;
	public GameObject[] TickPrefabs;
	private ArrayList TickObjs;
	//ingredient
	public GameObject[] IngredientPrefabs;
	private ArrayList IngredientObjs;
	//ingredient icon
	public GameObject[] IngredientIconPrefabs;
	private ArrayList IngredientIconObjs;
	public GameObject[] OrderPrefabs;
	private GameObject OrderObj;

	private const int NUM_OF_SET = 3;
	private const int NUM_OF_INGREDIENT_NEEDED = 4;
	private const int NUM_OF_INGREDIENT = 9;
	private ICE_KACANG_SET_TYPE Set_Type;
	private float fMiniGameFinished_Time;
	private const float MINIGAME_FINISH_DELAY = 1.0f;
	private ICE_KACANG_INGREDIENT_TYPE[,] IceKacangSets;
	private bool mini_game_over;

	private ICE_KACANG_INGREDIENT_TYPE[] Chosen_types;

	private int[] IceKacangSetFill;

	public bool order_fulfilled = false;

	private int user_count = 0;

    GameObject obj;
    Timer getTime;

	// Use this for initialization
	void Start () 
	{
		mini_game_over = false;

		Chosen_types = new ICE_KACANG_INGREDIENT_TYPE[4];

		Chosen_types[0] = ICE_KACANG_INGREDIENT_TYPE.ATAPCHI;
		Chosen_types[1] = ICE_KACANG_INGREDIENT_TYPE.GULA;
		Chosen_types[2] = ICE_KACANG_INGREDIENT_TYPE.SYRUP_YELLOW;
		Chosen_types[3] = ICE_KACANG_INGREDIENT_TYPE.CHENDOL;
//		IceKacangSetFill = new int[4];
//
//		for (int i = 0; i< 4; i++) 
//		{
//			IceKacangSetFill[i] = -1;
//		}

		// init iceKacang sets
		IceKacangSets = new ICE_KACANG_INGREDIENT_TYPE[NUM_OF_SET, NUM_OF_INGREDIENT_NEEDED];

		IceKacangSets [(int)ICE_KACANG_SET_TYPE.SET_A, 0] = ICE_KACANG_INGREDIENT_TYPE.ATAPCHI;
		IceKacangSets [(int)ICE_KACANG_SET_TYPE.SET_A, 1] = ICE_KACANG_INGREDIENT_TYPE.MILK;
		IceKacangSets [(int)ICE_KACANG_SET_TYPE.SET_A, 2] = ICE_KACANG_INGREDIENT_TYPE.CHENDOL;
		IceKacangSets [(int)ICE_KACANG_SET_TYPE.SET_A, 3] = ICE_KACANG_INGREDIENT_TYPE.SYRUP_RED;

		IceKacangSets [(int)ICE_KACANG_SET_TYPE.SET_B, 0] = ICE_KACANG_INGREDIENT_TYPE.GULA;
		IceKacangSets [(int)ICE_KACANG_SET_TYPE.SET_B, 1] = ICE_KACANG_INGREDIENT_TYPE.CORN;
		IceKacangSets [(int)ICE_KACANG_SET_TYPE.SET_B, 2] = ICE_KACANG_INGREDIENT_TYPE.SYRUP_GREEN;
		IceKacangSets [(int)ICE_KACANG_SET_TYPE.SET_B, 3] = ICE_KACANG_INGREDIENT_TYPE.ATAPCHI;

		IceKacangSets [(int)ICE_KACANG_SET_TYPE.SET_C, 0] = ICE_KACANG_INGREDIENT_TYPE.SYRUP_YELLOW;
		IceKacangSets [(int)ICE_KACANG_SET_TYPE.SET_C, 1] = ICE_KACANG_INGREDIENT_TYPE.RED_BEAN;
		IceKacangSets [(int)ICE_KACANG_SET_TYPE.SET_C, 2] = ICE_KACANG_INGREDIENT_TYPE.MILK;
		IceKacangSets [(int)ICE_KACANG_SET_TYPE.SET_C, 3] = ICE_KACANG_INGREDIENT_TYPE.CHENDOL;

		// init basic objs
		Set_Type = (ICE_KACANG_SET_TYPE)Random.Range(0,NUM_OF_SET);
		backGroundObj = (GameObject)Instantiate( backGroundObj,
		                                         backGroundObj.transform.position,
		                                         backGroundObj.transform.rotation);
		BaseShavedIce = (GameObject)Instantiate( BaseShavedIce,
		                                         BaseShavedIce.transform.position,
		                                         BaseShavedIce.transform.rotation);
		OrderObj = (GameObject)Instantiate( OrderPrefabs[(int)Set_Type],
		                                    OrderPrefabs[(int)Set_Type].transform.position,
		                                    OrderPrefabs[(int)Set_Type].transform.rotation);

		// init ingredient icon
		IngredientIconObjs = new ArrayList ();
		for ( int i = 0 ; i < NUM_OF_INGREDIENT ; i++ )
		{
			GameObject newIngredientIcon;
			newIngredientIcon = (GameObject)Instantiate( IngredientIconPrefabs[i],
			                                             IngredientIconPrefabs[i].transform.position,
			                                             IngredientIconPrefabs[i].transform.rotation);
			newIngredientIcon.GetComponent<IceKacangIngredientIconController>().Init((ICE_KACANG_INGREDIENT_TYPE)i);
			IngredientIconObjs.Add(newIngredientIcon);
		}
		
		IngredientObjs = new ArrayList ();
		TickObjs = new ArrayList ();
		fMiniGameFinished_Time = -1;

        obj = GameObject.Find("Pause");
        getTime = obj.GetComponent<Timer>();
	}

	public void DestroyMiniGame()
	{
        //backGroundObj.renderer.enabled = false;
        //BaseShavedIce.renderer.enabled = false;
        //OrderObj.renderer.enabled = false;

		Destroy (backGroundObj);
		Destroy (BaseShavedIce);
		Destroy (OrderObj);
		foreach( GameObject obj in IngredientIconObjs )
		{
            //obj.renderer.enabled = false;
			obj.GetComponent<IceKacangIngredientIconController>().DestroySelf();
		}

		foreach( GameObject obj in IngredientObjs )
		{
            //obj.renderer.enabled = false;
			Destroy (obj);
		}
		foreach( GameObject obj in TickObjs )
		{
            //obj.renderer.enabled = false;
			Destroy (obj);
		}
		Destroy (gameObject);
	}

	public bool IsMiniGameFinish()
	{
		if (fMiniGameFinished_Time > 0)
			return true;
		return false;
	}

	public bool IsIceKacangFinishedCorrectly()
	{
		if (TickObjs.Count >= NUM_OF_INGREDIENT_NEEDED) {
			order_fulfilled = true;			
			return true;

				}
		else{
			order_fulfilled = false;
			return false;
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (user_count == 4) {
						if //(
			(Chosen_types [0] == Chosen_types [1] 
								|| Chosen_types [0] == Chosen_types [2]
								|| Chosen_types [0] == Chosen_types [3]//) {
								  && user_count <= 3){
								order_fulfilled = false;		
						} 

			// winning algorithm
			else if ((Chosen_types [0] == IceKacangSets [(int)Set_Type, 0]
				         || Chosen_types [0] == IceKacangSets [(int)Set_Type, 1]
				         || Chosen_types [0] == IceKacangSets [(int)Set_Type, 2]
				         || Chosen_types [0] == IceKacangSets [(int)Set_Type, 3])
				         &&
			         (Chosen_types [1] == IceKacangSets [(int)Set_Type, 0]
						 || Chosen_types [1] == IceKacangSets [(int)Set_Type, 1]
						 || Chosen_types [1] == IceKacangSets [(int)Set_Type, 2]
						 || Chosen_types [1] == IceKacangSets [(int)Set_Type, 3])
						 &&
					 (Chosen_types [2] == IceKacangSets [(int)Set_Type, 0]
						 || Chosen_types [2] == IceKacangSets [(int)Set_Type, 1]
						 || Chosen_types [2] == IceKacangSets [(int)Set_Type, 2]
						 || Chosen_types [2] == IceKacangSets [(int)Set_Type, 3])
						 &&
					  (Chosen_types [3] == IceKacangSets [(int)Set_Type, 0]
						 || Chosen_types [3] == IceKacangSets [(int)Set_Type, 1]
						 || Chosen_types [3] == IceKacangSets [(int)Set_Type, 2]
						 || Chosen_types [3] == IceKacangSets [(int)Set_Type, 3])
			         ) {
				order_fulfilled = true;
			}


				}// else 
				//		order_fulfilled = true;

		if ( IsMiniGameFinish() == false  )
		{
            if (getTime.isDone)
			{ 

                //Debug.Log("its in");
                fMiniGameFinished_Time = Time.time;
                //DestroyMiniGame();
                //getTime.setIsDone(false);
            }




			if(Chosen_types[0] != null)
			print ("0:" + Chosen_types[0].ToString ());

			if(Chosen_types[1] != null)
				print ("1:" + Chosen_types[1].ToString ());

			if(Chosen_types[2] != null)
				print ("2:" + Chosen_types[2].ToString ());

			if(Chosen_types[3] != null)
				print ("3:" + Chosen_types[3].ToString ());
			//}


//			if (TickObjs.Count >= NUM_OF_INGREDIENT_NEEDED 
//			    && Chosen_types[0] == 1
//			    && Chosen_types[1] == 1
//			    && Chosen_types[2] == 1
//			    && Chosen_types[3] == 1) {
//				order_fulfilled = true;		
//			}

			// is placed all need ingredient ?
			if ( IngredientObjs.Count < NUM_OF_INGREDIENT_NEEDED )
			{
				order_fulfilled = false;
				//print (IngredientObjs.Count);
				if ( Input.GetMouseButtonDown(0) )
				{
					RaycastHit hitInfo = new RaycastHit();
					bool hit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo);
					if (hit) 
					{	
						if (hitInfo.transform.gameObject.tag == "IceKacangIngredientIcon")
						{
							// create glow
							IceKacangIngredientIconController iceKacangIngredientIconController;
							iceKacangIngredientIconController = hitInfo.transform.gameObject.GetComponent<IceKacangIngredientIconController>();
							ICE_KACANG_INGREDIENT_TYPE Type = iceKacangIngredientIconController.GetIngredientType();
							iceKacangIngredientIconController.CreateGlow();

							Chosen_types[user_count] = Type;
							print (user_count.ToString ());
							// create tick
							for ( int i = 0 ; i < NUM_OF_INGREDIENT_NEEDED ; i++ )
							{
								if ( Type == IceKacangSets[(int)Set_Type,i] )
								{
									GameObject newTick;
									newTick = (GameObject)Instantiate( TickPrefabs[i],
									                                   TickPrefabs[i].transform.position,
									                                   TickPrefabs[i].transform.rotation);
									TickObjs.Add(newTick);

									break;
								}
							}

							user_count += 1;

							// create ingredient on ice
							GameObject newIngredient;
							newIngredient = (GameObject)Instantiate( IngredientPrefabs[(int)Type],
							                                         IngredientPrefabs[(int)Type].transform.position,
							                                         IngredientPrefabs[(int)Type].transform.rotation);
							IngredientObjs.Add(newIngredient);
						}
					}
				}
			}
			else
			{
				// timer start
				fMiniGameFinished_Time = Time.time;
			}
		}
		else
		{
			// delay time over
			if ( Time.time > fMiniGameFinished_Time+MINIGAME_FINISH_DELAY )
			{
				order_fulfilled = false;
				DestroyMiniGame();
			}
		}     
	}
}
