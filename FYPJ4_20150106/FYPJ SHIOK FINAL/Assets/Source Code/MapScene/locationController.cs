﻿using UnityEngine;
using System.Collections;
using System;

public class locationController : MonoBehaviour 
{
    private float userLat;
    private float userLong;
    private float adamRoadLat = 1.324196f;
    private float adamRoadLong = 103.814039f;
    private float LPSlat = 1.280689f;
    private float LPSlong = 103.850412f;

    float xTop = 900;
    float yHeight = 608f;

    private bool initializingLoc = false;
    private bool selectHawker;
    private bool setTrue = false;

    public Texture2D backgroundImage;

    GUIContent boxContent;
    Rect boxRect;

	// Use this for initialization
	public IEnumerator Start ()
    {
        boxContent = new GUIContent();
        boxRect = new Rect((Screen.width * xTop) / 1920, (Screen.height * yHeight) / 1080, (Screen.width * 800) / 1920, (Screen.height * 100) / 1080);

        if (!Input.location.isEnabledByUser)
        {
            //checks if GPS satellite is on
        }

        Input.location.Start();

        //wait until service init
        int maxWait = 20;
        while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            initializingLoc = true;
            yield return new WaitForSeconds(1.0f);

            maxWait--;
        }

        //service did not init
        if (maxWait < 1)
        {
            print("Timed out");
        }

        //connection failed
        if (Input.location.status == LocationServiceStatus.Failed)
        {
            print("Unable to determine device location");
        }

        //success
        else
        {
            initializingLoc = false;
            print("Lat: " + userLat + " Long: " + userLong);
            userLat = Input.location.lastData.latitude;
            userLong = Input.location.lastData.longitude;
        }

        Input.location.Stop();
    }

    public void destroyEverything()
    {
        Destroy(gameObject);
    }

    public void setBoolTrue(bool showWindow, bool hawkerCenter)
    {
        setTrue = showWindow;       //enable/disable render

        selectHawker = hawkerCenter; //true = AR, false = LPS
    }

    public float getDistancefromAR () 
    {
        float R = 6378137; // Earth’s mean radius in meter
        float dLat, dLong, a, c, d;

        if ((adamRoadLat - userLat) > 0)    //checks to make sure result is positive
        {
            dLat = Mathf.Deg2Rad * (adamRoadLat - userLat);
            dLong = Mathf.Deg2Rad * (adamRoadLong - userLong);
        }
        else
        {
            dLat = Mathf.Deg2Rad * (userLat - adamRoadLat);
            dLong = Mathf.Deg2Rad * (userLong - adamRoadLong);
        }

        a = Mathf.Sin(dLat / 2) * Mathf.Sin(dLat / 2) + Mathf.Cos(Mathf.Deg2Rad*(userLat)) * Mathf.Cos(Mathf.Deg2Rad*(adamRoadLat)) * Mathf.Sin(dLong / 2) * Mathf.Sin(dLong / 2);
        c = 2 * Mathf.Atan2(Mathf.Sqrt(a), Mathf.Sqrt(1 - a));
        d = (R * c)/1000;
        d = (float)Math.Round(d, 2);

        return d; // returns the distance in kilometer
    }

    public float getDistancefromLPS()
    {
        float R = 6378137; // Earth’s mean radius in meter
        float dLat, dLong, a, c, d;

        if ((LPSlat - userLat) > 0)     //checks to make sure result is positive
        {
            dLat = Mathf.Deg2Rad * (LPSlat - userLat);
            dLong = Mathf.Deg2Rad * (LPSlong - userLong);
        }
        else
        {
            dLat = Mathf.Deg2Rad * (userLat - LPSlat);
            dLong = Mathf.Deg2Rad * (userLong - LPSlong);
        }

        a = Mathf.Sin(dLat / 2) * Mathf.Sin(dLat / 2) + Mathf.Cos(Mathf.Deg2Rad * (userLat)) * Mathf.Cos(Mathf.Deg2Rad * (LPSlat)) * Mathf.Sin(dLong / 2) * Mathf.Sin(dLong / 2);
        c = 2 * Mathf.Atan2(Mathf.Sqrt(a), Mathf.Sqrt(1 - a));
        d = (R * c)/1000;
        d = (float)Math.Round(d, 2);

        return d; // returns the distance in kilometer
    }

    public bool getInitLoc()
    {
        return initializingLoc;
    }

	// Update is called once per frame
	public void OnGUI ()
    {
        GUI.skin.box.normal.background = backgroundImage;
        GUI.skin.box.active.background = backgroundImage;
        GUI.skin.box.hover.background = backgroundImage;

        GUIStyle myBox = new GUIStyle();
        myBox.normal.textColor = Color.black;
        myBox.fontSize = 24;
        myBox.fontStyle = FontStyle.Bold;

        if (!initializingLoc && setTrue && selectHawker)    //AR hawker selected
            GUI.Box(new Rect((Screen.width * xTop) / 1920, (Screen.height * yHeight) / 1080, (Screen.width * 800) / 1920, (Screen.height * 100) / 1080), getDistancefromAR() + " KM " + "away from this hawker center!", myBox);
        else if (!initializingLoc && setTrue && !selectHawker)    //LPS hawker selected
            GUI.Box(new Rect((Screen.width * xTop) / 1920, (Screen.height * yHeight) / 1080, (Screen.width * 800) / 1920, (Screen.height * 100) / 1080), getDistancefromLPS() + " KM " + "away from this hawker center!", myBox);
        else if(initializingLoc && setTrue)
            GUI.Box(new Rect((Screen.width * xTop) / 1920, (Screen.height * yHeight) / 1080, (Screen.width * 800) / 1920, (Screen.height * 100) / 1080), "Getting Your Location! Hang on tight!", myBox);
	}
}
