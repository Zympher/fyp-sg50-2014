﻿using UnityEngine;
using System.Collections;

public class MapInterface : MonoBehaviour
{
    public Vector2 startTouch = Vector2.zero;

    private bool windowActivatedAR = false;
    private bool windowActivatedLPS = false;


    private bool locConfirmed;

    public bool selectedMap;        //true = AR, false = LPS

    private float xTop = 1575f;
    private float yHeight = 126f;

    private float xTopPlay = 886.6f;
    private float yHeightPlay = 728f;

    [SerializeField]
    private float xTopLead = 443.3f;
    [SerializeField]
    private float yHeightLead = 728f;

    public Texture2D[] closeButton;
    public Texture2D[] playButton;
    public Texture2D[] leaderboardButton;

    GUIContent buttonContent;
    GUIContent closeContent;
    GUIContent leaderContent;

    Rect buttonRect;
    Rect closeRect;
    Rect leaderRect;

    Vector2 mouse;

    [SerializeField]
    public GameObject[] mapButtons;

    GameObject hawker_1;
    GameObject hawker_2;
    GameObject blur;
    GameObject play;
    GameObject close;
    GameObject board;

    GameObject setMap;

    GameObject bg;

    // Use this for initialization
    void Start()
    {
        buttonContent = new GUIContent();
        closeContent = new GUIContent();
        leaderContent = new GUIContent();

        buttonRect = new Rect((Screen.width * xTopPlay) / 1920, (Screen.height * yHeightPlay) / 1080, (Screen.width * 577) / 1920, (Screen.height * 100) / 1080);
        closeRect = new Rect((Screen.width * xTop) / 1920, (Screen.height * yHeight) / 1080, (Screen.width * 100) / 1920, (Screen.height * 100) / 1080);
        leaderRect = new Rect((Screen.width * xTopLead) / 1920, (Screen.height * yHeightLead) / 1080, (Screen.width * 800) / 1920, (Screen.height * 100) / 1080);

        hawker_1 = GameObject.Find("ARinfo");
        hawker_2 = GameObject.Find("LPSinfo");

        blur = GameObject.Find("blur");
        play = GameObject.Find("playButton");
        close = GameObject.Find("closeButton");
        board = GameObject.Find("boardButton");

        setMap = GameObject.Find("selectedMap");
    }

    void destroyAll()
    {
        Destroy(gameObject);

        for (int i = 0; i < 8; i++)
        {
            if (i == 5)
                i++;
            Destroy(mapButtons[i]);
        }
    }

    void Resize()
    {
        bg = GameObject.Find("singapore_map_outline");
        SpriteRenderer sr = bg.GetComponent<SpriteRenderer>();

        sr.transform.localScale = new Vector3(1, 1, 1);


        float worldScreenHeight = Camera.main.orthographicSize * 2f;
        float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;

        sr.transform.localScale = new Vector3(worldScreenWidth / sr.sprite.bounds.size.x,
                                           worldScreenHeight / sr.sprite.bounds.size.y, 1);

    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetMouseButtonDown(0))
        //{
        //    mapButtons[5].GetComponent<LoadingSceneController>().enabled = true;
        //    mapButtons[5].GetComponent<LoadingSceneController>().setLoadingTrue("ShiokScene");
        //}

        Resize();

        mouse = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);

        if (buttonRect.Contains(mouse) && Input.GetMouseButton(0))
        {
            buttonContent.image = playButton[1];
        }
        else
        {
            buttonContent.image = playButton[0];
        }

        if (closeRect.Contains(mouse) && Input.GetMouseButton(0))
        {
            closeContent.image = closeButton[1];
        }
        else
        {
            closeContent.image = closeButton[0];
        }

        if (leaderRect.Contains(mouse) && Input.GetMouseButton(0))
        {
            leaderContent.image = leaderboardButton[1];
        }
        else
        {
            leaderContent.image = leaderboardButton[0];
        }


        foreach (Touch touch in Input.touches)
        {
            startTouch = touch.position;
            Ray ray = Camera.main.ScreenPointToRay(touch.position);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began)
                {
                    //Adam road (Hawker_1)
                    if (hit.collider.gameObject == mapButtons[0] && !windowActivatedLPS)    //pressed AR
                    {
                        windowActivatedAR = true;
                    }

                    //old airport road (Hawker_2)
                    //GameObject hawker_2 = GameObject.Find("hawker_2");
                    //if (hit.collider.gameObject == mapButtons[1])
                    //{
                    //}

                    //lau pa sat (Hawker_3)
                    //GameObject hawker_3 = GameObject.Find("hawker_3");
                    if (hit.collider.gameObject == mapButtons[2] && !windowActivatedAR)     //pressed LPS
                    {
                        windowActivatedLPS = true;
                    }

                    //GameObject BackButton = GameObject.Find("BackButton");
                    if (hit.collider.gameObject == mapButtons[3])                           //pressed back
                    {
                        GameObject BBfeedback = GameObject.Find("BackFB");
                        BBfeedback.renderer.enabled = true;

                        FadeTransition.LoadLevel("MainMenuScene", 0.5f, 0.5f, Color.black);
                    }

                    if (hit.collider.gameObject == mapButtons[8])                            //pressed upgrade shop
                    {
                        FadeTransition.LoadLevel("UpgradeScene", 0.5f, 0.5f, Color.black);
                    }
                }
            }
        }
    }

    void OnGUI()
    {
        //tablet resolution -> 1280w, 752h
        //GUI.Label(new Rect((Screen.width * 813.07f) / 1920, (Screen.height * 728f) / 1080, (Screen.width * 800) / 1920, (Screen.height * 100) / 1080), "Width: " + Screen.width + " Height: " + Screen.height);
        if (windowActivatedAR && !windowActivatedLPS) // Adam road info is selected and lps is not
        {
            blur.renderer.enabled = true;
            hawker_1.renderer.enabled = true;
            mapButtons[4].GetComponent<locationController>().setBoolTrue(true, true);

            mapButtons[3].GetComponent<BoxCollider>().enabled = false;

            GUI.skin.button.normal.background = null;
            GUI.skin.button.hover.background = null;
            GUI.skin.button.active.background = null;

            //if play button is pressed
            if (GUI.Button(new Rect((Screen.width * 813.07f) / 1920, (Screen.height * 728f) / 1080, (Screen.width * 800) / 1920, (Screen.height * 100) / 1080), buttonContent))
            {
                mapButtons[5].GetComponent<LoadingSceneController>().enabled = true;
                mapButtons[5].GetComponent<LoadingSceneController>().setLoadingTrue("ShiokScene");
                mapButtons[4].GetComponent<locationController>().setBoolTrue(false, true);
                windowActivatedAR = false;
                blur.renderer.enabled = false;
                hawker_1.renderer.enabled = false;
                setMap.GetComponent<SelectedMap>().setSelectedMap(true);    //declares adam road is selected
                destroyAll();
            }

            if (GUI.Button(new Rect((Screen.width * 303.03f) / 1920, (Screen.height * 728f) / 1080, (Screen.width * 800) / 1920, (Screen.height * 100) / 1080), leaderContent))
            {
                setMap.GetComponent<SelectedMap>().setSelectedMap(true);
                FadeTransition.LoadLevel("fast_highscore_build", 0.5f, 0.5f, Color.black);

                destroyAll();
            }

            //if close button is pressed
            if (GUI.Button(new Rect(((Screen.width * 80) / 100), ((Screen.height * 13) / 100), (Screen.width * 100) / 1920, (Screen.height * 100) / 1080), closeContent))
            {
                mapButtons[3].GetComponent<BoxCollider>().enabled = true;

                blur.renderer.enabled = false;

                windowActivatedAR = false;

                hawker_1.renderer.enabled = false;

                mapButtons[4].GetComponent<locationController>().setBoolTrue(false, true);
            }
        }

        if (windowActivatedLPS && !windowActivatedAR) // lps info is selected and adam road is not
        {
            blur.renderer.enabled = true;
            hawker_2.renderer.enabled = true;

            mapButtons[4].GetComponent<locationController>().setBoolTrue(true, false);
            mapButtons[3].GetComponent<BoxCollider>().enabled = false;

            GUI.skin.button.normal.background = null;
            GUI.skin.button.hover.background = null;
            GUI.skin.button.active.background = null;

            //if play button is pressed
            if (GUI.Button(new Rect((Screen.width * 813.07f) / 1920, (Screen.height * 728f) / 1080, (Screen.width * 800) / 1920, (Screen.height * 100) / 1080), buttonContent))
            {
                mapButtons[5].GetComponent<LoadingSceneController>().enabled = true;
                mapButtons[5].GetComponent<LoadingSceneController>().setLoadingTrue("ShiokSceneLPS");
                mapButtons[4].GetComponent<locationController>().setBoolTrue(false, true);
                windowActivatedAR = false;
                blur.renderer.enabled = false;
                hawker_2.renderer.enabled = false;
                setMap.GetComponent<SelectedMap>().setSelectedMap(false);    //declares adam road is selected
                destroyAll();
            }

            if (GUI.Button(new Rect((Screen.width * 303.03f) / 1920, (Screen.height * 728f) / 1080, (Screen.width * 800) / 1920, (Screen.height * 100) / 1080), leaderContent))
            {
                setMap.GetComponent<SelectedMap>().setSelectedMap(false);
                FadeTransition.LoadLevel("fast_highscore_build", 0.5f, 0.5f, Color.black);

                destroyAll();
            }
            //if close button is pressed
            //if (GUI.Button(new Rect((Screen.width * xTop) / 1920, (Screen.height * yHeight) / 1080, (Screen.width * 100) / 1920, (Screen.height * 100) / 1080), closeContent))
            if (GUI.Button(new Rect(((Screen.width * 80) / 100), ((Screen.height * 13) / 100), (Screen.width * 100) / 1920, (Screen.height * 100) / 1080), closeContent))
            {
                mapButtons[3].GetComponent<BoxCollider>().enabled = true;

                blur.renderer.enabled = false;

                windowActivatedLPS = false;

                hawker_2.renderer.enabled = false;

                mapButtons[4].GetComponent<locationController>().setBoolTrue(false, false);
            }
        }
    }
}
