﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour 
{
	public AudioClip[] sounds;

//    private static SoundManager _instance = null;
//
//    public static SoundManager instance
//    {
//        get
//        {
//            if (_instance == null)
//            {
//                _instance = GameObject.FindObjectOfType<SoundManager>();
//
//                //Tell unity not to destroy this object when loading a new scene!
//                DontDestroyOnLoad(_instance.gameObject);
//            }
//
//            return _instance;
//        }
//
//    }

	public void StartButton()
	{
		AudioSource.PlayClipAtPoint (sounds[0], transform.position);
	}

	public void MenuButtons()
	{
		AudioSource.PlayClipAtPoint (sounds[1], transform.position);
	}

	public void GameOrders()
	{
		AudioSource.PlayClipAtPoint (sounds[2], transform.position);
	}

	public void GameShiokPoints()
	{
		AudioSource.PlayClipAtPoint (sounds[3], transform.position);
	}

	public void GameLevelUp()
	{
		AudioSource.PlayClipAtPoint (sounds[4], transform.position);
	}

	public void SelectCustomerSound()
	{
		AudioSource.PlayClipAtPoint (sounds[5], transform.position);
	}

	public void SelectTableSound()
	{
		AudioSource.PlayClipAtPoint (sounds[6], transform.position);
	}
}
