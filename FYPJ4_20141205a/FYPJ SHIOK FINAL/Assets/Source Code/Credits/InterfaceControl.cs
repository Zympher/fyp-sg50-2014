﻿using UnityEngine;
using System.Collections;

public class InterfaceControl : MonoBehaviour 
{

	public Vector2 startTouch = Vector2.zero;

	public Sprite credit_first;
	public Sprite credit_second;

	private GameObject CreditImg;
	private SpriteRenderer sprite_renderer;

	private bool to_switch = false;

	// Use this for initialization
	void Start () 
	{
		GameObject CreditImg = GameObject.Find ("Credits");
		sprite_renderer = CreditImg.GetComponent<SpriteRenderer> ();
	}
	void ChangeCredits() 
	{
		if (to_switch == true) 
		{
			sprite_renderer.sprite= credit_second;
			return;
		} 
		else 
		{
			sprite_renderer.sprite= credit_first;
			return;
		}
	}

	// Update is called once per frame
	void Update () 
	{
		/*if (Input.GetKeyUp ("space")) 
		{
			to_switch ^= true;
			ChangeCredits ();
		}*/

		foreach (Touch touch in Input.touches) 
		{
			startTouch = touch.position;
			Ray ray = Camera.main.ScreenPointToRay(touch.position);
			RaycastHit hit;
			
			if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began) 
			{
				if(Physics.Raycast(ray, out hit))
				{
					//GameObject sound = GameObject.Find ("AudioManager");
					//SoundManager startSound = sound.GetComponent<SoundManager>();
					
					//BackButton Interface
					GameObject BackButton = GameObject.Find ("BackButton");
					GameObject sound = GameObject.Find ("AudioManager");
					SoundManager backSound = sound.GetComponent<SoundManager>();

					//NextButton Interface
					GameObject NextButton = GameObject.Find ("NextButton");

					//Credit gameObject
					//GameObject CreditImg = GameObject.Find ("Credits");
					if(hit.collider.gameObject == BackButton)
					{
						backSound.MenuButtons();
						//Loads Main menu.
						FadeTransition.LoadLevel("MainMenuScene", 0.5f,0.5f,Color.black);
					}


					if(hit.collider.gameObject == NextButton) 
					{
						backSound.MenuButtons ();

						//Change Texture to 2nd Credit page.
						to_switch ^= true;
						ChangeCredits ();


					}
				}
			}
		}
	}
}
