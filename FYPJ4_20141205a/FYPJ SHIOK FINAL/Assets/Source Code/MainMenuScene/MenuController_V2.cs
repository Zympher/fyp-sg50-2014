﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;

using System;

public class MenuController_V2 : MonoBehaviour 
{
	public Vector2 startTouch = Vector2.zero;

    public Texture2D StartButton;
    public Texture2D TutorialButton;
    public Texture2D HGButton;
    
    public Texture2D onceButton;

    public int JustOnce = 1;

    Rect StartPos    = new Rect((Screen.width * 565) / 1920, (Screen.height * 200) / 1080, (Screen.width * 800) / 1920, (Screen.height * 250) / 1080);
    Rect TutorialPos = new Rect((Screen.width * 565) / 1920, (Screen.height * 500) / 1080, (Screen.width * 800) / 1920, (Screen.height * 250) / 1080);
    Rect HGPos       = new Rect((Screen.width * 565) / 1920, (Screen.height * 800) / 1080, (Screen.width * 800) / 1920, (Screen.height * 250) / 1080);

    //debug only
    Rect OncePos = new Rect((Screen.width * 465) / 1920, (Screen.height * 200) / 1080, (Screen.width * 80) / 1920, (Screen.height * 25) / 1080);

    GameObject bg;

	// Use this for initialization
	void Start () 
	{
        JustOnce = 1;
        //PlayerPrefs.SetInt("tutOnce", 1);
        print (PlayerPrefs.GetInt("tutOnce"));
	}

    void Resize()
    {
        bg = GameObject.Find("MainMenu");
        SpriteRenderer sr = bg.GetComponent<SpriteRenderer>();

        sr.transform.localScale = new Vector3(1, 1, 1);

        float worldScreenHeight = Camera.main.orthographicSize * 2f;
        float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;

        sr.transform.localScale = new Vector3(worldScreenWidth / sr.sprite.bounds.size.x,
                                           worldScreenHeight / sr.sprite.bounds.size.y, 1);
    }

    void OnGUI()
    {
        GameObject sound = GameObject.Find("AudioManager");
        SoundManager buttonSound = sound.GetComponent<SoundManager>();

        //Start button ============================================
		GameObject startBtn = GameObject.Find ("StartFeedback");
		//if(hit.collider.gameObject == startBtn)
        if(GUI.Button(StartPos, StartButton, new GUIStyle()))
		{

			buttonSound.MenuButtons();

            if (PlayerPrefs.GetInt("tutOnce") == 1)
            {
               // Debug.Log(JustOnce);
                
                //Load Level
                startBtn.renderer.enabled = true;
                FadeTransition.LoadLevel("TutorialScene", 0.5f, 0.5f, Color.black);
                PlayerPrefs.SetInt("tutOnce", 0);
                print(PlayerPrefs.GetInt("tutOnce"));

            }
            else
            {
                FadeTransition.LoadLevel("MapScene", 0.5f, 0.5f, Color.black);
                startBtn.renderer.enabled = true;
            }
            
        }

        //Tutorial button ===========================================
        GameObject tutorialBtn = GameObject.Find("TutorialFeedback");
        if (GUI.Button(TutorialPos, TutorialButton, new GUIStyle()))
        {
            buttonSound.MenuButtons();

            //Load Level
            FadeTransition.LoadLevel("TutorialScene", 0.5f, 0.5f, Color.black);

            tutorialBtn.renderer.enabled = true;
        }

        //hawker guide Button =======================================
        GameObject hawkBtn = GameObject.Find("HGFeedback");
        if (GUI.Button(HGPos, HGButton, new GUIStyle()))
        {
            buttonSound.MenuButtons();

            FadeTransition.LoadLevel("HawkerGuideScene", 0.5f, 0.5f, Color.black);

            //hawker guide Button
            hawkBtn.renderer.enabled = true;
        }

        //button to reset tutOnce = 1;
        if (GUI.Button(OncePos, onceButton, new GUIStyle()))
        {
            buttonSound.MenuButtons();

            PlayerPrefs.SetInt("tutOnce", 1);
            print(PlayerPrefs.GetInt("tutOnce"));

            //hawker guide Button
        }
    }

	// Update is called once per frame
	void Update () 
	{
        Resize();

		foreach (Touch touch in Input.touches) 
		{
			startTouch = touch.position;
			Ray ray = Camera.main.ScreenPointToRay(touch.position);
			RaycastHit hit;

			if(Physics.Raycast(ray, out hit))
			{
				if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began)
				{
					GameObject sound = GameObject.Find ("AudioManager");
					SoundManager buttonSound = sound.GetComponent <SoundManager> ();

					//SG50 Button
					GameObject sgFifty = GameObject.Find("SG50");
					if(hit.collider.gameObject == sgFifty)
					{
						buttonSound.MenuButtons();
                        sgFifty.renderer.enabled = true;

						//Opens SG50 URL
						//Application.OpenURL("http://www.singapore50.sg/");
                        Application.OpenURL("http://data.gov.sg/common/Map.aspx?Theme=HAWKERCENTRE");
					}

					// Credits Button
					GameObject credsBtn = GameObject.Find("CreditsButton");
					if(hit.collider.gameObject == credsBtn)
					{
						buttonSound.MenuButtons();

						FadeTransition.LoadLevel ("CreditsScene",0.5f,0.5f,Color.black);

						//Credits Button
						credsBtn.renderer.enabled = true;
					}

                    GameObject quitBtn = GameObject.Find("QuitButton");
                    if (hit.collider.gameObject == quitBtn)
                    {
                        buttonSound.MenuButtons();
                        Application.Quit();
                        //FadeTransition.LoadLevel("StartMenuScene", 0.5f, 0.5f, Color.black);
                    }
				}
			}
		}
	}

}
