﻿using UnityEngine;
using System.Collections;

public class HawkerGuideController : MonoBehaviour 
{
    public Vector2 startTouch = Vector2.zero;

    // Hawker button textures
    public Texture2D LPSButton;
    public Texture2D AdamRoadButton;
    public Texture2D ChangiVillageButton;
    public Texture2D ExitButton;
    public bool buttonRender  = false;

    // Hawker "button pressed" textures
    public Texture2D LPSFeedback;
    public Texture2D AdamRoadFeedback;
    public Texture2D ExitFeedback;

    Rect AdamRoadPos = new Rect((Screen.width * 1100) / 1920, (Screen.height * 600) / 1080, (Screen.width * 550) / 1920, (Screen.height * 130) / 1080);
    Rect LPSPos = new Rect((Screen.width * 1100) / 1920, (Screen.height * 400) / 1080, (Screen.width * 550) / 1920, (Screen.height * 130) / 1080);
    Rect CVPos = new Rect((Screen.width * 1100) / 1920, (Screen.height * 200) / 1080, (Screen.width * 550) / 1920, (Screen.height * 130) / 1080);
    Rect ExitPos = new Rect((Screen.width * 1700) / 1920, (Screen.height) / 1080, (Screen.width * 200) / 1920, (Screen.height * 200) / 1080);

    GameObject arBtn;
    GameObject arBack;
    GameObject arInfo;
    GameObject arIcon;

    GameObject lpsBtn;
    GameObject lpsBack;
    GameObject lpsIcon;
    GameObject lpsInfo;

    GameObject cvBtn;
    GameObject cvBack;
    GameObject cvIcon;
    GameObject cvInfo;

    GameObject closeBtn;

    GameObject arSlide;
    GameObject lpsSlide;
    GameObject cvSlide;

    GameObject bg;

    private bool ARInfoWindow = false;
    private bool LPSInfoWindow = false;
    private bool CVInfoWindow = false;

    private int slideTimer;

	// Use this for initialization
	void Start () 
    {
	    arBtn = GameObject.Find("ARbutton");
        arBack = GameObject.Find("ARback");
        arInfo = GameObject.Find("ARinfo");
        arIcon = GameObject.Find("ARIcon");

        lpsBtn = GameObject.Find("LPSbutton");
        lpsBack = GameObject.Find("LPSback");
        lpsIcon = GameObject.Find("LPSIcon");
        lpsInfo = GameObject.Find("LPSinfo");

        cvBtn = GameObject.Find("CVbutton");
        cvBack = GameObject.Find("CVback");
        cvIcon = GameObject.Find("CVIcon");
        cvInfo = GameObject.Find("CVinfo");

        //slideshow gameobjects
        arSlide = GameObject.Find("ARslide");
        lpsSlide = GameObject.Find("LPSslide");
        cvSlide = GameObject.Find("CVslide");

        closeBtn = GameObject.Find("Backbutton");
	}

    void Resize()
    {
        bg = GameObject.Find("Background");
        SpriteRenderer sr = bg.GetComponent<SpriteRenderer>();

        sr.transform.localScale = new Vector3(1, 1, 1);


        float worldScreenHeight = Camera.main.orthographicSize * 2f;
        float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;

        sr.transform.localScale = new Vector3(worldScreenWidth / sr.sprite.bounds.size.x,
                                           worldScreenHeight / sr.sprite.bounds.size.y, 1);
    }

    void OnGUI()
    {
        GameObject sound = GameObject.Find("AudioManager");
        SoundManager buttonSound = sound.GetComponent<SoundManager>();

        if (buttonRender == false) 
        {
            // Adam road button press
            if (GUI.Button(AdamRoadPos, AdamRoadButton, new GUIStyle()))
            {
                buttonSound.MenuButtons();

                // Diable Lau Pa Sat info
                LPSInfoWindow = false;
                //lpsIcon.renderer.enabled = false;

                // Show Adam Road info panel
                ARInfoWindow = true;
                //arIcon.renderer.enabled = true;

                CVInfoWindow = false;
            }


            // LPS button press
            if (GUI.Button(LPSPos, LPSButton, new GUIStyle()))
            {
                buttonSound.MenuButtons();

                // Disable Adam Road info
                ARInfoWindow = false;
                //arIcon.renderer.enabled = false;

                // Show Lau Pa Sat info panel
                LPSInfoWindow = true;
                //lpsIcon.renderer.enabled = true;

                CVInfoWindow = false;
            }

            // Chanagi vill button press
            if (GUI.Button(CVPos, ChangiVillageButton, new GUIStyle()))
            {
                buttonSound.MenuButtons();

                // Diable Lau Pa Sat info
                LPSInfoWindow = false;
                //lpsIcon.renderer.enabled = false;

                // Show Adam Road info panel
                ARInfoWindow = false;
                //arIcon.renderer.enabled = true;

                CVInfoWindow = true;
            }
        }

        // Exit button press
        if (GUI.Button(ExitPos, ExitButton, new GUIStyle()))
        {
            buttonSound.MenuButtons();
    
            ARInfoWindow = false;
            arIcon.renderer.enabled = false;

            LPSInfoWindow = false;
            lpsIcon.renderer.enabled = false;

            CVInfoWindow = false;
            cvIcon.renderer.enabled = false;

            FadeTransition.LoadLevel("MainMenuScene", 0.5f, 0.5f, Color.black); 
        }


    }

	// Update is called once per frame
	void Update () 
    {
        Resize();

        //slide show
        if (slideTimer <= 200)
        {
            arSlide.renderer.enabled = true;
            lpsSlide.renderer.enabled = false;
            cvSlide.renderer.enabled = false;

            slideTimer++;
        }
        else if ((slideTimer <= 400) && (slideTimer >= 200))
        {
            lpsSlide.renderer.enabled = true;
            arSlide.renderer.enabled = false;
            cvSlide.renderer.enabled = false;

            slideTimer++;
        }
        else if ((slideTimer <= 600) && (slideTimer >= 400))
        {
            lpsSlide.renderer.enabled = false;
            arSlide.renderer.enabled = false;
            cvSlide.renderer.enabled = true;
            
            slideTimer++;
        }
        else
        {
            slideTimer = 0;
        }

        //hawker windows
        if (ARInfoWindow)
        {
            arInfo.renderer.enabled = true;
            arBack.collider.enabled = true;
            buttonRender = true;
        }
        
        else if (LPSInfoWindow)
        {
            lpsInfo.renderer.enabled = true;
            lpsBack.collider.enabled = true;
            buttonRender = true;
        }

        else if (CVInfoWindow)
        {
            cvInfo.renderer.enabled = true;
            cvBack.renderer.enabled = true;
            buttonRender = true;
        }

        else
        {
            arInfo.renderer.enabled = false;
            arBack.renderer.enabled = false;
            arBack.collider.enabled = false;
            arBtn.renderer.enabled = false;

            lpsInfo.renderer.enabled = false;
            lpsBack.renderer.enabled = false;
            lpsBack.collider.enabled = false;
            lpsBtn.renderer.enabled = false;

            cvInfo.renderer.enabled = false;
            cvBack.renderer.enabled = false;
            cvBack.collider.enabled = false;
           

            buttonRender = false;
        }
        foreach (Touch touch in Input.touches)
        {
            startTouch = touch.position;
            Ray ray = Camera.main.ScreenPointToRay(touch.position);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began)
                {
                    //if (hit.collider.gameObject == AdamRoadButton)
                    //{
                    //    // Play button press sound
                    //    buttonSound.MenuButtons();

                    //    // Show Adam Road info panel
                    //    ARInfoWindow = true;

                    //    // Turn on feedback button boolean
                    //    ButtonFeedback = true;
                    //    // Set selected button to current button pressed
                    //    ButtonSelected = 0;

                    //}

                    //if (hit.collider.gameObject == LPSButton)
                    //{
                    //    buttonSound.MenuButtons();

                    //    LPSInfoWindow = true;

                    //    ButtonFeedback = true;
                    //    ButtonSelected = 1;

                    //}

                    //if (hit.collider.gameObject == arBtn)
                    //{
                    //    buttonSound.MenuButtons();

                    //    arBtn.renderer.enabled = true;
                    //    arIcon.renderer.enabled = true;
                    //    ARInfoWindow = true;                        
                    //}

                    if (hit.collider.gameObject == arBack)
                    {
                        //buttonSound.MenuButtons();

                        arBack.renderer.enabled = true;

                        ARInfoWindow = false;
                    }


                    //if (hit.collider.gameObject == lpsBtn)
                    //{
                    //    buttonSound.MenuButtons();
                    //    lpsBtn.renderer.enabled = true;

                    //    lpsIcon.renderer.enabled = true;

                    //    LPSInfoWindow = true;
                    //}
                }
            }
        }

	}
}
