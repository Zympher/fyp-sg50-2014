﻿using UnityEngine;
using System.Collections;

public class Interface_Control : MonoBehaviour {

	public Vector2 startTouch = Vector2.zero;

    GameObject bg;

	// Use this for initialization
	void Start () 
	{
		GameObject startButton = GameObject.Find ("StartButton");

		startButton.renderer.enabled = false;
	}

    void Resize()
    {
        bg = GameObject.Find("StartUpScreen");
        SpriteRenderer sr = bg.GetComponent<SpriteRenderer>();

        sr.transform.localScale = new Vector3(1, 1, 1);


        float worldScreenHeight = Camera.main.orthographicSize * 2f;
        float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;

        sr.transform.localScale = new Vector3(worldScreenWidth / sr.sprite.bounds.size.x,
                                           worldScreenHeight / sr.sprite.bounds.size.y, 1);
    }

	// Update is called once per frame
	void Update () 
	{
        Resize();

		foreach (Touch touch in Input.touches) 
		{
			startTouch = touch.position;
			Ray ray = Camera.main.ScreenPointToRay(touch.position);
			RaycastHit hit;

			if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began) 
			{
				if(Physics.Raycast(ray, out hit))
				{
					GameObject sound = GameObject.Find ("AudioManager");
					SoundManager startSound = sound.GetComponent<SoundManager>();

					 //Quit Button Interface
					GameObject QuitButton = GameObject.Find ("QuitButton");
					if(hit.collider.gameObject == QuitButton)
					{
						////Quits the Application
						Application.Quit();
					}
					
					// Start Button Interface
					GameObject startButton = GameObject.Find ("StartButton");
					if(hit.collider.gameObject == startButton)
					{
						startSound.StartButton();

						startButton.renderer.enabled = true;
						//Loads Main menu.
						FadeTransition.LoadLevel("MainMenuScene", 0.5f,0.5f,Color.black);
					}
					else
					{
						renderer.enabled = false;
					}
				}
			}
		}
	}
}
