﻿using UnityEngine;
using System.Collections;

public class LoadingSceneController : MonoBehaviour
{
	[SerializeField]
    private bool loading = false;
    bool startButton = false;
    bool enableButton = false;

    public Texture backgroundTexture;
    public Texture[] loadingTexture;
    public Texture[] playButton;

    int index = 0;

    float timerLoad = 8.0f;
    private float xTopPlay = 886.6f;
    private float yHeightPlay = 728f;

    private int counter = 0;

    private string levelName;

    GUIContent buttonContent;

    Rect buttonRect;

    Vector2 mouse;

    void Start()
    {
        buttonContent = new GUIContent();
        buttonRect = new Rect((Screen.width * 310) / 1920, (Screen.height * 320) / 1080, (Screen.width * 1800) / 1920, (Screen.height * 800) / 1080);
    }

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    public void setLoadingTrue(string name)
    {
        //GameObject go = new GameObject("LoadingSceneController");
        //LoadingSceneController instance = go.AddComponent<LoadingSceneController>();
        loading = true;
        levelName = name;
        //instance.StartCoroutine(instance.showLoadScene(name));
        //if(startButton)
        //    StartCoroutine(showLoadScene(name));
        //FadeTransition.LoadLevel("ShiokScene", 0.5f, 0.5f, Color.black);
        //Application.LoadLevel(name);
        //loading = true;
    }

    string getNextLevel()
    {
        return levelName;
    }

    void showLoadScene(string name)
    {
        //Object.DontDestroyOnLoad(this.gameObject);

        //yield return new WaitForSeconds(15.0f);

        if (startButton)
        {
            FadeTransition.LoadLevel(name, 0.5f, 0.5f, Color.black);

            Debug.Log("application transited");

            //testFunc();

            //loading = false;

            Destroy(gameObject);

        }
    }

    //bool looptest()
    //{
    //    if(startButton)
    //        return true;
    //    return false;
    //}

    void testFunc()
    {
        Debug.Log("close this stupid object");
        loading = false;
        gameObject.GetComponent<LoadingSceneController>().enabled = false;
    }

    void Update()
    {
        //Debug.Log(Application.isLoadingLevel.ToString() + "1");
        //if (Application.isLoadingLevel)
        //{
        //    Debug.Log(Application.isLoadingLevel.ToString() + "4");
        //    Debug.Log("scene is loading!");
        //    loading = true;
        //}
        //else
        //    Debug.Log(Application.isLoadingLevel.ToString() + "2");
        //    loading = false;

        mouse = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);

        if (buttonRect.Contains(mouse) && Input.GetMouseButton(0))
        {
            buttonContent.image = playButton[1];
        }
        else
        {
            buttonContent.image = playButton[0];
        }
    }

    void OnGUI()
    {
        if (loading)
        {
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), backgroundTexture, ScaleMode.StretchToFill);

            if (!startButton)
                timerLoad -= Time.deltaTime;
            else
                timerLoad = 0;

            Debug.Log(timerLoad);

            GUI.DrawTexture(new Rect(-80, 50, Screen.width, Screen.height), loadingTexture[index], ScaleMode.StretchToFill);

            if (timerLoad < 0)
            {
                timerLoad = 8.0f;

                index = Random.Range(0, 4);
                counter++;

                if (counter >= 1)
                {
                    enableButton = true;
                    counter = 0;            
                }
            }

            if (enableButton)
            {
                GUI.skin.button.normal.background = null;
                GUI.skin.button.hover.background = null;
                GUI.skin.button.active.background = null;

                //if play button is pressed
                if (GUI.Button(new Rect((Screen.width * 310) / 1920, (Screen.height * 320) / 1080, (Screen.width * 1800) / 1920, (Screen.height * 800) / 1080), buttonContent))
                {
                    startButton = true;
                    showLoadScene(getNextLevel());
                }
            }
            //if (counter == 0)
            //{              
            //    GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), loadingTexture, ScaleMode.StretchToFill);
                
            //    if (timerLoad < 0)
            //    {
            //        timerLoad = 5.0f;
            //        counter++;
            //    }
            //}

            //if (counter == 1)
            //{
            //    GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), loadingTexture2, ScaleMode.StretchToFill);

            //    if (timerLoad < 0)
            //    {                   
            //        timerLoad = 5.0f;
            //        counter++;
            //    }
            //}

            //if (counter == 2)
            //{
            //    GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), loadingTexture3, ScaleMode.StretchToFill);

            //    if (timerLoad < 0)
            //    {
            //        timerLoad = 5.0f;
            //        counter ++;
            //    }
            //}

            //if (counter == 3)
            //{
            //    GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), loadingTexture4, ScaleMode.StretchToFill);

            //    if (timerLoad < 0)
            //    {
            //        timerLoad = 5.0f;
            //        counter ++;
            //    }
            //}

            //if (counter == 4)
            //{
            //    GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), loadingTexture5, ScaleMode.StretchToFill);

            //    if (timerLoad < 0)
            //    {
            //        timerLoad = 5.0f;
            //        counter = 0;
            //    }
            //}
        }
    }
}
