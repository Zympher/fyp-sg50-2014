﻿using UnityEngine;
using System.Collections;

public class SelectedMap : MonoBehaviour 
{
    public bool selectedMap;
	private static SelectedMap _instance = null;

	void Awake ()
	{
		if (_instance == null) {
			//If I am the first instance, make me the Singleton
			_instance = this;
			DontDestroyOnLoad (this);
		} else if (_instance != this) {
			Destroy (gameObject);
		}
	}

    public void setSelectedMap(bool map)
    {
        selectedMap = map;
    }

    public bool getSelectedMap()
    {
        return selectedMap;
    }

	// Update is called once per frame
	void Update () 
    {
	
	}
}
