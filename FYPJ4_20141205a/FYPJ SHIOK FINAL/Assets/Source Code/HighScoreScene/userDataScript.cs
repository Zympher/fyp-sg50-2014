﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;

using System;


[Serializable]
public class PlayerMapScore
{
		public List<float> point = new List<float> (10);
		public List<float> money = new List<float> (10);
	
		public List<string> point_namelist = new List<string> (10);
		public List<string> money_namelist = new List<string> (10);


		public List<int> map_enum = new List<int> (10);

}


[Serializable]
public class userDataScript : MonoBehaviour
{


		static int guidepth = -2;
		//private string path = "userData";
		//private string us
		//private static int listTotal = 10;
		private bool m_bLoad = false;
		private bool m_bSave = false;
		private bool m_bPrint = false;
		private bool m_btestset = false;


		public bool m_bgame_success = false;

		public PlayerMapScore total_storage_data;
		private string filename = "/playerInfo.dat";

		public PlayerMapScore score_AR;
		public PlayerMapScore score_LPS;

		public float total_money;

		GameObject pointcarryfinder;
		PointsCarry temp_carrier;

		int NUM_OF_USER_SCORE;

		private Rect testing_rect;

		//[SerializeField]
		private Rect save_rect;
		//[SerializeField]
		private Rect load_rect;
		//[SerializeField]
		private Rect settest_rect;

		GameObject namebox;
		NameBoxPrompt name_box;

        GameObject menuTut;
        MenuController_V2 theMenu;

		GameObject mapCheck;
		SelectedMap getMap;

		private bool JustOnce = false;

		private static userDataScript _instance = null;

		public static userDataScript instance {
				get {
						if (_instance == null) {
								_instance = GameObject.FindObjectOfType<userDataScript> ();

								//Tell unity not to destroy this object when loading a new scene!
								DontDestroyOnLoad (_instance.gameObject);
						}

						return _instance;
				}

		}

		// Use this for initialization
		void Start ()
		{

            //Find namebox component to retrieve name
            namebox = GameObject.Find("NameBoxPrompt");
            name_box = namebox.GetComponent<NameBoxPrompt>();

            save_rect = new Rect(Screen.width / 2, 300, 100, 100);
            load_rect = new Rect(Screen.width / 2, 450, 100, 100);
            settest_rect = new Rect(Screen.width / 2, 600, 100, 100);

            testing_rect = new Rect(0, 0, 200, 120);

            pointcarryfinder = GameObject.Find("PointsCarrying");
            temp_carrier = pointcarryfinder.GetComponent<PointsCarry>();


            total_storage_data = new PlayerMapScore();

            score_AR = new PlayerMapScore();
            score_LPS = new PlayerMapScore();

            mapCheck = GameObject.Find("selectedMap");
            getMap = mapCheck.GetComponent<SelectedMap>();

				//testing purposes
//		for (int i = 0; i <10; i++) 
//		{
//			//test_data.points.Add (i+1);
//			//test_data.money.Add (i-1);
//			//test_data.name.Add("a");
//			//test_data.money.Add(i);
//			//test_data.name.Add(i.ToString());
//			//Debug.Log (test_data.points[i].ToString());
//
//			score_AR.point.Add (i);
//			score_AR.money.Add (i);
//			score_AR.point_namelist.Add ("point");
//			score_AR.money_namelist.Add ("money");
//
//			score_LPS.point.Add (i);
//			score_LPS.money.Add (i);
//			score_LPS.point_namelist.Add ("point");
//			score_LPS.money_namelist.Add ("money");
//		}
//
//		for (int i = 0; i <20; i++) 
//		{
//			total_storage_data.point.Add (i);
//			total_storage_data.money.Add (i);
//			total_storage_data.point_namelist.Add ("point" + i.ToString());
//			total_storage_data.money_namelist.Add ("money" + i.ToString());
//			total_storage_data.map_enum .Add (-1);
//
//		}

//				for (int i = 10; i<20; i++) {
//						if (i == 10) {
//								total_storage_data.point.Add ((i - 9) * 1000);
//								total_storage_data.money.Add ((i - 9) * 15);
//								total_storage_data.point_namelist.Add ("point" + i.ToString ());
//								total_storage_data.money_namelist.Add ("money" + i.ToString ());
//						} else {
//								total_storage_data.point.Add ((i - 10) * 1000);
//								total_storage_data.money.Add ((i - 10) * 15);
//								total_storage_data.point_namelist.Add ("point" + i.ToString ());
//								total_storage_data.money_namelist.Add ("money" + i.ToString ());
//						}
//				}



				Load ();
				//Save ();
				//Load ();
				//test.some_val = 5;
				//test.some_str = "rand";

				//Debug.Log (test_data.points [0].ToString ());

				//m_bSave = true;
				//m_bLoad = true;
		}

		void Awake ()
		{

				if (_instance == null) {
						//If I am the first instance, make me the Singleton
						_instance = this;
						DontDestroyOnLoad (this);
				} else if (_instance != this) {
						Destroy (gameObject);
						//If a Singleton already exists and you find
						//another reference in scene, destroy it!
						//if(this != _instance) 
						//	Destroy(this.gameObject);
				}

		}

//	void SortList (PlayerMapScore temp)
//	{
//		for (int i = 0; i<temp.point.Count; i++) 
//		{
//			 
//		}
//
//		for (int i = 0; i<temp.money.Count; i++) 
//		{
//
//		}
//	}

		//if users have more than 10 records,delete and insert according to score
		//void CheckAndUpdateList(PlayerMapScore temp, float users_score, bool search_for )
		//{
		//check and find position
		//insert and remove at position
		//}


		//DONE
		int FindScorePosition (PlayerMapScore temp, float users_score, bool search_for)
		{
				//if search for == true,Find Position in point list 
				if (search_for) {
						for (int i = 0; i<temp.point.Count; i++) {
								//special case for 0
								if (i == 0) {
										if (users_score >= temp.point [i])
												return i;
								}
				//special case for 11
				else if (i + 1 == 10) {
										if (users_score <= temp.point [i])
												return -1;
								} else if (users_score >= temp.point [i - 1] && users_score <= temp.point [i + 1])
										return i;
						}
				}
		//else search for == false,Find Position in money list
		else {
						for (int i = 0; i<temp.money.Count; i++) {
								//special case for 0
								if (i == 0) {
										if (users_score >= temp.money [i])
												return i;
								}
				//special case for 11
				else if (i + 1 == 10) {
										if (users_score <= temp.money [i])
												return -1;
								} else if (users_score >= temp.money [i - 1] && users_score <= temp.money [i + 1])
										return i;
						}
				}

				return -1;

		}
		//users are added according to their score,highest at the top ,lowest at the bottom
		public void CheckAndAddARList (string users_name, float users_money, float users_points)
		{
				score_AR.map_enum.Add (0);
				//do for AR point
				if (score_AR.point.Count >= 10) {
						//if value is within highscore range
						if (FindScorePosition (score_AR, users_points, true) != -1) {
								score_AR.point.Insert (FindScorePosition (score_AR, users_points, true), users_points);
								score_AR.point_namelist.Insert (FindScorePosition (score_AR, users_points, true), users_name);
						}
						//return;

				}
		//add according to highest lowest
		else {
						if (score_AR.point.Count < 10) {
								//if user_value is lesser than last number
								if (score_AR.point.Count <= 0) {
										score_AR.point.Add (users_points);
										score_AR.point_namelist.Add (users_name);

								} else if (score_AR.point [score_AR.point.Count] >= users_points) {
										//add at the current lowest place
										score_AR.point.Add (users_points);
										score_AR.point_namelist.Add (users_name);
					
								} 
				//if not 1st and is not the lowest search and insert at the approriate area
				else if (score_AR.point [0] <= users_points) {
										score_AR.point.Insert (0, users_points);
										score_AR.point_namelist.Insert (0, users_name);
					
								}

						} else {
								//insert 
								if (FindScorePosition (score_AR, users_points, true) != -1) {
										score_AR.point.Insert (FindScorePosition (score_AR, users_points, true), users_points);
										score_AR.point_namelist.Insert (FindScorePosition (score_AR, users_points, true), users_name);
					
								}
						}
				}

				//dup for AR money

				if (score_AR.money.Count >= 10) {
						//if value is within highscore range
						if (FindScorePosition (score_AR, users_money, false) != -1) {
								score_AR.point.Insert (FindScorePosition (score_AR, users_money, false), users_money);
								score_AR.point_namelist.Insert (FindScorePosition (score_AR, users_money, false), users_name);
						}
						//return;
				
				}
			//add according to highest lowest
			else {
						if (score_AR.money.Count < 10) {
								if (score_AR.money.Count <= 0) {
										score_AR.money.Add (users_money);
										score_AR.money_namelist.Add (users_name);
								}
								//if user_value is lesser than last number
				else if (score_AR.money [score_AR.money.Count] >= users_money) {
										//add at the current lowest place
										score_AR.money.Add (users_money);
										score_AR.money_namelist.Add (users_name);
						
								} 
					//if not 1st and is not the lowest search and insert at the approriate area
					else if (score_AR.money [0] <= users_money) {
										score_AR.money.Insert (0, users_money);
										score_AR.money_namelist.Insert (0, users_name);
						
								}
					
						} else {
								//insert 
								if (FindScorePosition (score_AR, users_money, false) != -1) {
										score_AR.money.Insert (FindScorePosition (score_AR, users_money, false), users_money);
										score_AR.money_namelist.Insert (FindScorePosition (score_AR, users_money, false), users_name);
						
								}
						}

				}

                print("gets to herer too");
				Save ();
				//EOF
				return;
		}
		public void CheckAndAddLPSList_Point (string users_name, float users_points)
		{
				score_LPS.map_enum.Add (1);
				//do for LPS point
				if (score_LPS.point.Count >= 10) {
						//if value is within highscore range
						if (FindScorePosition (score_LPS, users_points, true) != -1) {
								score_LPS.point.Insert (FindScorePosition (score_LPS, users_points, true), users_points);
								score_LPS.point_namelist.Insert (FindScorePosition (score_LPS, users_points, true), users_name);
						}
						return;
		
				}
	//add according to highest lowest
	else {
						if (score_LPS.point.Count < 10) {

								//print ("less than ten");
								Debug.Log (score_LPS.point.Count.ToString ());
								if (score_LPS.point.Count <= 0) {
										//print ("less than zero");
										score_LPS.point.Add (users_points);
										score_LPS.point_namelist.Add (users_name);

										return;
								}
								//if user_value is lesser than last number
								if (score_LPS.point.Count != 0) {
										if (score_LPS.point [score_LPS.point.Count - 1] >= users_points) {
												//add at the current lowest place
												score_LPS.point.Add (users_points);
												score_LPS.point_namelist.Add (users_name);

												return;
										} else if (score_LPS.point [0] <= users_points) {
												score_LPS.point.Insert (0, users_points);
												score_LPS.point_namelist.Insert (0, users_name);
						
												return;
										}
								}

								//if not 1st and is not the lowest search and insert at the approriate area
		

								//return;
			
						} else {
								//insert 
								if (FindScorePosition (score_LPS, users_points, true) != -1) {
										score_LPS.point.Insert (FindScorePosition (score_LPS, users_points, true), users_points);
										score_LPS.point_namelist.Insert (FindScorePosition (score_LPS, users_points, true), users_name);
									
										return;
								}
						}

						//return;
				}

				return;
		}

		public void CheckAndAddLPSList_Money (string users_name, float users_money)
		{
				//dup for LPS money
				score_LPS.map_enum.Add (1);

				if (score_LPS.money.Count >= 10) {
						//if value is within highscore range
						if (FindScorePosition (score_LPS, users_money, false) != -1) {
								score_LPS.point.Insert (FindScorePosition (score_LPS, users_money, false), users_money);
								score_LPS.point_namelist.Insert (FindScorePosition (score_LPS, users_money, false), users_name);
						}
						return;
			
				}
		//add according to highest lowest
		else {
						if (score_LPS.money.Count < 10) {
								if (score_LPS.money.Count <= 0) {
										score_LPS.money.Add (users_money);
										score_LPS.money_namelist.Add (users_name);
					
										return;
								}
								if (score_LPS.money.Count != 0) {
										//if user_value is lesser than last number
										if (score_LPS.money [score_LPS.money.Count - 1] >= users_money) {
												//add at the current lowest place
												score_LPS.money.Add (users_money);
												score_LPS.money_namelist.Add (users_name);
						
												return;
										} 

					//if not 1st and is not the lowest search and insert at the approriate area
					else if (score_LPS.money [0] <= users_money) {
												score_LPS.money.Insert (0, users_money);
												score_LPS.money_namelist.Insert (0, users_name);
						
												return;
										}
								}

				
						} else {
								//insert 
								if (FindScorePosition (score_LPS, users_money, false) != -1) {
										score_LPS.money.Insert (FindScorePosition (score_LPS, users_money, false), users_money);
										score_LPS.money_namelist.Insert (FindScorePosition (score_LPS, users_money, false), users_name);
					
										return;
								}
						}
				}

		}

		public void CheckAndAddARList_Point (string users_name, float users_points)
		{
				score_AR.map_enum.Add (0);
				//do for LPS point
				if (score_AR.point.Count >= 10) {
						//if value is within highscore range
						if (FindScorePosition (score_AR, users_points, true) != -1) {
								score_AR.point.Insert (FindScorePosition (score_AR, users_points, true), users_points);
								score_AR.point_namelist.Insert (FindScorePosition (score_AR, users_points, true), users_name);
						}
						return;
			
				}
		//add according to highest lowest
		else {
						if (score_AR.point.Count < 10) {
				
								//print ("less than ten");
								Debug.Log (score_AR.point.Count.ToString ());
								if (score_AR.point.Count <= 0) {
										//print ("less than zero");
										score_AR.point.Add (users_points);
										score_AR.point_namelist.Add (users_name);
					
										return;
								}
								//if user_value is lesser than last number
								if (score_AR.point.Count != 0) {
										if (score_AR.point [score_AR.point.Count - 1] >= users_points) {
												//add at the current lowest place
												score_AR.point.Add (users_points);
												score_AR.point_namelist.Add (users_name);
						
												return;
										} else if (score_AR.point [0] <= users_points) {
												score_AR.point.Insert (0, users_points);
												score_AR.point_namelist.Insert (0, users_name);
						
												return;
										}
								}
				
								//if not 1st and is not the lowest search and insert at the approriate area
				
				
								//return;
				
						} else {
								//insert 
								if (FindScorePosition (score_AR, users_points, true) != -1) {
										score_AR.point.Insert (FindScorePosition (score_AR, users_points, true), users_points);
										score_AR.point_namelist.Insert (FindScorePosition (score_AR, users_points, true), users_name);
					
										return;
								}
						}
			
						//return;
				}
		
		
		
		
		
				return;
		}
	
		public void CheckAndAddARList_Money (string users_name, float users_money)
		{
				//dup for LPS money
				score_AR.map_enum.Add (0);
		
				if (score_AR.money.Count >= 10) {
						//if value is within highscore range
						if (FindScorePosition (score_AR, users_money, false) != -1) {
								score_AR.point.Insert (FindScorePosition (score_AR, users_money, false), users_money);
								score_AR.point_namelist.Insert (FindScorePosition (score_AR, users_money, false), users_name);
						}
						return;
			
				}
		//add according to highest lowest
		else {
						if (score_AR.money.Count < 10) {
								if (score_AR.money.Count <= 0) {
										score_AR.money.Add (users_money);
										score_AR.money_namelist.Add (users_name);
					
										return;
								}
								if (score_AR.money.Count != 0) {
										//if user_value is lesser than last number
										if (score_AR.money [score_AR.money.Count - 1] >= users_money) {
												//add at the current lowest place
												score_AR.money.Add (users_money);
												score_AR.money_namelist.Add (users_name);
						
												return;
										} 
					
					//if not 1st and is not the lowest search and insert at the approriate area
					else if (score_AR.money [0] <= users_money) {
												score_AR.money.Insert (0, users_money);
												score_AR.money_namelist.Insert (0, users_name);
						
												return;
										}
								}
				
				
						} else {
								//insert 
								if (FindScorePosition (score_AR, users_money, false) != -1) {
										score_AR.money.Insert (FindScorePosition (score_AR, users_money, false), users_money);
										score_AR.money_namelist.Insert (FindScorePosition (score_AR, users_money, false), users_name);
					
										return;
								}
						}
				}
		
		}
//		//do for LPS point
//		if (score_LPS.point.Count >= 10) 
//		{
//			CheckAndUpdateList (score_LPS,users_points,true);
//			return;
//		} 
//		//add according to highest lowest
//		else 
//		{
//			if(score_LPS.point.Count <=0) 
//			{
//				//if 1st entry just add
//				score_LPS.point.Add (users_points);
//				
//				return;
//			}
//			//check from lowest
//			if(score_LPS.point[score_LPS.point.Count]>= users_points)
//			{
//				//add at the current lowest place
//				score_LPS.point.Add (users_points);
//				
//				return;
//			} 
//			//if not 1st and is not the lowest search and insert at the approriate area
//			else 
//			{
//				//insert 
//				score_LPS.point.Insert (FindScorePosition (score_LPS,users_points,true),users_points);
//
//				if(score_LPS.point[10] != null)
//				score_LPS.point.RemoveAt (10);
//
//				return;
//			}
//			
//			//dup for AR money
//			
//			//EOF
//			return;
//		}



		// Update is called once per frame
		//void Update () {

		//}

		public void Save ()
		{
				if (File.Exists (Application.persistentDataPath + filename)) {
						BinaryFormatter bf = new BinaryFormatter ();
						FileStream file = File.Open (Application.persistentDataPath + "/playerInfo.dat", FileMode.Open, FileAccess.Write);
						PlayerMapScore data = new PlayerMapScore ();

						if (total_storage_data.point.Count <= 20) {
								for (int i = 0; i<total_storage_data.point.Count; i++) {

									
										data.point.Add (total_storage_data.point [i]);
										data.money.Add (total_storage_data.money [i]);
										data.point_namelist.Add (total_storage_data.point_namelist [i]);
										data.money_namelist.Add (total_storage_data.money_namelist [i]);
										data.map_enum.Add (total_storage_data.map_enum [i]);
								}
						
						}
						bf.Serialize (file, data);
						file.Close ();
				} else {
						File.Create (Application.persistentDataPath + filename);
						Save ();
				}

                print("gets to herer too");
				m_bSave = false;
		}
		public void Load ()
		{

				int LPS_count = 0;
				int AR_count = 0;

				int AR_amt_to_add = 0;
				int LPS_amt_to_add = 0;

				if (File.Exists (Application.persistentDataPath + filename)) {
						BinaryFormatter bf = new BinaryFormatter ();
						FileStream file = File.Open (Application.persistentDataPath + "/playerInfo.dat", FileMode.Open, FileAccess.Read);

						PlayerMapScore data = (PlayerMapScore)bf.Deserialize (file);

						for (int i = 0; i<data.point.Count; i++) {

								if (data.point [i] != null) {
										//print ("data not null");
										if (i < 20) {
												//print ("addding");
												total_storage_data.point.Add (data.point [i]);
												total_storage_data.money.Add (data.money [i]);
												total_storage_data.point_namelist.Add (data.point_namelist [i]);
												total_storage_data.money_namelist.Add (data.money_namelist [i]);
												total_storage_data.map_enum.Add (data.map_enum [i]);
										}
								}
								if (data.map_enum [i] == 0) {
										AR_count++;
								
//						score_AR.point[i] = data.point [i];
//						score_AR.money[i] = data.money [i];
//						score_AR.point_namelist[i] =data.point_namelist [i];
//						score_AR.money_namelist[i] =data.money_namelist [i];
//						score_AR.map_enum[i] = data.map_enum [i];
										print (i + "AR");
								} 


				else if (data.map_enum [i] == 1) {
										LPS_count++;

//						score_LPS.point[i] = data.point [i];
//						score_LPS.money[i] = data.money [i];
//						score_LPS.point_namelist[i] =data.point_namelist [i];
//						score_LPS.money_namelist[i] =data.money_namelist [i];
//						score_LPS.map_enum[i] = data.map_enum [i];
										print (i + "LPS");
								}
						}

						AR_amt_to_add = AR_count - score_AR.point.Count;
						LPS_amt_to_add = LPS_count - score_LPS.point.Count;
			

			
						//if need to add then to the adding
						for (int i = 0; i<AR_amt_to_add; i++) {
								if (data.map_enum [i] == 0) {
										if (i < 11) {
												score_AR.point.Add (data.point [i]);
												score_AR.money.Add (data.money [i]);
												score_AR.point_namelist.Add (data.point_namelist [i]);
												score_AR.money_namelist.Add (data.money_namelist [i]);
												score_AR.map_enum.Add (data.map_enum [i]);
										}
								}
						}

			
						for (int i = 0; i<LPS_amt_to_add; i++) {
								if (data.map_enum [i] == 1) {
										if (i < 11) {
												score_LPS.point.Add (data.point [i]);
												score_LPS.money.Add (data.money [i]);
												score_LPS.point_namelist.Add (data.point_namelist [i]);
												score_LPS.money_namelist.Add (data.money_namelist [i]);
												score_LPS.map_enum.Add (data.map_enum [i]);
										}
								}
						}

						print ("Loaded");

						file.Close ();
				} else {
						print ("unable to load");
						return;
				}

				m_bLoad = false;
		}

		public float GetTotalMoney ()
		{
			return total_money;
		}

		public void AddToTotalAmount ()
		{
			float amt = temp_carrier.money;
			total_money += amt;

			return;
		}

		void OnGUI ()
		{
				GUI.depth = guidepth;
//
//				GUILayout.Label ("Total_storage:" + total_storage_data.point.Count.ToString ());
//				GUILayout.Label ("score_LPS_PT:" + score_LPS.point.Count.ToString ());
//				GUILayout.Label ("score_LPS_MN:" + score_LPS.money.Count.ToString ());
//				GUILayout.Label ("score_AR_PT:" + score_AR.point.Count.ToString ());
//				GUILayout.Label ("score_AR_MN:" + score_AR.money.Count.ToString ());


//		if(GUI.Button(save_rect,"Save"))
//		{
//			m_bSave = true;
//		}
//
//		if (GUI.Button (load_rect, "Load")) 
//		{
//			m_bLoad  = true;
//		}
//
//		if (GUI.Button (settest_rect, "Set test Val")) 
//		{
//			m_btestset = true;
//		}

				//GUILayout.BeginArea (testing_rect);

				//GUILayout.Label (Application.persistentDataPath.ToString ());
//		if (GUILayout.Button ("Save")) 
//		{
//			m_bSave = true;
//		}
//		if (m_bSave) 
//		{
//			Save ();
//			//m_bSave = false;
//		}
//		
////		if (GUILayout.Button ("Load")) 
////		{
////			m_bLoad = true;
////		}
//		if (m_bLoad) 
//		{
//			Load ();
//			//m_bLoad = false;
//		}
//		if (GUILayout.Button ("Set test values"))
//		{
//			m_btestset = true;
//			
//		}
//		if (GUILayout.Button ("Print")) 
//		{
//			m_bPrint = !m_bPrint;
//		}
//		if (m_bPrint) 
//		{
//			Print ();
//		}
//		if (m_btestset) 
//		{
//			SetTestVal();
//		}
				//GUILayout.Label ("Path:" + Application.persistentDataPath.ToString ());

//		for(int i = 0; i<10; i++)
//		{
//			if(score_AR.point[i] != null)
//			GUILayout.Label("scoreAR" + i+":" +score_AR.point_namelist[i] + "   " + score_AR.point[i].ToString());
//			
//			//GUILayout.Label("scoreAR" + i+":" +score_AR.money_namelist[i] + "   " + score_AR.money[i].ToString());
//			//GUILayout.Label("scoreLPS" + i+":" +score_LPS.point_namelist[i] + "   " + score_LPS.point[i].ToString());
//			//GUILayout.Label("scoreLPS" + i+":" +score_LPS.money_namelist[i] + "   " + score_LPS.money[i].ToString());
//		}

				//GUILayout.EndArea ();
		
		}

		void Update ()
		{


//		if (File.Exists (Application.persistentDataPath + filename)) {
//			print ("file exists");
//				}

				//print (JustOnce.ToString ());

			if (name_box.JustOnce == false) 
            {
				JustOnce = true;
					
				if (JustOnce) 
                {			
					//Load ();
					AddToTotalAmount ();
					total_storage_data.point.Add (temp_carrier.points);
					total_storage_data.money.Add (temp_carrier.money);
					total_storage_data.point_namelist.Add (name_box.user_name);
					total_storage_data.money_namelist.Add (name_box.user_name);

					//name_box.JustOnce = true;
					//JustOnce = false;

					if (getMap.getSelectedMap ()) 
                    {
						//AddToTotalAmount ();
						print ("gets to herer too");
						total_storage_data.map_enum.Add (0);
//					total_storage_data.point.Add (temp_carrier.points);
//					total_storage_data.money.Add (temp_carrier.money);
//					total_storage_data.point_namelist.Add (name_box.user_name);
//					total_storage_data.money_namelist.Add (name_box.user_name);
					
						//saves into file
						CheckAndAddARList_Money (name_box.user_name, name_box.user_money);
						CheckAndAddARList_Point (name_box.user_name, name_box.user_score);
						Save ();

						name_box.JustOnce = false;
						name_box.time_to_transfer = true;
						JustOnce = false;
					} 

                    else 
                    {
				        CheckAndAddLPSList_Point (name_box.user_name, name_box.user_score);
						CheckAndAddLPSList_Money (name_box.user_name, name_box.user_money);
						print ("reaches here");
						//AddToTotalAmount ();

						total_storage_data.map_enum.Add (1);
//					total_storage_data.point.Add (temp_carrier.points);
//					total_storage_data.money.Add (temp_carrier.money);
//					total_storage_data.point_namelist.Add (name_box.user_name);
//					total_storage_data.money_namelist.Add (name_box.user_name);

						Save ();
						print ("saved");

						name_box.JustOnce = false;
						name_box.time_to_transfer = true;
						JustOnce = false;
					}
				
						temp_carrier.points = 0;
						temp_carrier.money = 0;
						JustOnce = false;
						name_box.JustOnce = true;
						//CheckAndAddARList (name_box.user_name, name_box.user_money, name_box.user_score);
				
				}

			}

			if (score_AR.point.Count > 10)
				score_AR.point.RemoveAt (10);

			if (score_AR.money.Count > 10)
				score_AR.money.RemoveAt (10);

			if (score_AR.point.Count > 10)
				score_AR.point_namelist.RemoveAt (10);

			if (score_AR.money.Count > 10)
				score_AR.money_namelist.RemoveAt (10);


			if (score_LPS.point.Count > 10)
				score_LPS.point.RemoveAt (10);
		
			if (score_LPS.money.Count > 10)
				score_LPS.money.RemoveAt (10);
		
			if (score_LPS.point.Count > 10)
				score_LPS.point_namelist.RemoveAt (10);
		
			if (score_LPS.money.Count > 10)
				score_LPS.money_namelist.RemoveAt (10);
		}
}



