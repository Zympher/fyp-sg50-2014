﻿using UnityEngine;
using System.Collections;

public class HighScoreBackground : MonoBehaviour {

	static int guiDepth = -1;
	public float imageAspect = 1.0f;
	public Texture BG_texture;

	private float NativeWidth = 1050;
	private float NativeHeight = 600;

	public Texture Success_text;
	public Texture Failure_text;

	[SerializeField]
	private Rect banner_rect;

	[SerializeField]
	private Texture facebook_btn;

	[SerializeField]
	private Texture back_btn;

	[SerializeField]
	private Texture try_again_btn;

	[SerializeField]
	private float test_x;
	[SerializeField]
	private float test_y;

	private Rect nameinputRect = new Rect (300,400,385,100);


	[SerializeField]
	private const int max_text_len = 10;

	private bool showTextInput = true;

	public Texture backbtn;
	public Texture backbtn_fb;
	public Texture tryagainbtn;
	public Texture tryagainbtn_fb;
	public Texture facebookbtn;
	public Texture facebookbtn_fb;

	private bool back_bool = false;
	private bool fb_bool = false;
	private bool try_again_bool = false;

	GameObject mapCheck;
	SelectedMap getMap;

	GameObject highscore;
	HighScoreScript getScore;

	GameObject userData;
	userDataScript user_data;

	public bool user_fail;

	// Use this for initialization
	void Start () {
		highscore = GameObject.Find ("Main Camera");
		getScore = highscore.GetComponent<HighScoreScript> ();

		mapCheck = GameObject.Find ("selectedMap");
		getMap = mapCheck.GetComponent<SelectedMap> ();

		userData = GameObject.Find ("Empty User");
		user_data = userData.GetComponent<userDataScript> ();
	
	}
	
	// Update is called once per frame
	void Update () {

		//print(Application.loadedLevelName);

//		if (Application.loadedLevelName == "fast_highscore_build") {
//						print ("received");
//				}
//		else {
//			print ("disabled");
//			enabled = false;
//				}
	}

	void DoMyNameInput(int ID) 
	{
		if (GUI.Button(new Rect(0, 0, 90, 25), "Enter Name"))
		{
			showTextInput = false;
		}


	}

	void OnGUI() 
	{
		GUI.depth = guiDepth;
		float rx = Screen.width / NativeWidth;
		float ry = Screen.height / NativeHeight;



		// Scale width the same as height - cut off edges to keep ratio the same
		GUI.matrix = Matrix4x4.TRS(new Vector3(0, 0, 0), Quaternion.identity, new Vector3(rx , ry, 1));
		// Get width taking into account edges being cut off or extended
		float adjustedWidth = NativeWidth * (rx / ry);



//		for (int i = 0; i<10; i++)
//		{
//			if(getScore.point_list != null)
//			GUILayout.Label("point" + i+":" +getScore.point_name_list[i] + "   " + getScore.point_list[i].ToString());
//		}

		//Draw background
		GUI.DrawTexture (new Rect (-0, 0, 1050, 600), BG_texture, ScaleMode.StretchToFill, true, imageAspect);

		//Draw success or fail texture 
		if (user_data.m_bgame_success)
						GUI.DrawTexture (banner_rect, Success_text);
				else 
						GUI.DrawTexture (banner_rect, Failure_text);
				

		//Draw buttons
		if(GUI.Button(new Rect(915, 475, 117.5f, 117.5f),back_bool? backbtn_fb: backbtn,GUIStyle.none))
		{
			back_bool =! back_bool;
			FadeTransition.LoadLevel ("MapScene",0.5f,0.5f,Color.black);
		}



		if(GUI.Button (new Rect(5, 475, 117.5f, 117.5f),try_again_bool? tryagainbtn_fb: tryagainbtn,GUIStyle.none))
		{
			try_again_bool =! try_again_bool;
			if (getMap.getSelectedMap())
			{
				FadeTransition.LoadLevel ("ShiokScene",0.5f,0.5f,Color.black);
			}
			else 
			{
				FadeTransition.LoadLevel ("UpgradeScene",0.5f,0.5f,Color.black);
			}
		}

	

		if(GUI.Button (new Rect(480, 510, 64, 64),fb_bool? facebookbtn_fb:facebookbtn,GUIStyle.none))
		{
			fb_bool =! fb_bool;
		}




	}




}
