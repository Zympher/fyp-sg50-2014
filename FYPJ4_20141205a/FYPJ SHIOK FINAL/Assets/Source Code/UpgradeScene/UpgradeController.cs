﻿using UnityEngine;
using System.Collections;

public class UpgradeController : MonoBehaviour 
{
    GameObject bg;
    GameObject buyList;

    GameObject CfmUpgrade;
    GameObject CfmIcon;

    GameObject BuyUpgrade;

    GameObject money;
    GameObject mapCheck;

    GameObject sound;

    GameObject buy1true;

    Renderer[] CfmUpRenderer;
    BoxCollider[] CfmUpCollider;
    BoxCollider[] ListCollider;

    GUIStyle largeMoney;

    ContainsUpgrade buyUpgrade;
    userDataScript moneyNow;
    SelectedMap getMap;
    SoundManager backSound;

    //GameObject obj;
    //SpriteRenderer Spr;

    public Sprite[] Feedbacks;
    public Sprite[] Cfmicons;

    float SliderVal = 0.0f;	// Variable for moving on-screen upgrades
	float SwipeSpeed = 0.1f; // Controls the speed for user swiping
	float PreviousTouchPos;

    private float MaxSliderVal = 100.0f;
    private float MinSliderVal = 0.0f;
    private float spriteSize;
    private float buyXvalue;

    private bool buyWindow = false;
    private bool buy1Window = false;
    private bool buy2Window = false;
    private bool buy3Window = false;

    private float upgradeCost1 = 600.0f;
    private float upgradeCost2 = 1300.0f;
    private float upgradeCost3 = 1000.0f;

    //private bool 

	// Use this for initialization
	void Start ()
    {
        buyList = GameObject.Find("BuyList");
        CfmUpgrade = GameObject.Find("ConfirmUpgrade");
        CfmIcon = GameObject.Find("ConfirmIcon");
        BuyUpgrade = GameObject.Find("UpgradesObject");
        money = GameObject.Find("Empty User");
        buy1true = GameObject.Find("Buy1");

        CfmUpRenderer = CfmUpgrade.GetComponentsInChildren<Renderer>();
        CfmUpCollider = CfmUpgrade.GetComponentsInChildren<BoxCollider>();
        ListCollider = buyList.GetComponentsInChildren<BoxCollider>();
        buyUpgrade = BuyUpgrade.GetComponent<ContainsUpgrade>();
        moneyNow = money.GetComponent<userDataScript>();

        largeMoney = new GUIStyle();
        largeMoney.fontSize = 50;
        largeMoney.normal.textColor = Color.green;

        mapCheck = GameObject.Find("selectedMap");
        getMap = mapCheck.GetComponent<SelectedMap>();

        sound = GameObject.Find("AudioManager");
        backSound = sound.GetComponent<SoundManager>();
	}

    void Resize()
    {
        bg = GameObject.Find("Background");
        SpriteRenderer sr = bg.GetComponent<SpriteRenderer>();

        sr.transform.localScale = new Vector3(1, 1, 1);


        float worldScreenHeight = Camera.main.orthographicSize * 2f;
        float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;

        sr.transform.localScale = new Vector3(worldScreenWidth / sr.sprite.bounds.size.x,
                                           worldScreenHeight / sr.sprite.bounds.size.y, 1);

    }

    void BuyListUpdate()
    {
        buyXvalue = -SliderVal/5;

        buyList.transform.position = new Vector3(buyXvalue, 0, 60);
    }

    void ControlUpdate() 
    {
        if (buyWindow)
        {
            foreach (BoxCollider cb in ListCollider)
            {
                cb.enabled = false;
            }
        }
        else
        {
            foreach (BoxCollider cb in ListCollider)
            {
                cb.enabled = true;
                if (buyUpgrade.getBuy1())
                {
                    buy1true.GetComponent<BoxCollider>().enabled = false;
                }
            }

            buy1Window = false;
            buy2Window = false;
            buy3Window = false;
        }

        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hitInfo = new RaycastHit();
            bool hit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo);
            if (hit)
            {
                if (hitInfo.transform.gameObject.name.Contains("Buy1"))
                {
                    buyWindow = true;
                    buy1Window = true;

                    backSound.MenuButtons();

                    CfmUpgrade.renderer.enabled = true;

                    CfmIcon.GetComponent<SpriteRenderer>().sprite = Cfmicons[0];
                    foreach (Renderer r in CfmUpRenderer) 
                    {
                        r.enabled = true;
                    }
                    foreach (BoxCollider cb in CfmUpCollider)
                    {
                        cb.enabled = true;
                    }
                }

                if (hitInfo.transform.gameObject.name.Contains("Buy2"))
                {
                    buyWindow = true;
                    buy2Window = true;

                    backSound.MenuButtons();

                    CfmUpgrade.renderer.enabled = true;

                    CfmIcon.GetComponent<SpriteRenderer>().sprite = Cfmicons[1];

                    foreach (Renderer r in CfmUpRenderer) 
                    {
                        r.enabled = true;
                    }
                    foreach (BoxCollider cb in CfmUpCollider)
                    {
                        cb.enabled = true;
                    }
                }

                if (hitInfo.transform.gameObject.name.Contains("Buy3"))
                {
                    buyWindow = true;
                    buy3Window = true;

                    backSound.MenuButtons();

                    CfmUpgrade.renderer.enabled = true;

                    CfmIcon.GetComponent<SpriteRenderer>().sprite = Cfmicons[2];

                    foreach (Renderer r in CfmUpRenderer) 
                    {
                        r.enabled = true;
                    }
                    foreach (BoxCollider cb in CfmUpCollider)
                    {
                        cb.enabled = true;
                    }
                }

                if (hitInfo.transform.gameObject.name.Contains("BackBtn"))
                {
                    backSound.MenuButtons();

                    FadeTransition.LoadLevel("MapScene", 0.5f, 0.5f, Color.black);
                }

                if (hitInfo.transform.gameObject.name.Contains("ContinueBtn"))
                {
                    backSound.MenuButtons();

                    FadeTransition.LoadLevel("ShiokSceneLPS", 0.5f, 0.5f, Color.black);
                }

                if (buyWindow && hitInfo.transform.gameObject.name.Contains("NoBtn"))
                {
                    //obj = GameObject.Find("NoBtn");
                    //Spr = obj.GetComponent<SpriteRenderer>();

                    //Spr.sprite = Feedbacks[1];

                    backSound.MenuButtons();

                    CfmUpgrade.renderer.enabled = false;

                    foreach (Renderer r in CfmUpRenderer)
                    {
                        r.enabled = false;
                    }
                    foreach (BoxCollider cb in CfmUpCollider)
                    {
                        cb.enabled = false;
                    }

                    buyWindow = false;
                }
                
                if (buyWindow && hitInfo.transform.gameObject.name.Contains("YesBtn"))
                {
                    //obj = GameObject.Find("YesBtn");
                    //Spr = obj.GetComponent<SpriteRenderer>();

                    //Spr.sprite = Feedbacks[1];

                    backSound.SelectCustomerSound();

                    if (buy1Window)
                    {
                        buy2Window = false;
                        buy3Window = false;

                        if (buyUpgrade.getBuy1() == false)
                        {
                            if (moneyNow.total_money >= upgradeCost1)
                            {
                                backSound.GameOrders();

                                moneyNow.total_money -= upgradeCost1;

                                buyUpgrade.setBuy1(true);

                                GameObject obj;

                                obj = GameObject.Find("UpgradeBought");

                                obj.renderer.enabled = true;
                            }
                        }
                    }

                    if (buy2Window)
                    {
                        buy1Window = false;
                        buy3Window = false;
                        if (buyUpgrade.getBuy2() == false)
                        {
							if (moneyNow.total_money >= upgradeCost2)
							{
								backSound.GameOrders();
								
								moneyNow.total_money -= upgradeCost2;
								
								buyUpgrade.setBuy2(true);
								
								GameObject obj;
								
								obj = GameObject.Find("UpgradeBought_2");
								
								obj.renderer.enabled = true;
							}
                        }
                    }

                    if (buy3Window)
                    {
                        buy1Window = false;
                        buy2Window = false;

                        if (buyUpgrade.getBuy3() == false)
                        {
							if (moneyNow.total_money >= upgradeCost3)
							{
								backSound.GameOrders();
								
								moneyNow.total_money -= upgradeCost3;
								
								buyUpgrade.setBuy3(true);
								
								GameObject obj;
								
								obj = GameObject.Find("UpgradeBought_3");
								
								obj.renderer.enabled = true;
							}
                        }
                    }

                    CfmUpgrade.renderer.enabled = false;

                    foreach (Renderer r in CfmUpRenderer)
                    {
                        r.enabled = false;
                    }
                    foreach (BoxCollider cb in CfmUpCollider)
                    {
                        cb.enabled = false;
                    }

                    buyWindow = false;
                }
            }
        }
    }

	void GetTouch()
	{
		if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
		{
			// Gets movement of finger since last frame
			Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;

			SliderVal = -touchDeltaPosition.x * SwipeSpeed;
		}
	}

	// Update is called once per frame
	void Update ()
    {
        Resize();
        BuyListUpdate();
        ControlUpdate();
		GetTouch();
	}

    void OnGUI()
    {
        //SliderVal = GUI.HorizontalSlider(new Rect(Screen.width / 4, ((Screen.height / 100) * 98.0f), 400.0f, 30.0f), SliderVal, MinSliderVal, MaxSliderVal);
	
		Debug.Log (SliderVal);
        GUI.Label(new Rect(Screen.width / 2.5f, (Screen.height / 100) * 5.0f, (Screen.width * 120) / 1920, (Screen.height * 50) / 1080), "$" + moneyNow.total_money, largeMoney);

        if (buy1Window)
        {
            GUI.Label(new Rect(Screen.width / 5.0f, (Screen.height / 100) * 75.0f, (Screen.width * 120) / 1920, (Screen.height * 50) / 1080), "$600", largeMoney);
        }
        if (buy2Window)
        {
            GUI.Label(new Rect(Screen.width / 5.0f, (Screen.height / 100) * 75.0f, (Screen.width * 120) / 1920, (Screen.height * 50) / 1080), "$1300", largeMoney);
        }
        if (buy3Window)
        {
            GUI.Label(new Rect(Screen.width / 5.0f, (Screen.height / 100) * 75.0f, (Screen.width * 120) / 1920, (Screen.height * 50) / 1080), "$1000", largeMoney);
        }
    }
}
