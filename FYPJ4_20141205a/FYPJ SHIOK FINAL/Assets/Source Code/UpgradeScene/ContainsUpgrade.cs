﻿using UnityEngine;
using System.Collections;

public class ContainsUpgrade : MonoBehaviour 
{
    [SerializeField]
    private bool Buy1 = false;
    [SerializeField]
    private bool Buy2 = false;
    [SerializeField]
    private bool Buy3 = false;
    void Awake()
    {
        GameObject UpgradeObj = GameObject.Find("UpgradesObject");

        //if (menuMusic)
      //  {
         //   Destroy(menuMusic.gameObject);
        //}
        //		if (_instance == null) 
        //		{
        //			//If I am the first instance, make me the Singleton
        //			_instance = this;
        //			DontDestroyOnLoad (this);
        //		}
        //
        //
        //        if(_instance != this)
        //        {
        //            //if another instance exists delete them
        //            Destroy (gameObject);
        //        }

        ContainsUpgrade duplicate = GameObject.FindObjectOfType<ContainsUpgrade>();
        if (duplicate && duplicate != this)
        {
            Destroy(duplicate.gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }
    public void setBuy1(bool buy1)
    {
        Buy1 = buy1;
    }
    
    public void setBuy2(bool buy2)
    {
        Buy2 = buy2;
    }
    
    public void setBuy3(bool buy3)
    {
        Buy3 = buy3;
    }

    public bool getBuy1()
    {
        return Buy1;
    }

    public bool getBuy2()
    {
        return Buy2;
    }
    
    public bool getBuy3()
    {
        return Buy3;
    }
	// Update is called once per frame
	void Update () 
    {
	
	}
}
