﻿using UnityEngine;
using System.Collections;

public class LevelUpPopout : MonoBehaviour 
{
	float DISPLAY_TIME = 2.5f;
	float time_to_display = 0.0f;
	const float LEVELUP_DURATION = 1.0f;
	public bool JustOnce = false;
	float elapsed_time = 0.0f;

	public bool JustOnce2 = false;

	GameObject Shiokbar;
	ShiokBar temp;

	[SerializeField]
	float rate_of_alpha = 0.05f;
	//bool JustOnce = true;
//	IEnumerator WaitPopOut()
//	{
//		renderer.enabled = true;
//
//		fLife_Time = Time.time;
//
////		GameObject sound = GameObject.Find ("AudioManager");
////		SoundManager levelSound = sound.GetComponent <SoundManager> ();
////		
////		levelSound.GameLevelUp ();
//
//		yield return new WaitForSeconds (3.0f);
//
//		renderer.enabled = false;
//	}

	// Use this for initialization
	void Start () 
	{
		renderer.enabled = false;

		Shiokbar = GameObject.Find ("ShiokBarUI");
		temp = Shiokbar.GetComponent<ShiokBar> ();
	}

	void DisplayLvlUp()
	{
		renderer.enabled = false;
		Color color = renderer.material.color;
		if (color.a >= 1.0f)
			rate_of_alpha = -rate_of_alpha;
				else if(color.a <=0.0f)
			rate_of_alpha = -rate_of_alpha;


		color.a += rate_of_alpha;
		renderer.material.color = color;


	}

	
	// Update is called once per frame
	void Update () 
	{
		//UpdateColorAlpha ();
		

		if (elapsed_time >= 3.0f) 
		{
			elapsed_time = 3.0f;
		}

		elapsed_time = Time.time;



		if (temp.lvl == 2 && JustOnce == false) {
			time_to_display = Time.time;
						JustOnce = true;

				}


		if (temp.lvl == 3 && JustOnce2 == false) {
			time_to_display = Time.time;
						JustOnce2 = true;
				}

		if (JustOnce == false) {
						time_to_display = Time.time;
						//elapsed_time = Time.time;
				} else {
				}
			//if (JustOnce == true ) 
			//{
				//time_to_display = Time.time;
				//JustOnce = false;

				
				//StartCoroutine(WaitPopOut());
				//JustOnce = false;
			//}

		if(elapsed_time - time_to_display <= DISPLAY_TIME && JustOnce2 )
		{
			//DisplayLvlUp ();
			//renderer.enabled = false;
			//JustOnce = false;
			
			
		}

		if(elapsed_time - time_to_display <= DISPLAY_TIME && JustOnce )
		{
			//DisplayLvlUp ();
			//renderer.enabled = false;
			//JustOnce = false;
			
			
		}
		if(elapsed_time - time_to_display >= DISPLAY_TIME )
		{
			//JustOnce = false;	
			renderer.enabled = false;
		}

	}

	//Debugging
//	void OnGUI()
//	{
//		GUILayout.Label ("time_to_display:"+time_to_display.ToString());
//		GUILayout.Label ("elapsed_time:"+elapsed_time.ToString());
//		GUILayout.Label ((elapsed_time - time_to_display).ToString());
//		GUILayout.Label (JustOnce.ToString ());
//		
//	}
}
