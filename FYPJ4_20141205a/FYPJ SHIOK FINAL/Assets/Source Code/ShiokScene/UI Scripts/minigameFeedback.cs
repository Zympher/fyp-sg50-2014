﻿using UnityEngine;
using System.Collections;

public class minigameFeedback : MonoBehaviour {

	public bool m_bsuccess = false;
	public bool m_brender = false;
	private SpriteRenderer sr;

	private float TIME_TO_RENDER = 1.25f;
	private float TIME_SINCE_RENDER = 0.0f;
	private bool m_bJustOnce = true;

	//private static minigameFeedback _instance = null;

	private float elapsed_time = 0.0f;

	[SerializeField]
	private Sprite[] feedback;

//	void Awake() 
//	{
//		if (_instance == null) {
//			//If I am the first instance, make me the Singleton
//			_instance = this;
//			DontDestroyOnLoad (this);
//		} else if (_instance != this) {
//			Destroy (gameObject);
//		}
//	}

	// Use this for initialization
	void Start () {
	
		sr = GetComponent<SpriteRenderer> ();

		sr.renderer.enabled = false;

		if (m_bsuccess) 
		{
			sr.sprite = feedback[1];
		}
		else 
		{
			sr.sprite = feedback[0];
		}
	}
	
	// Update is called once per frame
	void Update () {
	
		elapsed_time = Time.time;
		sr.renderer.enabled = m_brender;

		if (m_brender && m_bJustOnce) {
						print ("renderering");
						sr.renderer.enabled = true;
						TIME_SINCE_RENDER = Time.time;
						m_bJustOnce = false;



				} else if (m_brender == false) {
						sr.renderer.enabled = false;
				}

//		if (m_bsuccess) 
//		{
//			sr.sprite = feedback[1];
//		}
//		else 
//		{
//			sr.sprite = feedback[0];
//		}

		if (elapsed_time - TIME_SINCE_RENDER >= TIME_TO_RENDER) 
		{
			print ("Deleted");
			sr.renderer.enabled  = false;
			m_bJustOnce = true;
			Destroy (gameObject);

		}

	}
}
