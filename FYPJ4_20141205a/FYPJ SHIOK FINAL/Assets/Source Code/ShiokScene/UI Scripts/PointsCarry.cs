﻿using UnityEngine;
using System.Collections;

public class PointsCarry : MonoBehaviour 
{
	public int points = 0;
	public int money = 0;

	private static PointsCarry _instance = null;
	// Use this for initialization
	void Awake ()
	{
		if (_instance == null) {
			//If I am the first instance, make me the Singleton
			_instance = this;
			DontDestroyOnLoad (this);
		} else if (_instance != this) {
			Destroy (gameObject);

		}
	}

	// Update is called once per frame
	void Update () 
	{
	
	}
}
