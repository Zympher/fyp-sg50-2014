﻿using UnityEngine;
using System.Collections;

public class Timer : MonoBehaviour
{
    private float timer = 180.0f;
    public bool timerStarted = false;

    public bool isDone = false;
    private bool satay = true;
    public float clockTime = 60.0f;
    private float MGtime = 0.001f;
    private float MGtime_delay = 0.0001f;
    //font manipulation
    public GUISkin guiSkin;
    public Texture2D miniGtimer;	//drag in texture here
    //public Texture2D timerBg;
    GUIStyle largeFont;
    GUIStyle largeFont2;

    GameObject obj;
    ShiokAI ai;

    // Use this for initialization
    void Start()
    {
        if (timerStarted == false)
        {
            timerStarted = true;
        }
        else
        {
            timerStarted = false;
        }

        largeFont = new GUIStyle();
        largeFont.fontSize = 33;
        largeFont.normal.textColor = Color.white;

        largeFont2 = new GUIStyle();
        largeFont2.fontSize = 33;
        largeFont2.normal.textColor = Color.white;
        largeFont2.normal.background = miniGtimer;
        largeFont2.hover.background = miniGtimer;
        largeFont2.active.background = miniGtimer;
        largeFont2.alignment = TextAnchor.MiddleCenter;

        obj = GameObject.Find("Main Camera");
        ai = obj.GetComponent<ShiokAI>();
    }

    // Update is called once per frame
    void Update()
    {
        //testing timer
        if (Input.GetKeyDown(KeyCode.T))
        {
            if (timerStarted == false)
            {
                timerStarted = true;
            }
            else
            {
                timerStarted = false;
            }
        }

        if (timerStarted)
        {
            timer -= Time.deltaTime;

            if (timer < 0)
            {
                timerStarted = false;
                timer = 0.0f;
                FadeTransition.LoadLevel("fast_highscore_build", 0.5f, 1, Color.black);
            }
        }

        if (ai.IsInMiniGameMode)
        {
            if (satay)
            {
                if (ai.isSatay)
                {
                    setSatayTime();
                }
            }

            MGtime -= Time.deltaTime;
            //Debug.Log("code is running, MGtime: " + MGtime);

            if (MGtime < 0.00011)
            {
                isDone = true;
                //Debug.Log("code entered loop");
            }
        }
        else
        {
            MGtime = 0.002f;
            isDone = false;
            satay = true;
        }
    }

    private void setSatayTime()
    {
        MGtime = 0.0025f;
        satay = false;
    }

    public void TimeOfDay()
    {
        if (timer <= 180.0f || timer > 90.0f)
        {
            // Morning
        }
        else if (timer <= 90.0f || timer > 60.0f)
        {
            // Afternoon
        }
        else if (timer <= 60.0f)
        {
            // Night
        }
    }

    public void setIsDone(bool temp)
    {
        isDone = temp;
    }

    public float getTimer()
    {
        return timer;
    }

    public void addTime(float newTime)
    {
        timer += newTime;
    }

    void OnGUI()
    {
        int minutes = Mathf.FloorToInt(timer / 60F);
        int seconds = Mathf.FloorToInt(timer - minutes * 60);

        float temp = MGtime * 10000;
        int mgTimer = (int)temp;

        int minutes2 = Mathf.FloorToInt(mgTimer / 60F);
        int seconds2 = Mathf.FloorToInt(mgTimer - minutes2 * 60);

        //To give the format of 0:00 E.G: 3 Minutes -> 3:00
        string niceTime = string.Format("{0:0}:{1:00}", minutes, seconds);

        string niceTime2 = string.Format("{0:0}:{1:00}", minutes2, seconds2);

        // for custom fonts
        GUI.skin = guiSkin;

        GUI.depth = -10;
        // Time
        GUI.Label(new Rect((Screen.width * 209) / 1920, (Screen.height * 1023) / 1080, (Screen.width * 120) / 1920, (Screen.height * 50) / 1080), niceTime, largeFont);

        // MG timer
        if (ai.IsInMiniGameMode)
            GUI.Label(new Rect((Screen.width * 1700) / 1920, (Screen.height * 40) / 1080, (Screen.width * 120) / 1920, (Screen.height * 50) / 1080), niceTime2, largeFont2);
    }
}