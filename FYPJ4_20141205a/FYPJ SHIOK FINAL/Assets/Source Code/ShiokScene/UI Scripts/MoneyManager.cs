﻿using UnityEngine;
using System.Collections;

public class MoneyManager : MonoBehaviour 
{
	//public int money = 0;

	//font manipulation
	public GUISkin guiSkin;

	public ShiokAI sAI;

	//public Texture2D timerBg;
	GUIStyle largeFont;

    //Discount upgrade here?
    public bool extraMoneyActive = false;

	// Use this for initialization
	void Start () 
	{
		largeFont = new GUIStyle ();
		largeFont.fontSize = 33;
		largeFont.normal.textColor = Color.white;
	}
	
	// Update is called once per frame
	void Update () 
	{
	    
	}

	void OnGUI()
	{
		GameObject go2 = GameObject.Find ("PointsCarrying");
		PointsCarry moneyNow = go2.GetComponent <PointsCarry> ();

		//For custom fonts
		GUI.skin = guiSkin;

		if(sAI.IsInMiniGameMode == false)
		{
			GUI.depth = -10;
			//GUI.Label (new Rect( (Screen.width*896) / 1920, (Screen.height*34) / 1080 ,(Screen.width*120) / 1920,(Screen.height*50) / 1080),"$" + moneyNow.money, largeFont);
		}
	}

	public void IncreaseMoney()
	{
        {
            GameObject go2 = GameObject.Find("PointsCarrying");
            PointsCarry moneyNow = go2.GetComponent<PointsCarry>();

            moneyNow.money += 100;
        }
	}

	public void BuyTwoTimes()
	{
		GameObject go2 = GameObject.Find ("PointsCarrying");
		PointsCarry moneyNow = go2.GetComponent <PointsCarry> ();

		moneyNow.money -= 50;
	}

	public void BuyExtraTime()
	{
		GameObject go2 = GameObject.Find ("PointsCarrying");
		PointsCarry moneyNow = go2.GetComponent <PointsCarry> ();

		moneyNow.money -= 50;
	}
    
}
