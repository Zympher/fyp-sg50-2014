﻿using UnityEngine;
using System.Collections;

public class ShiokBar : MonoBehaviour
{
    //Size of shiok bar
    public float maxShiok = 1000;
    public float curShiok = 0;

    public Texture2D gameWin;
    public Texture2D gameLose;

    private float shiokBarMain = 517f;
    private float shiokBarLength;

    //SHIOK MODE TEXTURE
    public Texture2D shiokModeTex;
    //textures for shiok meter
    public Texture2D bgImage;
    public Texture2D fgImage;
    public Texture2D frame;
    public Texture2D comboFrame;
    //Combo textures
    public int combo = 1;
    public Texture2D[] multiplier;
    public int comboImage = 0;

    // lvl textures
    public Texture2D[] lvlBase;
    public int lvlup = 0;

    //Game over counter
    public Texture2D gameOverCount;
    public int gameOverText;

    //kallang wave ui
    public Texture2D bgKW;

    //public KallangWave kgW;
    public ShiokAI sAI;
    public PointsCarry pp;

    public Texture2D item1;
    public Texture2D item3;
    public Texture2D item1Activated;
    public Texture2D item3Activated;
    public Texture2D item1DisplayName;
    public Texture2D item3DisplayName;

    // Skill bar icon textures
    public Texture2D skillbarTex;
    public Texture2D trayTex;
    public Texture2D orderTex;
    public Texture2D kallangTex;
    public Texture2D LowButton; // cleanup
    public Texture2D MidButton; // cleanup
    public Texture2D fillTex;

    // Variables for skill bar node fill scaling
    private float nodeScale;
    private float ScreenWidth;
    private float ScreenHeight;

    // Skill bar node fill positions
    Rect TrayNodePos;
    Rect OrderNodePos;
    Rect KWNodePos;
    Rect FourthNodePos;
    Rect FifthNodePos;

    // Power up control
    public bool buttonOpen = true;
    public bool buttonOpen2 = true;

    // points
    //public int points = 0;
    public Texture2D scoreBase;
    public int lvl = 1;
    public bool shiokMode = false;

    //font manipulation
    public GUISkin guiSkin;
    GUIStyle largeFont;
    GUIStyle largeScore;
    GUIStyle gameOverNum;

    //shiok mode timer
    public int shiokTimer;
    public float counter;

    private int screenW = Screen.width;
    private int screenH = Screen.height;

    public int screenWD = 0;
    public int screenHG = 0;

    GameObject mapCheck;
    SelectedMap getMap;

    GameObject getTimer;
    Timer timingNow;

    GameObject userData;
    userDataScript user_data;

    GameObject NameBoxPrompter;
    NameBoxPrompt name_box_pt;

    // Shiok mode waiting time
    IEnumerator Wait()
    {
        yield return new WaitForSeconds(2.0f);

        // reset combo multiplier to 1
        combo = 1;
        comboImage = 0;

        curShiok -= 600 * Time.deltaTime;

        if ((lvl == 1) && (curShiok < 0))
        {
            shiokMode = false;
        }
        else if (curShiok < 0)
        {
            curShiok = 1000;
            lvl--;
            lvlup--;
        }

        if (lvl < 1)
        {
            lvl = 1;
            lvlup = 0;
        }
    }

    // Use this for initialization
    void Start()
    {
        //fonts
        largeFont = new GUIStyle();
        largeScore = new GUIStyle();
        gameOverNum = new GUIStyle();
        //largeFont.fontSize = 53;
        largeScore.fontSize = 33;
        gameOverNum.fontSize = 33;
        largeFont.normal.textColor = Color.white;
        largeScore.normal.textColor = Color.blue;
        gameOverNum.normal.textColor = Color.white;

        shiokTimer = 0;
        gameOverText = 14;

        if (screenW == 1920 && screenH == 1080)
        {
            //largeFont.fontSize = 53;
            largeScore.fontSize = 33;
        }
        mapCheck = GameObject.Find("selectedMap");
        getMap = mapCheck.GetComponent<SelectedMap>();

        getTimer = GameObject.Find("Pause");
        timingNow = getTimer.GetComponent<Timer>();

        userData = GameObject.Find("Empty User");
        user_data = userData.GetComponent<userDataScript>();

        NameBoxPrompter = GameObject.Find("NameBoxPrompt");
        name_box_pt = NameBoxPrompter.GetComponent<NameBoxPrompt>();
    }

    // Update is called once per frame
    void Update()
    {
        AdjustCurrentShiok(0);

        if (Input.GetKeyDown(KeyCode.L))
        {
            curShiok += (combo * 5);

            // Conditions for shiok bar
            if ((curShiok > maxShiok) && (lvl == 3))
            {
                curShiok = 1000;
            }
            else if (curShiok > maxShiok)
            {
                curShiok = 0;
                lvl++;
                lvlup++;
            }

            // set level amount
            if (lvl > 3)
            {
                lvl = 3;
                lvlup = 2;
            }

        }

        if (Input.GetKey(KeyCode.A))
        {
            curShiok += (combo * 5);

            // Conditions for shiok bar
            if ((curShiok > maxShiok) && (lvl == 3))
            {
                curShiok = 1000;
            }
            else if (curShiok > maxShiok)
            {
                curShiok = 0;
                lvl++;
                lvlup++;
            }

            // set level amount
            if (lvl > 3)
            {
                lvl = 3;
                lvlup = 2;
            }
        }


        //Testing purposes
        if (Input.GetKeyDown(KeyCode.P)) // code for special orders to increase combos
        {
            //each special order + 1 combo
            combo++;
            comboImage++;


            if (combo > 15)
            {
                combo = 15;
                comboImage = 14;
            }

        }

        //Testing purposes
        if (Input.GetKeyDown(KeyCode.O)) // <<<< change to break combo, If (Special Order Fail)
        {
            pp.points -= 500;
            combo = 1;
            comboImage = 0;

            if (pp.points < 0)
            {
                pp.points = 0;
            }
        }

        //For Shiok Mode
        // input codes OR call functions that you want shiok mode to be used on
        // E.G: Auto Serve, Instant cleaned tables, etc
        if ((lvl == 3) && (curShiok == 1000)) // Key to activate shiok mode
        {
            if (shiokMode == false)
            {
                shiokMode = true;
            }
        }

        // shiok mode activated
        if (shiokMode)
        {
            StartCoroutine(Wait());
        }
    }

    void SetNodes(float scale)  // Sets skill bar fill size and initialises positions
    {
        ScreenWidth = ((Screen.width * 140) / 1920) * scale;
        ScreenHeight = ((Screen.height * 115) / 1080) * scale;



        // Initialise skill bar node fill positions
        TrayNodePos = new Rect((Screen.width * 649) / 1920, (Screen.height * 955) / 1080, ScreenWidth * scale, ScreenHeight * scale);
        OrderNodePos = new Rect((Screen.width * 765) / 1920, (Screen.height * 850) / 1080, ScreenWidth * scale, ScreenHeight * scale);
        KWNodePos = new Rect((Screen.width * 765) / 1920, (Screen.height * 850) / 1080, ScreenWidth * scale, ScreenHeight * scale);
        FourthNodePos = new Rect((Screen.width * 765) / 1920, (Screen.height * 850) / 1080, ScreenWidth * scale, ScreenHeight * scale);
        FifthNodePos = new Rect((Screen.width * 765) / 1920, (Screen.height * 850) / 1080, ScreenWidth * scale, ScreenHeight * scale);
    }

    void GetNode(int node)  // Renders skill bar fill (Red background)
    {

        switch (node)
        {
            case 1: GUI.DrawTexture(TrayNodePos, fillTex);
                Debug.Log("SWITCHED");
                break;
            case 2: GUI.DrawTexture(OrderNodePos, fillTex);
                break;
            case 3: GUI.DrawTexture(KWNodePos, fillTex);
                break;
            case 4: GUI.DrawTexture(FourthNodePos, fillTex);
                break;
            case 5: GUI.DrawTexture(FifthNodePos, fillTex);
                break;
            default:
                Debug.Log("Skill bar fill : Rendering error");
                break;
        }
    }

    void RenderSkillbarNodes()
    {
        if ( curShiok >= 50 && curShiok <= 200 )
        {
            GetNode(1);
        }
        else if ( curShiok >= 200 && curShiok <= 350 )
        {
            GetNode(2);
        }
        else if ( curShiok >= 350 && curShiok <= 500 )
        {
            GetNode(3);
        }
    }

    void RenderSkillBar()
    {
        RenderSkillbarNodes();
        //Skill Bar background
        GUI.DrawTexture(new Rect((Screen.width * 650) / 1920, (Screen.height * 820) / 1080, (Screen.width * 1100) / 1920, (Screen.height * 250) / 1080), skillbarTex);

        //Skill bar fill
        


        //Skill bar icons
        GUI.DrawTexture(new Rect((Screen.width * 638) / 1920, (Screen.height * 940) / 1080, (Screen.width * 170) / 1920, (Screen.height * 130) / 1080), trayTex);
        GUI.DrawTexture(new Rect((Screen.width * 780) / 1920, (Screen.height * 885) / 1080, (Screen.width * 110) / 1920, (Screen.height * 70) / 1080), orderTex);
        GUI.DrawTexture(new Rect((Screen.width * 905) / 1920, (Screen.height * 970) / 1080, (Screen.width * 110) / 1920, (Screen.height * 90) / 1080), kallangTex);

        //Skill bar button sprites
         //gameObject.GetComponent<SpriteRenderer>().sprite = SkillButtonSprite[0];
         //gameObject.transform.localScale.Set(1.0, 1.0f, 1.0f);
         //gameObject.transform.position = new Vector3(-4,-9);

    }

    //GUI
    void OnGUI()
    {
        SetNodes(1.0f);

        ////////////////////////////////////////
        ///                TEXT              ///
        ////////////////////////////////////////

        //For custom fonts
        GUI.skin = guiSkin;

        if (sAI.IsInMiniGameMode == false)
        {
            RenderSkillBar();
            GUI.DrawTexture(TrayNodePos, fillTex);

            //Score Base
            GUI.DrawTexture(new Rect((Screen.width * 21) / 1920, (Screen.height * 9) / 1080, (Screen.width * 256) / 1920, (Screen.height * 130) / 1080), scoreBase);

            // Points
            GUI.Label(new Rect((Screen.width * 60) / 1920, (Screen.height * 27) / 1080, (Screen.width * 120) / 1920, (Screen.height * 50) / 1080), "" + pp.points, largeScore);

            ////////////////////////////////////////
            ///            COMBO BAR             ///
            ////////////////////////////////////////

            ////draw background image
            //GUI.DrawTexture(new Rect((Screen.width * 676) / 1920, (Screen.height * 102) / 1080, (Screen.width * shiokBarMain) / 1920, (Screen.height * 45) / 1080), bgImage);

            ////fore ground
            //GUI.DrawTexture(new Rect((Screen.width * 676) / 1920, (Screen.height * 102) / 1080, (Screen.width * shiokBarLength) / 1920, (Screen.height * 45) / 1080), fgImage);

            ////Frame
            //GUI.DrawTexture(new Rect((Screen.width * 664) / 1920, (Screen.height * 11) / 1080, (Screen.width * 629) / 1920, (Screen.height * 145) / 1080), frame);

            ////Combo multiplier
            //GUI.DrawTexture(new Rect((Screen.width * 1160) / 1920, (Screen.height * 30) / 1080, (Screen.width * 130) / 1920, (Screen.height * 123) / 1080), multiplier[comboImage]);

            //// Level base 
            //GUI.DrawTexture(new Rect((Screen.width * 680) / 1920, (Screen.height * 29) / 1080, (Screen.width * 170) / 1920, (Screen.height * 67) / 1080), lvlBase[lvlup]);

            //Game over Counter
            GUI.DrawTexture(new Rect((Screen.width * 1671) / 1920, (Screen.height * 8) / 1080, (Screen.width * 246) / 1920, (Screen.height * 188) / 1080), gameOverCount);

            // Text game over counter
            GUI.Label(new Rect((Screen.width * 1800) / 1920, (Screen.height * 45) / 1080, (Screen.width * 120) / 1920, (Screen.height * 50) / 1080), gameOverText + "/15", gameOverNum);
        }




        GameObject moneyHave = GameObject.Find("PointsCarrying");
        PointsCarry moneyNow = moneyHave.GetComponent<PointsCarry>();
        pp = moneyNow;

        if (getMap.getSelectedMap())
        {
            if (shiokMode)
            {
                buttonOpen = true;
                buttonOpen2 = true;
            }
            else if (lvl == 2)
            {
                GameObject goTo2X = GameObject.Find("TwoXPower");
                GameObject lvl2Lock = GameObject.Find("Level2LOCKED");
                TwoXController twoX = goTo2X.GetComponent<TwoXController>();

                if (buttonOpen)
                {
                    twoX.renderer.enabled = true;
                    //2x Power Up icon
                    //GUI.DrawTexture (new Rect ((Screen.width*480) / 1920, (Screen.height*169) / 1080, (Screen.width*269) / 1920, (Screen.height*254) / 1080), item1);

                    //GUI.DrawTexture (new Rect ((Screen.width*578) / 1920, (Screen.height*974) / 1080, (Screen.width*100) / 1920, (Screen.height*100) / 1080), item3Locked);
                }
                else if (twoX.activated == true && moneyNow.money >= 500)
                {
                    //GUI.DrawTexture (new Rect ((Screen.width*480) / 1920, (Screen.height*169) / 1080, (Screen.width*269) / 1920, (Screen.height*254) / 1080), item1Activated);

                    //GUI.DrawTexture (new Rect ((Screen.width*578) / 1920, (Screen.height*974) / 1080, (Screen.width*100) / 1920, (Screen.height*100) / 1080), item3Locked);
                }

                if (twoX.displaySkill == true)
                {
                    twoX.renderer.enabled = false;
                    //GUI.DrawTexture (new Rect ((Screen.width*804) / 1920, (Screen.height*1002) / 1080, (Screen.width*329) / 1920, (Screen.height*54) / 1080), item1DisplayName);
                }

                if (twoX.renderer.enabled == false)
                {
                    lvl2Lock.renderer.enabled = true;
                }
                else
                {
                    lvl2Lock.renderer.enabled = false;
                }
            }
            else if (lvl == 3)
            {

                GameObject goToTimeEx = GameObject.Find("ExtendedTimePower");
                GameObject lvl3Lock = GameObject.Find("Level3LOCKED");
                ExtendedTime exTime = goToTimeEx.GetComponent<ExtendedTime>();

                if (buttonOpen2)
                {
                    exTime.renderer.enabled = true;
                    //GUI.DrawTexture (new Rect ((Screen.width*689) / 1920, (Screen.height*934) / 1080, (Screen.width*100) / 1920, (Screen.height*100) / 1080), item1);
                    //Time Extention
                    //GUI.DrawTexture (new Rect ((Screen.width*1150) / 1920, (Screen.height*163) / 1080, (Screen.width*253) / 1920, (Screen.height*255) / 1080), item3);
                }
                else if (moneyNow.money >= 1000)
                {
                    exTime.activated = true;

                    //GUI.DrawTexture (new Rect ((Screen.width*680) / 1920, (Screen.height*925) / 1080, (Screen.width*109) / 1920, (Screen.height*108) / 1080), item1Activated);
                    //GUI.DrawTexture (new Rect ((Screen.width*1150) / 1920, (Screen.height*163) / 1080, (Screen.width*253) / 1920, (Screen.height*255) / 1080), item3Activated);
                }

                if (exTime.displaySkill == true)
                {
                    exTime.renderer.enabled = false;
                    //GUI.DrawTexture (new Rect ((Screen.width*804) / 1920, (Screen.height*1002) / 1080, (Screen.width*329) / 1920, (Screen.height*54) / 1080), item3DisplayName);
                }

                if (exTime.renderer.enabled == false)
                {
                    lvl3Lock.renderer.enabled = true;
                }
                else
                {
                    lvl3Lock.renderer.enabled = false;
                }
            }
        }


        //Shiok Mode conditions
        if ((lvl == 3) && (curShiok == 1000) && shiokTimer < 100)
        {

            GUI.depth = -20;
            // SHIOK MODE
            GUI.DrawTexture(new Rect((Screen.width * 1) / 1920, (Screen.height * 1) / 1080, (Screen.width * 1920) / 1920, (Screen.height * 1080) / 1080), shiokModeTex);
            shiokTimer = shiokTimer + 1;

        }
        else
        {
            shiokTimer = 0;
        }

        // Kallang Wave UI
        GameObject getTimer = GameObject.Find("Pause");
        Timer timingNow = getTimer.GetComponent<Timer>();

        //when game ends,add user earned amount to user total
        if (gameOverText >= 15 || timingNow.getTimer() <= 0)
        {
            //player failed
            //user_data.m_bgame_success = false;
            

            // if counter hasn't reached 10
            if (counter <= 20)
            {   // Render "Lose" screen
                GUI.DrawTexture(new Rect((Screen.width * 825) / 1920, (Screen.height * 500) / 1080, (Screen.width * 256) / 1920, (Screen.height * 130) / 1080), gameWin);
                counter = counter + 1;
            }

            // after counter has reached limit
            else
            {   // transition to highscore screen
                Application.LoadLevel("fast_highscore_build");
                //FadeTransition.LoadLevel("fast_highscore_build", 0.5f, 0.5f, Color.black);
                counter = 0;
            }

        }

        // when timer ends and the timer hits zero
        else if (gameOverText < 15 && timingNow.getTimer() <= 0)
        {
            // player succeeded
            //user_data.m_bgame_success = true;

            if (counter <= 20)
            {   // render "Win" screen
                GUI.DrawTexture(new Rect((Screen.width * 1) / 1920, (Screen.height * 1) / 1080, (Screen.width * 1920) / 1920, (Screen.height * 1080) / 1080), gameWin);

                counter = counter + 1;
            }
            else
            {   // transition to highscore screen
                FadeTransition.LoadLevel("fast_highscore_build", 0.5f, 0.5f, Color.black);
                counter = 0;
            }
        }

        if (timingNow.getTimer() <= 60.0f && timingNow.getTimer() >= 53.0f)
        {
            //kgW.enabled= true;

            //GUI.DrawTexture (new Rect ((Screen.width*1) / 1920, (Screen.height*1) / 1080, (Screen.width*1920) / 1920, (Screen.height*1080) / 1080), bgKW);
        }
        else
        {
            //kgW.enabled = false;
        }
    }

    //skill buttons

    public void AdjustCurrentShiok(float adj)
    {
        curShiok += adj;

        if (curShiok < 0)
        {
            curShiok = 0;
        }

        if (curShiok > maxShiok)
        {
            curShiok = maxShiok;
        }

        if (maxShiok < 1)
        {
            maxShiok = 1;
        }

        shiokBarLength = shiokBarMain * (curShiok / (float)maxShiok);
    }
}
