﻿using UnityEngine;
using System.Collections;

public class ExtendedTime : MonoBehaviour 
{
	public bool activated = false;
	public bool displaySkill = false;

	IEnumerator Active()
	{
		activated = true;
		
		yield return new WaitForSeconds (0.1f);
		
		activated = false;
		
	}

	IEnumerator DisplaySkillName()
	{
		yield return new WaitForSeconds (2.0f);
		
		displaySkill = false;
	}

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		GameObject go = GameObject.Find ("ShiokBarUI");
		GameObject go2 = GameObject.Find ("PointsCarrying");
		ShiokBar shiokBar = go.GetComponent <ShiokBar> ();
		PointsCarry moneyNow = go2.GetComponent <PointsCarry> ();
		
		if ( shiokBar.lvl == 3 && shiokBar.buttonOpen2 && moneyNow.money >= 50)
		{ 
			collider.enabled = true;
		}
		else
		{
			collider.enabled = false;
		}
		
		if (displaySkill == true)
		{
			StartCoroutine(DisplaySkillName());
		}
	}

	void OnMouseDown()
	{
		GameObject go = GameObject.Find ("ShiokBarUI");
		ShiokBar shiokBar = go.GetComponent <ShiokBar> ();

		GameObject goToTimer = GameObject.Find ("Pause");
		Timer timerPlus = goToTimer.GetComponent <Timer> ();

		StartCoroutine (Active());

        timerPlus.addTime(60.0f);

		collider.enabled = false;
		
		shiokBar.buttonOpen2 = false;
		
		displaySkill = true;
		
		go.GetComponent<MoneyManager> ().BuyExtraTime ();
	}
}
