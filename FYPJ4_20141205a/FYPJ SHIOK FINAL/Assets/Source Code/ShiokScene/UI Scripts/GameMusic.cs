﻿using UnityEngine;
using System.Collections;

public class GameMusic : MonoBehaviour 
{

	//private static GameMusic _instance = null;

	void Awake () 
	{
		GameObject menuMusic = GameObject.Find ("MenuMusic");

		if (menuMusic) 
		{
			Destroy (menuMusic.gameObject);
		}
//		if (_instance == null) 
//		{
//			//If I am the first instance, make me the Singleton
//			_instance = this;
//			DontDestroyOnLoad (this);
//		}
//
//
//        if(_instance != this)
//        {
//            //if another instance exists delete them
//            Destroy (gameObject);
//        }

		GameMusic duplicate = GameObject.FindObjectOfType<GameMusic> ();
		        if (duplicate && duplicate != this ) 
		        {
		            Destroy(duplicate.gameObject);
		        }
		
		        DontDestroyOnLoad(gameObject);
		
	}


//	void Update()
//	{
//		//Destroy (this);
//	}
}
