﻿using UnityEngine;
using System.Collections;

public class RiceCountUI : MonoBehaviour {

	private SpriteRenderer sr;

	private GameObject SatayMini;
	private SatayGrillMiniGame satay_temp;

	public Sprite [] rice_UI;
	// Use this for initialization
	void Start () {

		sr = GetComponent<SpriteRenderer> ();

		SatayMini = GameObject.Find ("Satay(Clone)");
		satay_temp = SatayMini.GetComponent<SatayGrillMiniGame> ();

		if (satay_temp.satay_order_type == SATAY_SET_TYPE.WITH_RICE) {
						sr.enabled = true;
				} else 
						sr.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {

		if (satay_temp.satay_order_type == SATAY_SET_TYPE.WITH_RICE) {
			sr.enabled = true;
		} else 
			sr.enabled = false;


		if (satay_temp.rice_placed)
						sr.sprite = rice_UI [1];
				else 
						sr.sprite = rice_UI [0];
			
	}
}
