﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum COOKED_TYPE
{
    RAW,
    UNDERCOOKED,
    COOKED,
    OVERCOOKED,
    DESTROYED,
}

public enum SATAY_SET_TYPE
{
    WITH_RICE,
    NO_RICE,
}

public class SatayGrillMiniGame : MonoBehaviour
{

    public GameObject grillObj;
    public GameObject plateObj;
    public GameObject sataybagObj;
    public GameObject ricebagObj;
    public GameObject backgroundObj;
	public GameObject riceDisplayObj;

	public GameObject satayUILeft;
	public GameObject satayUIRight;
	public GameObject satayUIRice;

    private float MAX_NUM_ON_GRILL = 5;

    public GameObject[] sataystickPrefab;
    private GameObject[] sataystickObj;

    public GameObject ricebagPrefab;

	private int NUM_OF_DESTROYED_SATAY = 0;
	private int NUM_OF_COOKED_SATAY = 0;
    public int NUM_OF_ORDERED_SATAY;
    private int NUM_OF_RICE = 1;
    private int MAX_SATAY = 10;
    private int MIN_SATAY = 5;

    public SATAY_SET_TYPE satay_order_type;

    GameObject overall_timer;

    bool m_bprintTestVal = false;

    private float lastRealtimeSinceStartup;

    private GameObject currentmovingObj;

    private bool m_bUpdateOnce = true;
    private int current_satay_count = 0;
    private int scene_satay_count = 0;
    public int num_of_satay_in_plate;

    private int ricebagcounter = 0;

    private const float MINIGAME_FINISH_DELAY = 2.0f;
    private float fMiniGameFinished_Time;

    private Vector3 finger_pos;

    GameObject obj;
    Timer getTime;

    GameObject obj2;
    pauseinMG setTS;

	public bool five_or_ten; // if true 5 else 10 

	[SerializeField]
	private GUISkin satay_font_skin;
	//update order
	[SerializeField]
	private Rect output_text;

	GUIStyle Satay_Font;

	public bool rice_placed; 

	private int current_satay_index;

	[SerializeField]
	private float NativeHeight;

	[SerializeField]
	private float NativeWidth;

	private Vector3 Scale;

	[SerializeField]
	private float x_PositionRatio;
	[SerializeField]
	private float y_PositionRatio;


	private float x_pos;
	private float y_pos;

	[SerializeField]
	private float x_widthRatio;
	[SerializeField]
	private float y_heightRatio;

	public bool order_fulfilled = false;


	private float time_to_fade;

	int temp = 0;
    // Use this for initialization
    void Start()
    {

		Satay_Font = new GUIStyle ();
		Satay_Font.normal.textColor = Color.white;


		//NativeHeight = 1024;
		//NativeWidth = 512;

		x_pos = Screen.width;
		y_pos = Screen.width;

		if (Random.value >= 0.5f)
						five_or_ten = true;
				else 
						five_or_ten = false;
        if (five_or_ten)
						NUM_OF_ORDERED_SATAY = 5;
				else 
						NUM_OF_ORDERED_SATAY = 10;

        //random between with rice and no rice
        satay_order_type = (SATAY_SET_TYPE)Random.Range(0, 2);

        grillObj = (GameObject)Instantiate(grillObj,
                                           grillObj.transform.position,
                                           grillObj.transform.rotation);

        plateObj = (GameObject)Instantiate(plateObj,
                                           plateObj.transform.position,
                                           plateObj.transform.rotation);

        sataybagObj = (GameObject)Instantiate(sataybagObj,
                                             sataybagObj.transform.position,
                                             sataybagObj.transform.rotation);


		riceDisplayObj = (GameObject)Instantiate (riceDisplayObj,
		                                           riceDisplayObj.transform.position,
		                                           riceDisplayObj.transform.rotation);

        //ricebagObj = new GameObject[NUM_OF_RICE];
        //for (int i = 0; i < NUM_OF_RICE; i++)
        //{
            ricebagObj = (GameObject)Instantiate(ricebagObj,
                                                 ricebagObj.transform.position,
                                                 ricebagObj.transform.rotation);
        //}

        backgroundObj = (GameObject)Instantiate(backgroundObj,
                                                backgroundObj.transform.position,
                                                backgroundObj.transform.rotation);

		satayUILeft = (GameObject)Instantiate (satayUILeft,
		                                       satayUILeft.transform.position,
		                                       satayUILeft.transform.rotation);

		satayUIRight = (GameObject)Instantiate (satayUIRight,
		                                        satayUIRight.transform.position,
		                                        satayUIRight.transform.rotation);
		satayUIRice =(GameObject)Instantiate (satayUIRice,
		                                      satayUIRice.transform.position,
		                                      satayUIRice.transform.rotation);

        ricebagObj.collider.enabled = true;

//        ricebagPrefab = (GameObject)Instantiate(ricebagPrefab,
//                                                ricebagPrefab.transform.position,
//                                                ricebagPrefab.transform.rotation);

        //sataystickObj = new GameObject[NUM_OF_ORDERED_SATAY];
        //for (int i = 0; i < NUM_OF_ORDERED_SATAY; i++)
        //{
        //    sataystickObj[i] = (GameObject)Instantiate(sataystickPrefab[i], // swapped NUM_OF_ORDERED_SATAY for i, swap back if doesn't work
        //                                               sataystickPrefab[i].transform.position,
        //                                               sataystickPrefab[i].transform.rotation);
        //}

        currentmovingObj = null;

        fMiniGameFinished_Time = -1;


        obj = GameObject.Find("Pause");
        getTime = obj.GetComponent<Timer>();

        obj2 = GameObject.Find("PauseInMiniGame");
        setTS = obj2.GetComponent<pauseinMG>();

		if (satay_order_type == SATAY_SET_TYPE.WITH_RICE) {
			Satay_Font.fontSize = 10;
		} else {
			Satay_Font.fontSize = 15;
		}

    }

    public void DestroyMiniGame()
    {
        //Destroy(backgroundObj);
        //Destroy(grillObj);
        //Destroy(plateObj);
        //Destroy(sataybagObj);
        //Destroy(ricebagObj);
        //if (ricebagPrefab != null)
        //{
        //    Destroy(ricebagPrefab);
        //}
        ////foreach (GameObject obj in ricebagPrefab)
        ////{
        ////    Destroy(obj);
        ////}
        //foreach (GameObject obj in sataystickPrefab)
        //{
        //    Destroy(obj);
        //}
        //for (int i = 0; i < NUM_OF_ORDERED_SATAY; i++) 
        //{
        //    //if (sataystickPrefab[i] != null) 
        //    //{
            
        //    Destroy(sataystickPrefab[i]);

        //    //}
        //}
        //Destroy(gameObject);
        GameObject[] temp;
        temp = GameObject.FindGameObjectsWithTag("Satay");
        for (int i = 0; i < temp.Length; i++) 
        {

            Destroy(temp[i]);
        }
       // Destroy(gameObject.FindGameObjectsWithTag("Satay"));
    }

    // Update is called once per frame
    void Update()
    {

//		if (satay_order_type == SATAY_SET_TYPE.NO_RICE) {
//						if (NUM_OF_COOKED_SATAY == NUM_OF_ORDERED_SATAY) 
//								order_fulfilled = true;
//			else
//				order_fulfilled = false;
//						
//
//				} 
//		else if (satay_order_type == SATAY_SET_TYPE.WITH_RICE) 
//		{
//			if(NUM_OF_COOKED_SATAY == NUM_OF_ORDERED_SATAY && rice_placed)
//				order_fulfilled = true;
//			else
//				order_fulfilled = false;
//		}
//		else 
//			order_fulfilled = false;

		
        if (getTime.isDone)
        {
            fMiniGameFinished_Time = Time.time;
            //Debug.Log("its innnn");
            //DestroyMiniGame();
        }

        //convert touch to World points
       finger_pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        finger_pos.z = sataybagObj.transform.position.z;

        RaycastHit2D hit = Physics2D.Raycast(finger_pos, Vector2.zero);

        //if (current_satay_count < 0)
        //{
        //    current_satay_count = 0;
        //}

        //for testing purposes
       // for (int i = 0; i < NUM_OF_ORDERED_SATAY; i++)
       // {
        if (Input.GetMouseButtonDown(0))
        {
            for (int i = 0; i < scene_satay_count; i++)
            {
                //m_bUpdateOnce = true;
                if (sataystickPrefab[i].GetComponent<SatayController>().satay_lvl == COOKED_TYPE.OVERCOOKED && m_bUpdateOnce)
                {
                    sataystickPrefab[i].GetComponent<SatayController>().satay_lvl = COOKED_TYPE.DESTROYED;
                    current_satay_count --;
                }
                
                    if (sataystickPrefab[i] != null)
                    {
                        //switch current gameObject
                        if (sataystickPrefab[i].collider.bounds.Contains(finger_pos) && currentmovingObj == null && m_bUpdateOnce)
                        {
                            //print (sataystickPrefab [i].collider.enabled.ToString ());
                            //print ("clicked on another satay");
                            currentmovingObj = sataystickPrefab[i];
                            current_satay_index = i;
                            //m_bUpdateOnce = false;

                        }

                    }
                    if (m_bUpdateOnce && sataystickPrefab[i].GetComponent<SatayController>().m_bDeleted )
                    {
                        NUM_OF_DESTROYED_SATAY ++;
                        //					m_bUpdateOnce = false;
                    }
                
            }



            if (ricebagPrefab.collider.bounds.Contains(finger_pos) && currentmovingObj == null)
            {
                currentmovingObj = ricebagPrefab;
            }


            if (sataybagObj.collider.bounds.Contains(finger_pos) && currentmovingObj == ricebagPrefab)
            {
                currentmovingObj = null;
            }

            //spawn one ricebag
            if (ricebagObj.collider.bounds.Contains(finger_pos) && currentmovingObj == null)
            {
                if (ricebagcounter < 1)
                {
                    print("ricebag");
                    ricebagPrefab = (GameObject)Instantiate(ricebagPrefab,
                                                ricebagObj.transform.position,
                                                    ricebagObj.transform.rotation);


                    ricebagcounter++;
                    currentmovingObj = ricebagPrefab;
                }


            }




            //spawn a satay		/*added a boolean to make sure only one spawn*/

            if (sataybagObj.collider.bounds.Contains(finger_pos) && current_satay_count < MAX_NUM_ON_GRILL &&
                    NUM_OF_ORDERED_SATAY > scene_satay_count - NUM_OF_DESTROYED_SATAY - NUM_OF_COOKED_SATAY && currentmovingObj == null && m_bUpdateOnce)
                {
                    //Debug.Log ("spawn satay");

                    //				if(scene_satay_count >= 9) 
                    //				{
                    //					sataystickPrefab[9+temp] = (GameObject)Instantiate (sataystickPrefab [9+temp],
                    //					                                                    sataybagObj.transform.position,
                    //					                                                    sataybagObj.transform.rotation);
                    //					currentmovingObj = sataystickPrefab [9+temp];
                    //					current_satay_count++;
                    //				}
                    
                        sataystickPrefab[scene_satay_count] = (GameObject)Instantiate(sataystickPrefab[scene_satay_count],
                                                                                 sataybagObj.transform.position,
                                                                                 sataybagObj.transform.rotation);

                        //sataystickPrefab[current_satay_count].name = name + current_satay_count.ToString();

                        //currentmovingObj.collider.enabled = true;

                        //scale satay to bigger size when inside grill

                        currentmovingObj = sataystickPrefab[scene_satay_count];
                        scene_satay_count++;
                        current_satay_count++;
                        //i++;

                        //m_bUpdateOnce = false;
                    

                }

            }
       
                //moving sprite according to fingerpos
                if (currentmovingObj != null)
                {
                    currentmovingObj.transform.position = finger_pos;
                }


            //when finger is released,dont allow movement
            if (Input.GetMouseButtonUp(0))
            {
			if(currentmovingObj != null)
			{
			if (currentmovingObj == sataystickPrefab[current_satay_index])
                {
                    //if within plate do collider false
                    if (currentmovingObj.GetComponent<SatayController>().IsInsideSet == true 
				    && currentmovingObj.GetComponent<SatayController>().satay_lvl  == COOKED_TYPE.COOKED )
                    {
                        //print("colliders off");
                        currentmovingObj.collider.enabled = false;
						currentmovingObj.renderer.enabled = false;
                        current_satay_count--;
                        num_of_satay_in_plate++;
                        currentmovingObj = null;
						NUM_OF_COOKED_SATAY++;

                        if (current_satay_count >= NUM_OF_ORDERED_SATAY)
                        {
                            current_satay_count = NUM_OF_ORDERED_SATAY;
                        }
                    }

                }
                if (currentmovingObj == ricebagPrefab)
                    {

                        if (currentmovingObj.GetComponent<RiceBagController>().IsInsideSet == true)
                        {
                            // collider not working

                            currentmovingObj.collider.enabled = false;
							currentmovingObj.renderer.enabled = false;
                            currentmovingObj = null;
							rice_placed = true;
                        }
                    }
                
			}
                currentmovingObj = null;

            }

            if (!IsMiniGameFinish())
            {
                //this thing is supposed to end the game
                //check sataystickPrefab if all are IsInsideSet, if yes end game if not check if time = 0 then end

			if( satay_order_type == SATAY_SET_TYPE.NO_RICE)
			{
			if (NUM_OF_ORDERED_SATAY >= num_of_satay_in_plate) //rice not in plate
                {
					order_fulfilled = true;
                    fMiniGameFinished_Time = Time.time;
                }
			}
			else if ( satay_order_type >= SATAY_SET_TYPE.WITH_RICE)
                {
                    if (rice_placed == true && NUM_OF_ORDERED_SATAY == num_of_satay_in_plate)
                    {
						order_fulfilled = true;
                        fMiniGameFinished_Time = Time.time;
                    }
                }
            }
            else
            {
                // destroy minigame when 
                if (Time.time > fMiniGameFinished_Time + MINIGAME_FINISH_DELAY)
                {
					order_fulfilled = false;
                    DestroyMiniGame();
                }
            }
        }

    //}

    public bool IsMiniGameFinish()
    {
        if (fMiniGameFinished_Time > 0)
            return true;
        return false;
    }

    void OnGUI()
    {
		GUI.skin = satay_font_skin;

		 Scale.x = Screen.width / NativeWidth;
		 Scale.y = Screen.height / NativeHeight;
		 Scale.z = 1;

		//float newFontSize = Scale.x * Satay_Font.fontSize;
		//Satay_Font.fontSize = (int)newFontSize;

		float rx = Screen.width / NativeWidth;
		float ry = Screen.height / NativeHeight;

//		// Scale width the same as height - cut off edges to keep ratio the same
//		GUI.matrix = Matrix4x4.TRS(new Vector3(0, 0, 0), Quaternion.identity, new Vector3(ry, ry, 1));
//		// Get width taking into account edges being cut off or extended
//		float adjustedWidth = NativeWidth * (rx / ry);

		//GUILayout.Label ("scene_satay_count: " + scene_satay_count.ToString ());
		//GUILayout.Label ("current_satay_count:" + current_satay_count.ToString ());


		//Previous GUI Implementation of UI
//		if (satay_order_type == SATAY_SET_TYPE.NO_RICE)
//			GUI.Label ( new Rect((output_text.x - 20) * x_pos *(x_widthRatio/100),(output_text.y + 10)*y_pos*(y_heightRatio/100),100*(x_PositionRatio/100) ,100*(y_PositionRatio/100) ) , NUM_OF_COOKED_SATAY.ToString () + "/" + NUM_OF_ORDERED_SATAY + " Satay",Satay_Font);
//		else
//			GUI.Label (new Rect ( output_text.x * x_pos *(x_widthRatio/100), output_text.y * y_pos *(y_heightRatio/100) ,100*(x_PositionRatio/100) , 100*(y_PositionRatio/100)  ), NUM_OF_COOKED_SATAY.ToString () + "/" + NUM_OF_ORDERED_SATAY + " Satay",Satay_Font);
//		
//		if (satay_order_type == SATAY_SET_TYPE.WITH_RICE) {
//			
//			GUI.Label (new Rect ((output_text.x - 10) * x_pos *(x_widthRatio/100), (output_text.y + 30) * y_pos *(y_heightRatio/100), 100*(x_PositionRatio/100) , 100*(y_PositionRatio/100)), "RICE NOT PLACED",Satay_Font);
//		} 
//		else if (satay_order_type == SATAY_SET_TYPE.WITH_RICE && rice_placed) 
//		{
//			GUI.Label (new Rect (output_text.x * x_pos *(x_widthRatio/100), (output_text.y + 30) * y_pos * (y_heightRatio/100), 100*(x_PositionRatio/100), 100*(y_PositionRatio/100) ), "RICE PLACED",Satay_Font);
//		}

//		if (satay_order_type == SATAY_SET_TYPE.NO_RICE)
//			GUI.Label ( new Rect(output_text.x - 20,output_text.y + 10,100 * Scale.x,100 * Scale.y) , NUM_OF_COOKED_SATAY.ToString () + "/" + NUM_OF_ORDERED_SATAY + " Satay",Satay_Font);
//		else
//			GUI.Label (new Rect ( output_text.x, output_text.y ,100 * Scale.x, 100 * Scale.y ), NUM_OF_COOKED_SATAY.ToString () + "/" + NUM_OF_ORDERED_SATAY + " Satay",Satay_Font);
//
//		if (satay_order_type == SATAY_SET_TYPE.WITH_RICE) {
//
//			GUI.Label (new Rect (output_text.x - 10, output_text.y + 30, 100 * Scale.x, 100 * Scale.y), "RICE NOT PLACED",Satay_Font);
//				} 
//		else if (satay_order_type == SATAY_SET_TYPE.WITH_RICE && rice_placed) 
//		{
//			GUI.Label (new Rect (output_text.x, output_text.y + 30, 100 * Scale.x, 100 * Scale.y), "RICE PLACED",Satay_Font);
//		}


		//DEBUG
//        GUILayout.Label ("X:" + finger_pos.x.ToString());
//        GUILayout.Label ("Y:" + finger_pos.y.ToString());
//        GUILayout.Label ("Z:" + finger_pos.z.ToString());
//
//        ////GUILayout.Label("Grill_ZZ: " + grillObj.transform.position.z.ToString());
//        //GUILayout.Label("Ricebag_z: " + ricebagObj.transform.position.z.ToString());
//        if (currentmovingObj != null && currentmovingObj != ricebagPrefab)
//        {
//            GUILayout.Label(currentmovingObj.ToString());
//            GUILayout.Label(currentmovingObj.GetComponent<SatayController>().IsInsideGrill.ToString());
//            GUILayout.Label(currentmovingObj.GetComponent<SatayController>().IsInsideSet.ToString());
//
//            GUILayout.Label(currentmovingObj.transform.position.x.ToString());
//            GUILayout.Label(currentmovingObj.transform.position.y.ToString());
//            GUILayout.Label(currentmovingObj.transform.position.z.ToString());
//            GUILayout.Label(currentmovingObj.collider.enabled.ToString());
//			GUILayout.Label(currentmovingObj.renderer.enabled.ToString ());
//        }

        //if (currentmovingObj == ricebagPrefab) 
        //{
        //    GUILayout.Label(currentmovingObj.ToString());
        //    //GUILayout.Label(currentmovingObj.GetComponent<RiceBagController>().IsInsideGrill.ToString());
        //    GUILayout.Label(currentmovingObj.GetComponent<RiceBagController>().IsInsideSet.ToString());

        //    GUILayout.Label(currentmovingObj.transform.position.x.ToString());
        //    GUILayout.Label(currentmovingObj.transform.position.y.ToString());
        //    GUILayout.Label(currentmovingObj.transform.position.z.ToString());
        //    GUILayout.Label(currentmovingObj.collider.enabled.ToString());

        //}


        //for (int i = 0; i < NUM_OF_ORDERED_SATAY; i++)
        //{
        //    GUILayout.Label(sataystickPrefab[i].GetComponent<SatayController>().getCookedLvl().ToString());
        //}

        //GUILayout.Label(current_satay_count.ToString());
        //GUILayout.Label(NUM_OF_ORDERED_SATAY.ToString());
        //if (satay_order_type == SATAY_SET_TYPE.WITH_RICE)
        //{
        //    GUILayout.Label("need rice");
        //}
        //else if (satay_order_type == SATAY_SET_TYPE.NO_RICE)
        //{
        //    GUILayout.Label("no rice");
        //}

    }
}
