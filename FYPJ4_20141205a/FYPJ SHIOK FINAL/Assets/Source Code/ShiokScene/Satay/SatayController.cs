﻿using UnityEngine;
using System.Collections;

public class SatayController : MonoBehaviour
{

    //to calculate timer
    //GameObject satay_timer;

    // 3 seconds to cooked state
    private float TIME_TO_COOKED = 3.0f;
    private float TIME_SINCE_PLACED = 0.0f;
    private float elapsed_time = 0.0f;
    public float CookingTimer = 0.0001f;

    //public bool IsPlaced = false;
    public bool IsInsideSet = false;
    public bool IsInsideGrill = false;

	[SerializeField]
	private float TIME_TO_CHANGE_STATE = 1.25f;



	[SerializeField]
	private float TIME_TO_FADE = 2.0f;
	private float TIME_SINCE_BURNED = 0.0f;


    private bool clicked = false;
    private GameObject plateObj; //satayPlate;
    private GameObject grillObj;
    public Sprite[] sprites;
    private int image_count = 0;
    private SpriteRenderer spriterenderer;

    private bool JustOnce = true;
    public COOKED_TYPE satay_lvl;

	private bool after_grill = false;

	[SerializeField]
	private float rate_alpha_fade = 0.05f;
	private float temp_alpha = 1.0f;
    //private float lastRealtimeSinceStartup;

	public bool m_bDeleted = false;
    // Use this for initialization
    void Start()
    {

        plateObj = GameObject.Find("plateObj(Clone)");
        grillObj = GameObject.Find("grillObj(Clone)");
        spriterenderer = GetComponent<SpriteRenderer>();
        collider.enabled = true;
        //sprites = new Sprite [4];
    }

    // Update is called once per frame
    void Update()
    {
//		if(satay_lvl == COOKED_TYPE.RAW)
//		{
//			IsInsideSet = false;
//		}

       
        elapsed_time = Time.time;

        spriterenderer.sprite = sprites[image_count];

		if (IsInsideGrill) {
						//transform.localScale = new Vector3 (0.8f, 0.8f, 1f);
			after_grill = true;
				} 
		else {
			//transform.localScale = new Vector3 (0.3f,0.3f, 1f);
			}

		if (after_grill) 
		{
			transform.localScale = new Vector3 (0.8f, 0.8f, 1f);
		}


		
		if(satay_lvl == COOKED_TYPE.OVERCOOKED)
		{
			temp_alpha -= rate_alpha_fade; 
			spriterenderer.color = new Color(1f,1f,1f,temp_alpha);
			
			if(temp_alpha <= 0 )
			{
				m_bDeleted = true;
			renderer.enabled = false;
			collider.enabled = false;
				//Destroy(gameObject);
			}
				//

           
			
		}

        if (satay_lvl == COOKED_TYPE.DESTROYED)
        {
            renderer.enabled = false;
        }

		if (satay_lvl == COOKED_TYPE.RAW && !IsInsideGrill && !IsInsideSet && after_grill) 
		{
			collider.enabled = true;	
		}

        if (satay_lvl == COOKED_TYPE.UNDERCOOKED && !IsInsideGrill && !IsInsideSet && after_grill)
        {
            collider.enabled = true;
        }

        if (grillObj != null)
        {       
            if (grillObj.collider.bounds.Contains(gameObject.transform.position))
            {
                IsInsideGrill = true;
                //Debug.Log("the satay collided with the grill");

                if (IsInsideGrill && JustOnce)
                {
                    // the satay is inside the grill, add time since placed to timer to make the satay cook
                    //Debug.Log("The satay is inside the grill");
                    TIME_SINCE_PLACED = Time.time;
                    JustOnce = false;

                }

				if (elapsed_time - TIME_SINCE_PLACED >= TIME_TO_CHANGE_STATE * CookingTimer && !JustOnce)
                {
                    if (image_count < 3)
                    {
                        satay_lvl++;
                        image_count += 1;
                    }
                    TIME_SINCE_PLACED = Time.time;
                    JustOnce = true;
               }

				if(satay_lvl == COOKED_TYPE.COOKED)
				{
					collider.enabled = true;
				}
				else 
				{
					collider.enabled = false;
				}



            }
            else
            {
                IsInsideGrill = false;
            }
        }
        if (plateObj != null)
        {
			if(satay_lvl == COOKED_TYPE.COOKED)
			{
	            if (plateObj.collider.bounds.Contains(gameObject.transform.position) ) 
	            { 
	                IsInsideSet = true;
	                //Debug.Log("Satay is inside plate");
	            }
	            else
	            {
	                IsInsideSet = false;
	            }
			}
            
        }

    }

    public COOKED_TYPE getCookedLvl()
    {
        return satay_lvl;
    }

}
