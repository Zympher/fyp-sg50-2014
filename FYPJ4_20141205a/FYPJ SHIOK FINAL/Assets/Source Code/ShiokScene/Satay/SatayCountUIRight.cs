﻿using UnityEngine;
using System.Collections;

public class SatayCountUIRight : MonoBehaviour {

	private SpriteRenderer sr;
	
	private GameObject SatayMini;
	private SatayGrillMiniGame satay_temp;
	
	public Sprite [] ordered_satay_UI;
	
	// Use this for initialization
	void Start () {
		
		sr = GetComponent<SpriteRenderer> ();
		
		SatayMini = GameObject.Find ("Satay(Clone)");
		satay_temp = SatayMini.GetComponent<SatayGrillMiniGame> ();

		if (satay_temp.five_or_ten)
						sr.sprite = ordered_satay_UI [0];
				else
						sr.sprite = ordered_satay_UI [1];
		
	}

	
	// Update is called once per frame
	void Update () {
	
	}
}
