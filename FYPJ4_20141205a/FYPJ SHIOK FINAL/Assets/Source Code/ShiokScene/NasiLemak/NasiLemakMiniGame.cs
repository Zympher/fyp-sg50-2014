﻿using UnityEngine;
using System.Collections;

public enum NASILEMAK_SET_TYPE
{
	CHICKEN_WING,
	FISH,
	OTAH,
}

public class NasiLemakMiniGame : MonoBehaviour 
{
	// basic objs
	public GameObject BackGroundObj;
	public GameObject PlateObj;
	public GameObject TickObj;

	// Set Indicatior
	public GameObject SetIndicatorObj;
	public GameObject[] SetIndicatorPrefabs;

	// Ingredient
	public GameObject[] IngredientPrefabs;
	public GameObject[] IngredientHintPrefabs;
	private GameObject[] IngredientObjs;
	private ArrayList IngredientHintObjs;
	
	private GameObject CurrentMovingObj;

    GameObject obj;
    Timer getTime;

	private const int NUM_OF_INGREDIENT_NEEDED = 5;
	private const int NUM_OF_INGREDIENT = 7;
	private const int NUM_OF_SET = 3;
	private NASILEMAK_SET_TYPE Type;
	private float fMiniGameFinished_Time;
	private const float MINIGAME_FINISH_DELAY = 1.0f;

	public bool order_fulfilled = false;
	// Use this for initialization
	void Start () 
	{
		Type = (NASILEMAK_SET_TYPE)Random.Range (0,NUM_OF_SET);

		// init basic objs
		PlateObj = (GameObject)Instantiate(PlateObj,
		                       PlateObj.transform.position,
		                       PlateObj.transform.rotation);
		BackGroundObj = (GameObject)Instantiate(BackGroundObj,
		                                        BackGroundObj.transform.position,
		                                        BackGroundObj.transform.rotation);
		SetIndicatorObj = (GameObject)Instantiate(SetIndicatorPrefabs[(int)Type],
		                                          SetIndicatorPrefabs[(int)Type].transform.position,
		                                          SetIndicatorPrefabs[(int)Type].transform.rotation);

		// init ingredient objs
		IngredientObjs = new GameObject[NUM_OF_INGREDIENT];
		IngredientHintObjs = new ArrayList ();
		for ( int i = 0 ; i < NUM_OF_INGREDIENT ; i++ )
		{
			IngredientObjs[i] = (GameObject)Instantiate(IngredientPrefabs[i],
			                                            IngredientPrefabs[i].transform.position,
			                                            IngredientPrefabs[i].transform.rotation);
		}

		// init needed ingredient objs
		for ( int i = 0 ; i < NUM_OF_INGREDIENT_NEEDED ; i++ )
		{
			// is it last obj ?
			int index = (i == NUM_OF_INGREDIENT_NEEDED-1 ? i+(int)Type : i);
			// create hint obj
			GameObject newHintObj = (GameObject)Instantiate(IngredientHintPrefabs[index],
			                                                IngredientHintPrefabs[index].transform.position,
			                                                IngredientHintPrefabs[index].transform.rotation);
			IngredientHintObjs.Add(newHintObj);
			// set alpha value
			newHintObj.GetComponent<SpriteRenderer>().color = new Vector4(1.0f,1.0f,1.0f,0.5f);
			// assign hint obj to ingredient obj
			IngredientObjs[index].GetComponent<NasiLemakIngredientController>().Init(newHintObj);
		}

		fMiniGameFinished_Time = -1;

        obj = GameObject.Find("Pause");
        getTime = obj.GetComponent<Timer>();
	}

	public bool IsMiniGameFinish()
	{
		if (fMiniGameFinished_Time > 0)
		{
			//order_fulfilled = true;
			return true;
			}
		return false;
	}

	public void DestroyMiniGame()
	{
        //BackGroundObj.renderer.enabled = false;
        //PlateObj.renderer.enabled = false;
        //TickObj.renderer.enabled = false;
        //SetIndicatorObj.renderer.enabled = false;
		Destroy(BackGroundObj);
		Destroy(PlateObj);
		//Destroy(TickObj);
		Destroy(SetIndicatorObj);
		foreach(GameObject obj in IngredientObjs)
		{
            //obj.renderer.enabled = false;
			Destroy(obj);
		}
		foreach(GameObject obj in IngredientHintObjs)
		{
            //obj.renderer.enabled = false;
			Destroy(obj);
		}
		Destroy(gameObject);
	}
	
	// Update is called once per frame
	void Update () 
	{

		if ( !IsMiniGameFinish() )
		{
            if (getTime.isDone)
            {
				//order_fulfilled = true;
                //DestroyMiniGame();
                //getTime.setIsDone(false);
                fMiniGameFinished_Time = Time.time;
            }
            //else
            //{
                foreach (GameObject obj in IngredientObjs)
                {
                    NasiLemakIngredientController nasiLemakIngredientController = obj.GetComponent<NasiLemakIngredientController>();
                    // is this indredient placed ?
                    if (!nasiLemakIngredientController.IsIngredientPlaced)
                    {
                        // get mouse position
                        Vector3 tempPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                        // set mouse position z value the same as the obj
                        tempPosition.z = obj.transform.position.z;
                        // select the obj
                        if (Input.GetMouseButtonDown(0) && obj.collider.bounds.Contains(tempPosition))
                        {
                            CurrentMovingObj = obj;
                        }
                        // moving the obj
                        if (Input.GetMouseButton(0) && CurrentMovingObj == obj)
                        {
                            obj.transform.position = tempPosition;
                        }
                        // release the obj
                        else if (Input.GetMouseButtonUp(0))
                        {
                            SphereCollider collider = obj.transform.GetComponent<SphereCollider>();
                            CurrentMovingObj = null;
                            // is the hint obj exist ?
                            if (nasiLemakIngredientController.IngredientHint)
                            {
                                float sqrDistance = (obj.transform.position - nasiLemakIngredientController.IngredientHint.transform.position).sqrMagnitude;
                                // are they most in the same place ?
                                if (sqrDistance < collider.radius * collider.radius)
                                {
                                    // set obj to the same place as hint obj
                                    obj.transform.position = nasiLemakIngredientController.IngredientHint.transform.position;
                                    // remove hint obj
                                    IngredientHintObjs.Remove(nasiLemakIngredientController.IngredientHint);
                                    Destroy(nasiLemakIngredientController.IngredientHint);
                                    // is all obj placed ?
                                    if (IngredientHintObjs.Count == 0)
                                    {
                                        // timer start
                                        fMiniGameFinished_Time = Time.time;
                                        // show the tick
                                        TickObj = (GameObject)Instantiate(TickObj,
                                                                          TickObj.transform.position,
                                                                          TickObj.transform.rotation);
										order_fulfilled = true;
                                    }
                                    nasiLemakIngredientController.IsIngredientPlaced = true;
                                }
                            }
                        }
                    }
                }
            //}
		}
		else
		{
			// delay time over
			if ( Time.time > fMiniGameFinished_Time+MINIGAME_FINISH_DELAY )
			{
				DestroyMiniGame();
			}
		}
	}
}
