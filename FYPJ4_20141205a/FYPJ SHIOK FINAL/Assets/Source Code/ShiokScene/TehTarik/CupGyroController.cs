﻿using UnityEngine;
using System.Collections;
//using ThinksquirrelSoftware.Fluvio;

public class CupGyroController : MonoBehaviour
{

	
		private GameObject tehtarikmain;

		public GameObject tutorial_phone_anim;
		public GameObject pouring_anim;
		private bool getGyro;

		private Vector2 startPos;
	
		
		public bool m_bDoRotation = false;

		public bool m_bPouring = false;

		public Vector3 starting_pos;

		public GameObject bubble_sprite;
		public GameObject teh_sprite;
		public GameObject other_cup;
		public GameObject cup_glow;

		public float filled_val;

		//Progressbar for filled indicators of teh
		public Texture2D filledBarEmpty;
		public Texture2D filledBarFull;
		public Vector2 pos = new Vector2 (20, 40);
		public Vector2 size = new Vector2 (20, 60);
		public float speed = 0.5f;
	
		public bool with_teh;

		public Vector3 temp_pos;

		public bool m_bSelected = false;
		private Vector3 temp_touch;

		public float max_x;
		public float min_x;

		const int CUP_HEIGHT = 332;
		const int CUP_WIDTH = 387;

		public bool m_bCreatebtn = false;

		public bool m_bTestCase = false;
	
		private Vector3 min_x_vec;
		private Vector3 max_x_vec;

		public float TIME_SINCE_POUR;
		public float elapsed_time;

		public bool JustOnce = true;
		private bool JustTimeOnce = true;

		public bool m_bBubble = false;

		public string test_val;

		public bool m_bSpill;

		//stop showing the tutorial animation when player has poured correctly once
		public bool show_tutorial_anim = true;
		public bool m_bFirst = true;

		//Textured ProgressBar
		public Rect ProgressBar;
		public Texture ProgressTex;
	
		public Rect ProgressBarInner;
		public Texture ProgressTexInner;
		public Rect ProgressInnerCoords;


		private float progressRatio = 0.001f;
		// Use this for initialization
		void Start ()
		{
		tehtarikmain = GameObject.FindWithTag ("TehTarikMiniGame");

				//check if gyroscope is available
				getGyro = tehtarikmain.GetComponent<TehTarikMiniGame> ().gyrobool;
				//record starting position
				starting_pos = gameObject.transform.position;

				tutorial_phone_anim.renderer.enabled = false;
		}
	
		// Update is called once per frame
		void Update ()
		{

		tutorial_phone_anim.GetComponent<Animator> ().speed = 1 / Time.timeScale;
		pouring_anim.GetComponent<Animator> ().speed = 1 / Time.timeScale;

				if (m_bBubble) {
						bubble_sprite.GetComponent<BubbleController> ().renderer.enabled = true;
						pouring_anim.GetComponent<PouringController> ().renderer.enabled = false;

				} else {
						bubble_sprite.GetComponent<BubbleController> ().renderer.enabled = false;
				}

				if (tehtarikmain.GetComponent<TehTarikMiniGame> ().mini_game_over == false) {

						if (with_teh) { 
								teh_sprite.GetComponent<TehSpriteController> ().renderer.enabled = true;
						} else {
								teh_sprite.GetComponent<TehSpriteController> ().renderer.enabled = false;
						}
						
						if (filled_val <= 0) {
								filled_val = 0;
						}

						if (filled_val >= 100) {
								filled_val = 100;
						}

						if (m_bSelected) {

								temp_touch = tehtarikmain.GetComponent<TehTarikMiniGame> ().touch_temp;
								temp_pos = new Vector3 (temp_touch.x,
			                        temp_touch.y,
			                        0);
								//from center to left
								min_x = (float)temp_touch.x - CUP_WIDTH / 2f;
								//from center to right
								max_x = (float)temp_touch.x + CUP_WIDTH / 2f;
								
								min_x_vec = Camera.main.ScreenToWorldPoint (new Vector3 (min_x, 0, 0));
								max_x_vec = Camera.main.ScreenToWorldPoint (new Vector3 (max_x, 0, 0));

								cup_glow.GetComponent<CupGlowController> ().renderer.enabled = true;
								m_bDoRotation = true;

								
						}
						//disable glow if not current obj
						if (!m_bSelected) {
								cup_glow.GetComponent<CupGlowController> ().renderer.enabled = false;
						}

						
						if (m_bSelected && m_bDoRotation) {
								gameObject.transform.Rotate (0, 0, Input.gyro.rotationRate.z * 1.7f);
						}

						if (JustOnce == false) {
								gameObject.transform.rotation = Quaternion.identity;
								gameObject.transform.position = starting_pos;
						}

			if (m_bFirst) {
				tutorial_phone_anim.renderer.enabled = true;
				tutorial_phone_anim.GetComponent<Animator>().Play ("Tutorial_Phone");
			} else {
				tutorial_phone_anim.renderer.enabled = false;
			}

						//if(!tehtarikmain.GetComponent<TehTarikMiniGame>().m_bSpilling) 
						//{
						if (pouring_anim.GetComponent<PouringController> ().renderer.enabled == true) {
								pouring_anim.GetComponent<PouringController> ().pour_check.GetComponent<PouringCollisionController> ().m_bCheck = true;
						} else {
								pouring_anim.GetComponent<PouringController> ().pour_check.GetComponent<PouringCollisionController> ().m_bCheck = false;
						}
						//}

						//if pouring state spilling cannot be
						if (m_bPouring) {
								//tehtarikmain.GetComponent<TehTarikMiniGame>().m_bSpilling = false;
								other_cup.GetComponent<CupGyroController> ().filled_val += tehtarikmain.GetComponent<TehTarikMiniGame> ().teh_rate_of_change;
								filled_val -= tehtarikmain.GetComponent<TehTarikMiniGame> ().teh_rate_of_change;
								tehtarikmain.GetComponent<TehTarikMiniGame> ().user_transferred_amt += tehtarikmain.GetComponent<TehTarikMiniGame> ().teh_rate_of_change;
						}

						if (m_bSpill) {
								tehtarikmain.GetComponent<TehTarikMiniGame> ().spilled_amt += tehtarikmain.GetComponent<TehTarikMiniGame> ().teh_rate_of_change;
								tehtarikmain.GetComponent<TehTarikMiniGame> ().total_spilled_amt += tehtarikmain.GetComponent<TehTarikMiniGame> ().teh_rate_of_change;
								filled_val -= tehtarikmain.GetComponent<TehTarikMiniGame> ().teh_rate_of_change;
								tehtarikmain.GetComponent<TehTarikMiniGame> ().user_transferred_amt += tehtarikmain.GetComponent<TehTarikMiniGame> ().teh_rate_of_change;

						}
						//no gyro,use accelerometer
						if (m_bDoRotation && getGyro == false) {

						}

						if (gameObject.transform.eulerAngles.z >= 200 && gameObject.transform.eulerAngles.z <= 265) {

								pouring_anim.GetComponent<PouringController> ().pour_check.GetComponent<PouringCollisionController> ().collider.enabled = true;
								//if(filled_val >= 0)
					
			

//				if(tehtarikmain.GetComponent<TehTarikMiniGame>().m_bSpilling)
//				{
//					m_bPouring = false;
//				}
//				else
//					m_bPouring = true;
								//hardcode to look nice
								//if(!tehtarikmain.GetComponent<TehTarikMiniGame>().m_bSpilling){
								pouring_anim.GetComponent<PouringController> ().transform.localPosition = new Vector3 (1f, 1.5f, 4f);
								pouring_anim.GetComponent<PouringController> ().transform.localEulerAngles = new Vector3 (0f, 0f, 144f);
								//}

								pouring_anim.GetComponent<PouringController> ().renderer.enabled = true;

								//m_bFirst = false;

						}
						

						if (gameObject.transform.eulerAngles.z >= 120 && gameObject.transform.eulerAngles.z <= 170) {

								pouring_anim.GetComponent<PouringController> ().pour_check.GetComponent<PouringCollisionController> ().collider.enabled = true;
								//if(filled_val >= 0)
								//{
								if (pouring_anim.GetComponent <PouringController> ().pour_check.GetComponent<PouringCollisionController> ().m_bLeftCollided == true ||
										pouring_anim.GetComponent <PouringController> ().pour_check.GetComponent<PouringCollisionController> ().m_bRightCollided == true) 
										m_bPouring = true;
				//}
				else {
										m_bPouring = false;
								}	

//				if(tehtarikmain.GetComponent<TehTarikMiniGame>().m_bSpilling)
//				{
//					m_bPouring = false;
//				}
//				else
//					m_bPouring = true;
								//hardcode to look nice
								//if(!tehtarikmain.GetComponent<TehTarikMiniGame>().m_bSpilling)
								pouring_anim.transform.localPosition = new Vector3 (-1.9f, 1.25f, 4f);




								pouring_anim.GetComponent<PouringController> ().transform.localEulerAngles = new Vector3 (pouring_anim.GetComponent<PouringController> ().transform.localEulerAngles.x, 180f, pouring_anim.GetComponent<PouringController> ().transform.localEulerAngles.z);
								pouring_anim.GetComponent<PouringController> ().renderer.enabled = true;

								//m_bFirst = false;
						}
							

						//Control when the sprite is displayed
						if (gameObject.transform.eulerAngles.z <= 120 && gameObject.transform.eulerAngles.z >= 0) {
								pouring_anim.GetComponent<PouringController> ().pour_check.GetComponent<PouringCollisionController> ().collider.enabled = false;
								tehtarikmain.GetComponent<TehTarikMiniGame> ().m_bSpilling = false;
								pouring_anim.GetComponent<PouringController> ().renderer.enabled = false;
								m_bPouring = false;

						}
						if (gameObject.transform.eulerAngles.z >= 265) {
								pouring_anim.GetComponent<PouringController> ().pour_check.GetComponent<PouringCollisionController> ().collider.enabled = false;
								tehtarikmain.GetComponent<TehTarikMiniGame> ().m_bSpilling = false;
								pouring_anim.GetComponent<PouringController> ().renderer.enabled = false;
								m_bPouring = false;
						}
						if (gameObject.transform.eulerAngles.z > 170 && gameObject.transform.eulerAngles.z < 190) {
								pouring_anim.GetComponent<PouringController> ().pour_check.GetComponent<PouringCollisionController> ().collider.enabled = false;
								tehtarikmain.GetComponent<TehTarikMiniGame> ().m_bSpilling = false;
								pouring_anim.GetComponent<PouringController> ().renderer.enabled = false;
								m_bPouring = false;
						}

			

					
				
				}//mini game bracket
		}

	
		void OnGUI ()
		{

				//if (m_bSelected) {

				//if (progressRatio >= 1.0f) {
				
				//progressRatio = 1.0f;
				//}
				//ProgressInnerCoords = new Rect (0, 0, 1, 20 - (19 * progressRatio));
				//GUI.BeginGroup (new Rect (temp_pos.x, (Screen.height - temp_pos.y) - 50, size.x, size.y));		
				//ProgressBarInner = new Rect (0,0, );
				//ProgressInnerCoords = new Rect();

				//uncomment for progressbar
				//GUI.DrawTexture (ProgressBar, ProgressTex);
				//GUI.DrawTexture (ProgressBarInner, ProgressTexInner);
				//GUI.DrawTextureWithTexCoords (ProgressBarInner, ProgressTexInner, ProgressInnerCoords, true);

				//GUI.EndGroup ();
				//}
//		GUI.backgroundColor.r = 1.0f;
//		GUI.backgroundColor.g = 0.62f;
//		GUI.backgroundColor.b = 0.12f;
//		GUI.backgroundColor.a = 1.0f;

				//GUI.backgroundColor = new Color (1.0f, 0.62f, 0.12f);

				if (m_bCreatebtn) {
						Rect btnRect = new Rect (temp_pos.x - 50, (Screen.height - temp_pos.y) + 50, 100, 50);
						if (GUI.Button (btnRect, "Pour Mode")) {
								m_bDoRotation = !m_bDoRotation;
						}
				}
	

				if (m_bSelected) {
						GUI.BeginGroup (new Rect (temp_pos.x, (Screen.height - temp_pos.y) - 50, size.x, size.y));
						GUI.Box (new Rect (0, 0, size.x, size.y), filledBarEmpty);
		
						// draw the filled-in part:
						GUI.BeginGroup (new Rect (0, (size.y - (size.y * filled_val / 100)), size.x, size.y * filled_val / 100));
						GUI.Box (new Rect (0, -size.y + (size.y * filled_val / 100), size.x, size.y), filledBarFull);
						GUI.EndGroup ();
						GUI.EndGroup ();
				}

	
		}







}

