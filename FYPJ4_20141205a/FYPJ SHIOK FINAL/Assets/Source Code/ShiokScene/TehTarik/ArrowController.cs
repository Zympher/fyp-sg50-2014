﻿using UnityEngine;
using System.Collections;

public class ArrowController : MonoBehaviour {

	public bool m_bArrowActive = false;

	private SpriteRenderer spriteRenderer;
	public float speed = 1.0f;
	public Color A = Color.black;
	public Color B = Color.red;

	// Use this for initialization
	void Start () {
		renderer.enabled = false;
		spriteRenderer = GetComponent<SpriteRenderer> ();
	}
	
	// Update is called once per frame
	void Update () {

		if (m_bArrowActive) {
						renderer.enabled = true;
			float lerp = Mathf.PingPong (Time.time, 1.0f) * speed;
			renderer.material.color = new Color(1.0f,1.0f,1.0f,lerp);
						//spriteRenderer.material.color = Color.Lerp (A, B, Mathf.PingPong (Time.time * speed, 1.0f));
				} 
		else {

			spriteRenderer.renderer.enabled = false;
				}
	}
}
