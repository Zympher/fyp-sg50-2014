﻿using UnityEngine;
using System.Collections;

public class DayNightTimer : MonoBehaviour
{
    GameObject rotateTex;
    GameObject timerObj;

    private float fullDay = 360;      //one revolution = 360 degrees

    private float angleZ = 0;

    private float everySecond;

    private float timeGiven;        //180 seconds

    public float angleMoved;

    private int lastCall;

    private int secondsCounter;

    // Use this for initialization
    void Start()
    {
        rotateTex = GameObject.Find("TimerRotation");

        timerObj = GameObject.Find("Pause");

        timeGiven = timerObj.GetComponent<Timer>().getTimer();

        everySecond = fullDay / timeGiven;

        lastCall = (int)Time.time;

        angleZ = everySecond;

        angleMoved = 0;

        secondsCounter = 0;
    }

    void RotateClock()
    {
        //Debug.Log(lastCall);

        if ((int)Time.time - lastCall == 1)
        {
            rotateTex.transform.Rotate(new Vector3(0, 0, 1.0f), -angleZ);
            lastCall = (int)Time.time;
            secondsCounter++;
        }
    }

    public float getAngle()
    {
        return angleMoved = secondsCounter * angleZ;
    }

    // Update is called once per frame
    void Update()
    {
        RotateClock();
        //angleMoved = rotateTex.transform.rotation.z;  
        //Debug.Log("AngleMoved: " + angleMoved);
    }
}
