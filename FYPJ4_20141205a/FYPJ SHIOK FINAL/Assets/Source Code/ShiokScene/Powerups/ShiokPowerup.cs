﻿using UnityEngine;
using System.Collections;

public class ShiokPowerup : MonoBehaviour 
{
    public ShiokAI shiokAI;

    // Use this for initialization
    void Start()
    {

    }

	// Update is called once per frame
	void Update () 
    {
	    
	}

    //void OnGUI()
    //{
    //    GameObject gameObjectToUpgrade = GameObject.Find("PointsCarrying)");
    //    PointsCarry moneyUpgrade = gameObjectToUpgrade.GetComponent<PointsCarry> ();

    //    if ( shiokAI.IsInMiniGameMode == false)
    //    {
    //        GUI.depth = -10;
    //        //
    //    }
    //}


    public void PurchaseShiokUpgrade()
    {
        GameObject gameObjectPurchase = GameObject.Find ("PointsCarrying");
        PointsCarry moneyDeduct = gameObjectPurchase.GetComponent<PointsCarry> ();

        moneyDeduct.money -= 50;
    }

    public void PurchaseNewStallUpgrade()
    {
        GameObject gameObjectPurchase = GameObject.Find("PointsCarrying");
        PointsCarry moneyDeduct = gameObjectPurchase.GetComponent<PointsCarry>();

        moneyDeduct.money -= 100;
    }
    public void PurchaseBetterQualityUpgrade()
    {
        //better quality upgrade temp
        GameObject gameObjectPurchase = GameObject.Find("PointsCarrying");
        PointsCarry moneyDeduct = gameObjectPurchase.GetComponent<PointsCarry>();

        moneyDeduct.money -= 100;
    }

    public void PurchasePatienceUpgrade()
    {
        //patience upgrade temp
        GameObject gameObjectPurchase = GameObject.Find("PointsCarrying");
        PointsCarry moneyDeduct = gameObjectPurchase.GetComponent<PointsCarry>();

        moneyDeduct.money -= 100;
    }
}
