﻿using UnityEngine;
using System.Collections;

public class pauseinMG : MonoBehaviour
{
    GameObject obj;
    GameObject objPause;
    Pause pause;
    ShiokAI ai;

    public bool TSvalue = true;

	// Use this for initialization
    void Start()
    {
        obj = GameObject.Find("Main Camera");
        ai = obj.GetComponent<ShiokAI>();

        objPause = GameObject.Find("Pause");
        pause = objPause.GetComponent<Pause>();
    }
	
	// Update is called once per frame
	void Update () 
    {
        if (ai.IsInMiniGameMode)
        {
            if (TSvalue)
                Time.timeScale = 0.0001f;
            else
                Time.timeScale = 0.0f;
        }
        if(pause.isPause == 1)
            Time.timeScale = 0;
        if (pause.isPause == 0 && !ai.IsInMiniGameMode)
            Time.timeScale = 1;
	}

    public void setTimeScale(bool tempTS)
    {
       TSvalue = tempTS;
    }
}
