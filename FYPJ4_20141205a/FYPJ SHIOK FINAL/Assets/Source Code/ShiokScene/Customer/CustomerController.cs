﻿using UnityEngine;
using System.Collections;

public enum CUSTOMER_CONTROL_STATE
{
	AI_CONTROL,
	PLAYER_SELECT,
};

public enum CUSTOMER_TYPE
{
	CHINESE_GIRL,
	MALAY_GIRL,
	INDIAN_GUY,
	BOY,
    STUDENT, 
	CHATTY_AUNTIE,
    DURAIN_UNCLE
};

public enum CUSTOMER_ANIMATION_TYPE
{
	IDLE,
	WALK_LEFT,
	WALK_RIGHT,
	SIT_LEFT,
	SIT_RIGHT,
};

public class CustomerController : MonoBehaviour
{
	//Sprite
    const float SPRITE_SIZE = 1.2f; //0.6
    const float SPRITE_SIZE_STAND = 0.28f;
	private float MapHeight;
	const float SPRITE_SCALE_VALUE = 0.3f;
	public Sprite[] LegSprites;
	public GameObject LegPrefab;
	private GameObject LegObj;
	private const float OVERHEAD_Y_OFFSET = 1.0f;
	private const float SIDEHEAD_X_OFFSET = 1.0f;

	//Moving
	public ArrayList path = null;
	private float SPEED = 6.0f;
	private Vector3 direction;
	private Vector3 nextPoint;
	private Vector3 targetPoint;
	private Vector3 position_offset;
	public bool IsMoving = false;

	//AIF
	public CUSTOMER_AI_STATE currentState;
	public CUSTOMER_CONTROL_STATE controlState;

	//Stall
	public GameObject TargetStall;
	public int CurrentQueueNumber;

	//Order
	public GameObject OrderPrefab;
	public GameObject OrderObj;

	//Enter
	private float fWaitForEnterPermission_Time;
	private const float SPECIAL_CUSTOMER_ENTER_DELAY = 5.0f;
	private const float NORMAL_CUSTOMER_ENTER_DELAY = 1.0f;
	public bool IsEnterFromLeftSide;

	//Walk around
	private const float LOOKING_FOR_SEAT_DURATION_MIN = 1.0f;
	private const float LOOKING_FOR_SEAT_DURATION_MAX = 1.5f;
	private const float WALK_AROUND_PATIENCE_MAX = 30.0f;
	private float fLookingForSeat_Time = 0;
	private float LookingForSeatDuration = 0;
	public bool IsStartLookingForSeat = false;
	public GameObject TapIndicatorPrefab;
	private GameObject TapIndicatorObj;

	//Eating
	private const float EATING_DURATION_MIN = 3.0f;
	private const float EATING_DURATION_MAX = 5.0f;
	private float fEating_Time = 0;
	private float EatingDuration = 0;

	//Table
	public GameObject TableTaken;
	public GameObject TargetTable;
	public int seatIndex;
	private float BenchZValue;// the value of bench Z axis

	//Group
	public int groupID;
	public int groupIndex;
	public int numOfCustomerInTheGroup;
	public GameObject GroupMarkerPrefab;
	private GameObject GroupMarkerObj;

	//Shiok
	public GameObject ShiokBubblePrefab;
	private float fSpawnShiokBubble_Time = 0;
	private const float SHIOK_BUBBLE_DELAY_MIN = 1.0f;
	private const float SHIOK_BUBBLE_DELAY_MAX = 3.0f;
	private float SpawnShiokBubbleDelay = 0;

	//Animation
	public CUSTOMER_TYPE type;
	private const int NUM_OF_CUSTOMER_TYPE = 7; //changed from 4    //(idk when) changed to 6 
	private const int NUM_OF_ANIMATION_PER_CUSTOMER = 5;
	public RuntimeAnimatorController[] AniController;

	//Patience
	public GameObject PatienceIndicatorPrefab;
	private GameObject PatienceIndicatorObj;

	//Added 
	//private float CHATTING_DURATION_MIN = 5.0f;
	//private float CHATTING_DURATION_MAX = 10.0f;

    GameObject mapCheck;
    SelectedMap getMap;

    private const int NUM_OF_NORM_CUSTOMER_TYPE = 6;    //changed from 3 (hazlan)

	public int GetNumOfCustomerType()
	{
		return NUM_OF_NORM_CUSTOMER_TYPE;
	}

	public bool IsAllowEnterHawkerCenter()
	{
		float tempDelay;
		if (type == CUSTOMER_TYPE.DURAIN_UNCLE )
		 //|| type == CUSTOMER_TYPE.CHATTY_AUNTIE ) 
		{
			tempDelay = SPECIAL_CUSTOMER_ENTER_DELAY;
		} 
		else
		{
			tempDelay = NORMAL_CUSTOMER_ENTER_DELAY;
		}
		if (Time.time > fWaitForEnterPermission_Time + tempDelay)
			return true;
		return false;
	}

	public void SetAnimation(CUSTOMER_ANIMATION_TYPE AnimationType)
	{
		Animator animator = gameObject.GetComponent<Animator>();
		if ( animator.runtimeAnimatorController != AniController[(int)type*NUM_OF_ANIMATION_PER_CUSTOMER+(int)AnimationType] )
		{
			animator.runtimeAnimatorController = AniController[(int)type*NUM_OF_ANIMATION_PER_CUSTOMER+(int)AnimationType];
		}
	}

	public bool IsFinishEating()
	{

		if ( Time.time > EatingDuration+fEating_Time )
			return true;
		return false;
	}

	public float GetDurainUncleDelay()
	{
		return SPECIAL_CUSTOMER_ENTER_DELAY;
	}

	public bool IsSitWithDurainUncle()
	{
		if ( TableTaken.GetComponent<TableController> ().IsTakenByDurainUncle )
			return true;
		return false;
	}

	public void Eat()
	{
		TableTaken.GetComponent<TableController> ().NumOfCustomerAreEating++;
		fEating_Time = Time.time;
		EatingDuration = Random.Range (EATING_DURATION_MIN, EATING_DURATION_MAX);
	}
	
	public void LookingForSeat()
	{
		LookingForSeatDuration = Random.Range (LOOKING_FOR_SEAT_DURATION_MIN, LOOKING_FOR_SEAT_DURATION_MAX);
		fLookingForSeat_Time = Time.time;
		IsStartLookingForSeat = true;
		SetAnimation (CUSTOMER_ANIMATION_TYPE.IDLE);
	}

	public bool IsLookingForSeat()
	{
		if ( Time.time > fLookingForSeat_Time + LookingForSeatDuration)
			return false;
		return true;
	}

	public void setPosition(Vector3 Position)
	{
		gameObject.transform.position = Position;
	}

	public Vector3 GetOverHeadPosition()
	{
		return (gameObject.transform.position + new Vector3 (0,gameObject.renderer.bounds.size.y / 2+OVERHEAD_Y_OFFSET,-10));
	}

	public Vector3 GetSideHeadPosition()
	{
		return (GetOverHeadPosition()+new Vector3(SIDEHEAD_X_OFFSET,0,0));
	}

	public Vector3 GetPositionOffSet()
	{
		//from feet to body center
		return position_offset;
	}
	
	public Vector3 GetFeetPosition()
	{
		return (gameObject.transform.position + GetPositionOffSet() );
	}

	public Vector3 GetPosition()
	{
		return gameObject.transform.position;
	}

	public Vector3 GetDirection2D(Vector3 Origin, Vector3 Target)
	{
		Vector3 temp_origin = new Vector3 (Origin.x,Origin.y,0);
		Vector3 temp_target = new Vector3 (Target.x,Target.y,0);
		Vector3 Direction2D = temp_target - temp_origin;
		Direction2D.Normalize ();
		return Direction2D;
	}

	private void UpdateAnimation()
	{	
		if ( controlState == CUSTOMER_CONTROL_STATE.PLAYER_SELECT )
		{
			SetAnimation (CUSTOMER_ANIMATION_TYPE.IDLE);
		}
		else
		{
			if ( IsMoving )
			{
				if ( direction.x < 0 )
				{
					SetAnimation (CUSTOMER_ANIMATION_TYPE.WALK_LEFT);
				}
				else
				{
					SetAnimation (CUSTOMER_ANIMATION_TYPE.WALK_RIGHT);
				}
			}
		}
	}

	public void Init(CUSTOMER_TYPE Type, float mapHeight, float Bench_Z_Value)
	{
		fWaitForEnterPermission_Time = Time.time;
		seatIndex = -1;
		currentState = CUSTOMER_AI_STATE.WAIT_FOR_ENTER_PREMISSION;
		setPosition (new Vector3(GetPosition().x,GetPosition().y,GetPosition().y));
		type = Type;
		gameObject.name = name + gameObject.GetInstanceID ().ToString();
		BenchZValue = Bench_Z_Value;
		MapHeight = mapHeight;
		IsEnterFromLeftSide = (GetPosition ().x < 0 ? true : false);
		SetAnimation (CUSTOMER_ANIMATION_TYPE.IDLE);
		position_offset = Vector3.zero;
	}

	public void GroupInit(int ID, int Index, int NumOfCustomer)
	{
		groupID = ID;
		groupIndex = Index;
		numOfCustomerInTheGroup = NumOfCustomer;
	}

	// Use this for initialization
	void Start ()
	{
        mapCheck = GameObject.Find("selectedMap");
        getMap = mapCheck.GetComponent<SelectedMap>();
	}

    void UpdateSpriteSize()
    {
        //if (currentState == CUSTOMER_AI_STATE.EAT || currentState == CUSTOMER_AI_STATE.WAIT_FOR_GROUP_MEMBER || currentState == CUSTOMER_AI_STATE.CHOPE)
        //{
            float tempScaleValue = SPRITE_SIZE * (1 - SPRITE_SCALE_VALUE * 2 * GetPosition().y / MapHeight);
            Vector3 tempScale = new Vector3(tempScaleValue, tempScaleValue, 1);
            gameObject.transform.localScale = tempScale;
        //}
        //else
        //{
        //    float tempScaleValue = SPRITE_SIZE_STAND * (1 - SPRITE_SCALE_VALUE * 2 * GetPosition().y / MapHeight);
        //    Vector3 tempScale = new Vector3(tempScaleValue, tempScaleValue, 1);
        //    gameObject.transform.localScale = tempScale;
        //}
    }

	// Update is called once per frame
	void Update () 
	{
		// position offset init
		if ( position_offset == Vector3.zero )
		{
			position_offset = new Vector3 (0, -gameObject.renderer.bounds.size.y / 2, 0);
		}

		// Move to next path node
		if ( path != null )
		{
			if ( !IsMoving )
			{
				if ( path.Count > 0 )
				{
					MoveTo(((Grid)path[0]).posX,((Grid)path[0]).posY);
					path.RemoveAt(0);
				}
				else
				{
					MoveTo(targetPoint.x,targetPoint.y);
					path = null;
				}
			}
		}

		// moving customer
		if (IsMoving)
		{
			direction = GetDirection2D (GetFeetPosition(),nextPoint);
			if ( controlState == CUSTOMER_CONTROL_STATE.AI_CONTROL )
			{
				if ( IsReachThePoint(nextPoint) )
				{
					IsMoving = false;
				}
				else
				{
					setPosition(GetPosition()+direction*Time.deltaTime*SPEED);
				}
			}
		}


		//setPosition(GetPosition()+direction*Time.deltaTime*SPEED);
		// set z value equal to y value
		setPosition (new Vector3(GetPosition().x,GetPosition().y,GetPosition().y));
	}

	void LateUpdate()
	{
		UpdateAnimation ();
		UpdateSpriteSize ();
	}

	public void MoveTo(float X, float Y)
	{
		IsMoving = true;

		if ( path != null )
			nextPoint = new Vector3 (X,Y,GetFeetPosition().z);
		else
		{
			nextPoint = new Vector3 (X,Y,GetFeetPosition().z);
			targetPoint = new Vector3 (X,Y,GetFeetPosition().z);
		}
		direction = GetDirection2D (GetFeetPosition(),nextPoint);
	}

	public void StartToFollowThePath(ArrayList Path)
	{
		if ( Path != null )
		{
			path = Path;
			targetPoint = new Vector3 (((Grid)path[0]).posX,((Grid)path[0]).posY,GetFeetPosition().z);
			path.Reverse ();
			path.RemoveAt (0);
		}
		else
		{
			targetPoint = GetFeetPosition();
		}
	}

	public void StartToFollowThePathAndMoveTo(ArrayList Path,float X, float Y)
	{
		if ( Path != null )
		{
			path = Path;
			path.Reverse ();
			path.RemoveAt (0);
			targetPoint = new Vector3 (X,Y,GetFeetPosition().z);
		}
		else
		{
			targetPoint = GetFeetPosition();
		}
	}

	private float GetDistanceSq(float x1, float y1, float x2, float y2)
	{
		return ((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
	}

	public bool IsReachTargetPoint()
	{
		if ( GetDistanceSq (GetFeetPosition().x,
		                    GetFeetPosition().y,
		                    targetPoint.x,
		                    targetPoint.y) <= SPEED*Time.deltaTime*SPEED*Time.deltaTime )
			return true;
		return false;
	}

	public bool IsReachThePoint(Vector3 Point)
	{
		if ( GetDistanceSq (GetFeetPosition().x,
		                    GetFeetPosition().y,
		                    Point.x,
		                    Point.y) <= SPEED*Time.deltaTime*SPEED*Time.deltaTime )
			return true;
		return false;
	}

	public void CreateAndShowLeg()
	{
		Vector3 LegPosition;
		int nSide;
		float LegPositionZ;
        int tempStore;

		// create leg obj
		LegObj = (GameObject)Instantiate (LegPrefab,
		                                  GetFeetPosition(),
		                                  gameObject.transform.rotation);
		TableController tableController = TableTaken.GetComponent<TableController> ();
		// is the customer sit on left side
        if (seatIndex < tableController.MAX_SEAT / 2)
        {


            tempStore = (int)type * 2;

            if (tempStore == 0 || tempStore == 6)
                LegObj.GetComponent<SpriteRenderer>().sprite = LegSprites[tempStore];
            else
                LegObj.GetComponent<SpriteRenderer>().sprite = LegSprites[0];

            LegPositionZ = BenchZValue - 1.0f;
            // is the customer sit on table 1 or table 2
            if (TableTaken.name.Contains("1") || TableTaken.name.Contains("2"))
                LegPositionZ = BenchZValue - 1.0f;
            else
                LegPositionZ = BenchZValue;
            //LegPositionZ = BenchZValue+1.0f;
            nSide = 1;

            if (TableTaken.name.Contains("1") || TableTaken.name.Contains("4"))
            {
                // set leg position
                LegPosition = new Vector3(GetFeetPosition().x + nSide * LegObj.renderer.bounds.size.y / 6,
                                          GetFeetPosition().y + LegObj.renderer.bounds.size.y / 6,
                                          LegPositionZ);
                LegObj.transform.position = LegPosition;
            }
            else
            {

                LegPosition = new Vector3(GetFeetPosition().x + nSide * LegObj.renderer.bounds.size.y / 6,
                          GetFeetPosition().y + LegObj.renderer.bounds.size.y / 4,
                          LegPositionZ);
                LegObj.transform.position = LegPosition;
            }
        }
        else
        {
            tempStore = (int)type * 2 + 1;

            if (tempStore == 1 || tempStore == 7)
                LegObj.GetComponent<SpriteRenderer>().sprite = LegSprites[tempStore];
            else
                LegObj.GetComponent<SpriteRenderer>().sprite = LegSprites[1];

            //LegObj.GetComponent<SpriteRenderer>().sprite = LegSprites[(int)type * 2 + 1];
            if (TableTaken.name.Contains("1") || TableTaken.name.Contains("2"))
                LegPositionZ = BenchZValue;
            //LegPositionZ = BenchZValue+1.0f;
            else
                LegPositionZ = BenchZValue - 1.0f;
            nSide = -1;

            if (TableTaken.name.Contains("1") || TableTaken.name.Contains("4"))
            {
                // set leg position
                LegPosition = new Vector3(GetFeetPosition().x + nSide * LegObj.renderer.bounds.size.y / 4,
                                          GetFeetPosition().y + LegObj.renderer.bounds.size.y / 6,
                                          LegPositionZ);
                LegObj.transform.position = LegPosition;
            }
            else
            {
                LegPosition = new Vector3(GetFeetPosition().x + nSide * LegObj.renderer.bounds.size.y / 4,
                          GetFeetPosition().y + LegObj.renderer.bounds.size.y / 3.5f,
                          LegPositionZ);
                LegObj.transform.position = LegPosition;
            }
        }
	}

	public void CreateAndShowGroupMark()
	{
		GroupMarkerObj = (GameObject)Instantiate(GroupMarkerPrefab,
		                           GetPosition(),
		                           gameObject.transform.rotation);
		GroupMarkerObj.GetComponent<GroupMarkerController> ().Set (gameObject,groupID);
	}

	public void StopMoving()
	{
		path = null;
		IsMoving = false;
		targetPoint = GetFeetPosition ();
	}

	public bool IsSitDown()
	{
        if (seatIndex == -1)
            return false;        
		return true;
	}

	public void ChopeTableForSingleCustomer()
	{
		TableController tableController = TableTaken.GetComponent<TableController>();
		if ( tableController.GetNumOfSeatLeft() == tableController.MAX_SEAT )
			tableController.CreateAndShowChopeObj ();
	}

    public void TrayTableForSingleCustomer()
    {
        TableController tableController = TableTaken.GetComponent<TableController>();
        if (tableController.GetNumOfSeatLeft() == tableController.MAX_SEAT)
            tableController.CreateAndShowTrayObj();
    }

	public void RemoveChopeObj()
	{
		TableController tableController = TableTaken.GetComponent<TableController>();
		tableController.DestroyChopeObj ();
	}

    public void RemoveTrayObj()
    {
        TableController tableController = TableTaken.GetComponent<TableController>();
        tableController.DestroyTrayObj();
    }

	public void ChopeSeat()
	{
		TableController tableController = TableTaken.GetComponent<TableController>();
		int SeatIndex = tableController.GetAvailableSeatIndex ();
		if ( SeatIndex >= 0 )
		{
			seatIndex = SeatIndex;
			tableController.ChopeSeat(SeatIndex);
		}
	}

    public void TraySeat()
    {
        TableController tableController = TableTaken.GetComponent<TableController>();
        int SeatIndex = tableController.GetAvailableSeatIndex();
        if (seatIndex >= 0)
        {
            seatIndex = SeatIndex;
            tableController.TraySeat(SeatIndex);
        }
    }

	public void SitDown()
	{
		TableController tableController = TableTaken.GetComponent<TableController>();
		if ( seatIndex != -1 )
		{
            if (getMap.getSelectedMap())
            {
                tableController.TakeSeat(groupID, seatIndex);
                setPosition(tableController.GetSeatPosition(seatIndex));
                //CreateAndShowLeg();
                if (seatIndex < tableController.MAX_SEAT / 2)
                    SetAnimation(CUSTOMER_ANIMATION_TYPE.SIT_RIGHT);
                else
                    SetAnimation(CUSTOMER_ANIMATION_TYPE.SIT_LEFT);
                if (type == CUSTOMER_TYPE.DURAIN_UNCLE)
                    tableController.IsTakenByDurainUncle = true;
            }
            else
            {
                tableController.TakeSeat(groupID, seatIndex);
                setPosition(tableController.GetSeatPosition(seatIndex));
                CreateAndShowLeg();
                if (seatIndex < tableController.MAX_SEAT / 2)
                    SetAnimation(CUSTOMER_ANIMATION_TYPE.SIT_RIGHT);
                else
                    SetAnimation(CUSTOMER_ANIMATION_TYPE.SIT_LEFT);
                if (type == CUSTOMER_TYPE.DURAIN_UNCLE)
                    tableController.IsTakenByDurainUncle = true;
            }
		}
		else
		{
			int AvailableSeatIndex = tableController.GetAvailableSeatIndex ();
            if (AvailableSeatIndex >= 0)
            {
                if (getMap.getSelectedMap())
                {
                    seatIndex = AvailableSeatIndex;
                    tableController.TakeSeat(groupID, seatIndex);
                    setPosition(tableController.GetSeatPosition(seatIndex));
                    //CreateAndShowLeg();
                    if (seatIndex < tableController.MAX_SEAT / 2)
                        SetAnimation(CUSTOMER_ANIMATION_TYPE.SIT_RIGHT);
                    else
                        SetAnimation(CUSTOMER_ANIMATION_TYPE.SIT_LEFT);
                    if (type == CUSTOMER_TYPE.DURAIN_UNCLE)
                        tableController.IsTakenByDurainUncle = true;
                }
                else
                {
                    seatIndex = AvailableSeatIndex;
                    tableController.TakeSeat(groupID, seatIndex);
                    setPosition(tableController.GetSeatPosition(seatIndex));
                    CreateAndShowLeg();
                    if (seatIndex < tableController.MAX_SEAT / 2)
                        SetAnimation(CUSTOMER_ANIMATION_TYPE.SIT_RIGHT);
                    else
                        SetAnimation(CUSTOMER_ANIMATION_TYPE.SIT_LEFT);
                    if (type == CUSTOMER_TYPE.DURAIN_UNCLE)
                        tableController.IsTakenByDurainUncle = true;
                }
            }
		}
	}

	public void StandUp()
	{
		if ( IsSitDown() )
		{
			DestroyLeg();
			TableController tableController = TableTaken.GetComponent<TableController>();
			tableController.LeaveSeat (groupID,seatIndex);
			seatIndex = -1;
			setPosition(GetTableStandUpPoint());
			if ( type == CUSTOMER_TYPE.DURAIN_UNCLE )
				tableController.IsTakenByDurainUncle = false;
		}
	}

	public void CreateAndShowOrder()
	{
		OrderObj = (GameObject)Instantiate(OrderPrefab,
		            GetOverHeadPosition(),
		            gameObject.transform.rotation);
		OrderObj.GetComponent<OrderController>().Set(gameObject);
	}

	public Vector3 GetTableSitDownPoint()
	{
		return TableTaken.GetComponent<TableController>().GetSitDownPoint();
	}

	public Vector3 GetTableStandUpPoint()
	{
		return TableTaken.GetComponent<TableController>().GetStandUpPoint();
	}

	public bool CheckIsFoodFinished()
	{
		if ( TargetStall.GetComponent<StallController> ().IsFoodFinished() )
			return true;
		return false;

	}

	public bool CheckIsShiokBubbleSpawnDelayEnded()
	{
		if ( Time.time > SpawnShiokBubbleDelay + fSpawnShiokBubble_Time )
			return true;
		return false;
	}

	public void CreateAndShowShiokBubble()
	{
		fSpawnShiokBubble_Time = Time.time;
		SpawnShiokBubbleDelay = Random.Range (SHIOK_BUBBLE_DELAY_MIN, SHIOK_BUBBLE_DELAY_MAX);

		Instantiate(ShiokBubblePrefab,
		            GetPosition()+ new Vector3(0,0,-1.0f),
		            gameObject.transform.rotation);
	}

	public void CreateAndShowPatienceIndicator()
	{
		PatienceIndicatorObj = (GameObject)Instantiate(PatienceIndicatorPrefab,
		                                               GetSideHeadPosition(),
		                                               gameObject.transform.rotation);
		PatienceIndicatorObj.GetComponent<PatienceIndicatorController> ().Init (gameObject,WALK_AROUND_PATIENCE_MAX);
	}

	public void CreateAndShowTapIndicator()
	{
		TapIndicatorObj = (GameObject)Instantiate(TapIndicatorPrefab,
		                                          GetOverHeadPosition() + new Vector3(0,0,-5.0f),
		                                          gameObject.transform.rotation);
	}

	public void SendOrder()
	{
		TargetStall.GetComponent<StallController> ().StartCookingFood ();
	}

	public void DestroyPatienceIndicator()
	{
		if ( PatienceIndicatorObj != null )
			Destroy (PatienceIndicatorObj);
		PatienceIndicatorObj = null;
	}

	public void DestoryTapIndicator()
	{
		if ( TapIndicatorObj != null )
			Destroy (TapIndicatorObj);
		TapIndicatorObj = null;
	}

	public void DestroyOrder()
	{
		if ( OrderObj != null )
			Destroy (OrderObj);

		OrderObj = null;
	}

	public void DestroyGroupMarker()
	{
		if ( GroupMarkerObj != null )
			Destroy (GroupMarkerObj);
		GroupMarkerObj = null;
	}

	public void DestroyLeg()
	{
		if ( LegObj != null )
			Destroy (LegObj);
		LegObj = null;
	}

	public void DestroySelf()
	{
		DestroyPatienceIndicator ();
		DestroyOrder ();
		DestroyLeg ();
		DestroyGroupMarker ();
	
		if ( gameObject != null )
			Destroy (gameObject);
	}
	
}
