﻿using UnityEngine;
using System.Collections;

public class RojakCircularController : MonoBehaviour 
{
    Vector3 StartPoint;
    private const int DISTANCE = 2;
    private int ID;
    public bool IsMix = false;
    
    private float mouseDist;
    private Vector3 mouseLastPos;
    private Vector3 mouseStartPos;
    private bool mouseTrack = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (Input.GetMouseButtonDown(0))
        {
            //set start point when tap/mouse button down inside this dotted line sprite
            //StartPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            //StartPoint.z = gameObject.transform.position.z;
            //if (!gameObject.collider.bounds.Contains(StartPoint))
            //{
            //    StartPoint = Vector3.zero;
            //}

            mouseTrack = true;
            mouseStartPos = Input.mousePosition;
        }
        //set end point when mouse release 
        if (Input.GetMouseButton(0))
        {
            mouseTrack = false;
            //Debug.Log("Mouse moved " + mouseDist + " while button was down.");
            mouseDist = 0;

            Vector3 mouseDelta = Input.mousePosition - mouseStartPos;

            if (mouseDelta.sqrMagnitude < 0.1f)
            {
                return;
            }

            float angle = Mathf.Atan2(mouseDelta.y, mouseDelta.x) * Mathf.Rad2Deg;
            if (angle < 0)
            {
                angle += 360;
            }
            Debug.Log("angle is: " + angle);
            if (angle > 0)
            {
                IsMix = true;
            }

            //detects if mouse move went into circular motion
            //Vector3 EndPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            //EndPoint.z = gameObject.transform.position.z;

            //if ((EndPoint - StartPoint).sqrMagnitude > DISTANCE * DISTANCE)
            //{
            //    IsMix = true;
            //}   
        }

        if (mouseTrack)
        {
            Vector3 newPos = Input.mousePosition;
            mouseDist += Mathf.Abs(newPos.x - mouseLastPos.x);
            mouseLastPos = newPos;
        }
	}

    public int GetID()
    {
        return ID;
    }

    public bool GetIsMix()
    {
        return IsMix;
    }

    public void DestroySelf()
    {
        Destroy (gameObject);
    }
}
