﻿using UnityEngine;
using System.Collections;

public class RojakIngredientController : MonoBehaviour 
{
    private ROJAK_INGREDIENT_TYPE Type;
    public GameObject GlowPrefab;
    private GameObject GlowObj;

    public void Init(ROJAK_INGREDIENT_TYPE Ingredient_Type)
    {
        Type = Ingredient_Type;
    }

    public ROJAK_INGREDIENT_TYPE GetIngredientType()
    {
        return Type;
    }

    public void CreateGlow()
    {
        Vector3 tempPosition = new Vector3( gameObject.transform.position.x,
                                            gameObject.transform.position.y,
                                            GlowPrefab.transform.position.z);
        if (!GlowObj)
        {
            GlowObj = (GameObject)Instantiate(  GlowPrefab,
                                                tempPosition,
                                                gameObject.transform.rotation);
        }
    }

    public void DestroySelf()
    {
        if (GlowObj)
            Destroy(GlowObj);
        Destroy(gameObject);
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
