﻿using UnityEngine;
using System.Collections;

public class ChickenPieceController : MonoBehaviour 
{
	public bool IsLeftSideCut;
	public bool IsRightSideCut;
	public bool IsPlaced;
	public bool IsInsideRiceSet;

    GameObject riceSet;

	public void Init(bool isLeftSideCut, bool isRightSideCut)
	{
		IsLeftSideCut = isLeftSideCut;
		IsRightSideCut = isRightSideCut;
		IsInsideRiceSet = false;
		IsPlaced = false; 
	}

	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () 
	{
        checkChicken();
	}

	public bool IsCut()
	{
		if ( IsLeftSideCut && IsRightSideCut )
			return true;
		return false;
	}

    void checkChicken()
    {
        Vector3 chickenPiecePos;
        Vector3 ricePos;
        float distance;

        riceSet = GameObject.Find("RiceSet(Clone)");

        chickenPiecePos = gameObject.transform.position;
        ricePos = riceSet.transform.position;

        //chicken (x1,y1,z1)    //rice (x2,y2,z2)
        distance = Vector2.Distance(ricePos, chickenPiecePos);

        Debug.Log(distance);

        if (distance <= 3)
        {
            IsInsideRiceSet = true;
        }
        else
        {
            IsInsideRiceSet = false;
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.name.Contains("RiceSet"))
        {
            //Debug.Log("Its in4");
            IsInsideRiceSet = true;
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.name.Contains("RiceSet"))
        {
            IsInsideRiceSet = false;
        }
    }
	
}
