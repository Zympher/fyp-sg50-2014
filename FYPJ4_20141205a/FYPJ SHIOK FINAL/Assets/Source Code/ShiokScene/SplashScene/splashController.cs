﻿using UnityEngine;
using System.Collections;

public class splashController : MonoBehaviour
{
    public Texture2D splash1, splash2;

    void Start()
    {
        GetComponent<Animator>().enabled = false;
    }

    void OnGUI()
    {
        if (Time.time < 3)
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), splash1, ScaleMode.StretchToFill);
        if (Time.time > 2 && Time.time < 6)
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), splash2, ScaleMode.StretchToFill);
    }

    void Update()
    {
        if (Time.time > 6)
        {
            GetComponent<Animator>().enabled = true;

            if(Input.GetMouseButtonDown(0))
                FadeTransition.LoadLevel("StartMenuScene", 0.5f, 0.5f, Color.black);
        }
    }

    void loadNextLevel()
    {
        FadeTransition.LoadLevel("StartMenuScene",0.5f,0.5f,Color.black);
        destroySplash();
    }

    void destroySplash()
    {
        Destroy(gameObject);
    }
}
