﻿using UnityEngine;
using System.Collections;

public class TutorialController : MonoBehaviour
{
    public Vector2 startTouch = Vector2.zero;

    public Texture[] tutTex; // 4 pages

    public int index;

    public Texture2D SkipButton;

    Rect SkipPos = new Rect((Screen.width * 1600) / 1920, (Screen.height * 190) / 1080, (Screen.width * 300) / 1920, (Screen.height * 160) / 1080);

    GameObject noBtn = GameObject.Find("NoPrevButton");
    GameObject prvBtn = GameObject.Find("PreviousButton");
    GameObject nxtBtn = GameObject.Find("NextButton");
    GameObject skipBtn = GameObject.Find("SkipButton");
    GameObject endBtn = GameObject.Find("EndButton");

    // Use this for initialization
    void Start()
    {
        index = 0; //page number
    }

    // Update is called once per frame
    void Update()
    {        
        //Debug.Log(tutTex.Length);
        foreach (Touch touch in Input.touches)
        {
            startTouch = touch.position;
            Ray ray = Camera.main.ScreenPointToRay(touch.position);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {

                //if buttons in tutorial is tapped
                if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began)
                {
                    GameObject sound = GameObject.Find("AudioManager");
                    SoundManager buttonSound = sound.GetComponent<SoundManager>();


                    if (hit.collider.gameObject == prvBtn) // if prev btn is tapped
                    {
                        buttonSound.MenuButtons();

                        if (index <= 0) // if page number is == 0 / 1st page, 
                        {
                            index = 0;  // dont change page
                            prvBtn.renderer.enabled = false;
                        }

                        else
                        {
                            index--;    // else go to prev page
                            noBtn.renderer.enabled = false;
                        }

                    }

                    if (hit.collider.gameObject == nxtBtn) //if next button is tapped
                    {
                        buttonSound.MenuButtons();

                        index++; // page number +1

                        if (index > tutTex.Length - 1) // if page number exceeds 
                        {
                            FadeTransition.LoadLevel("MapScene", 0.5f, 0.5f, Color.black);
                        }

                        nxtBtn.renderer.enabled = true;
                    }

                    if (hit.collider.gameObject == skipBtn) // if skip button is tapped
                    {
                        buttonSound.MenuButtons();

                        FadeTransition.LoadLevel("MapScene", 0.5f, 0.5f, Color.black);

                        skipBtn.renderer.enabled = true;
                    }
                }
                else // if no button is tapped
                {
                    nxtBtn.renderer.enabled = false;
                    prvBtn.renderer.enabled = false;
                    skipBtn.renderer.enabled = false;
                }
            }
        }
        if (index == 0) // if page number is == 0 / 1st page, 
        {
            noBtn.renderer.enabled = true;
        }
        else if (index >= tutTex.Length - 1) // if at last page
        {
            endBtn.renderer.enabled = true;
        }
        else // if not at 1st page nor last
        {
            noBtn.renderer.enabled = false;
            endBtn.renderer.enabled = false;
        }
    }

    void OnGUI()
    {
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), tutTex[index], ScaleMode.StretchToFill);

        if (GUI.Button(SkipPos, SkipButton, new GUIStyle()))
        {
            GameObject sound = GameObject.Find("AudioManager");
            SoundManager buttonSound = sound.GetComponent<SoundManager>();

            buttonSound.MenuButtons();

            FadeTransition.LoadLevel("MainMenuScene", 0.5f, 0.5f, Color.black);

            skipBtn.renderer.enabled = true;
        }
    }
}
