﻿using UnityEngine;
using System.Collections;

public class PowerupShop : MonoBehaviour {

    public PointsCarry pointsCarry;
    public ShiokBar shiokBar;

    private bool activatedNewStall = false;
    //private bool activatedDiscount = false;
    //private bool activatedBetterQuality = false;

    //GUIContent buttonContent;
    //GUIContent closeContent;

    [SerializeField]
    public GameObject[] shopButtons;

    //public float hScrollbarValue;
    //public Vector2 scrollPosition = Vector2.zero;

    public Vector2 startTouch = Vector2.zero;

    // button to activate powerup
    public bool buttonOpenNewStall = true;

    public GameObject backgroundObj;
    public GameObject newStallObj;
    //public GameObject discountObj;
    //public GameObject qualityObj;
    public GameObject closeShopObj;
    public GameObject blurBackground;

    public GameObject newShopBtn;
    public GameObject newShopIcon;
    public GameObject newShopInfo;

    //public GameObject qualityBtn;
    //public GameObject qualityIcon;
    //public GameObject qualityInfo;

    //public GameObject discountBtn;
    //public GameObject discountIcon;
    //public GameObject discountInfo;


    public GameObject mapCheck;
    SelectedMap getMap;

    public Vector2 mouse;

    

	// Use this for initialization
	void Start () 
    {
        mapCheck = GameObject.Find("selectedMap");
        getMap = mapCheck.GetComponent<SelectedMap>();
	}

    // Update is called once per frame
    void Update()
    {
        //foreach (Touch touch in Input.touches)
        //{
        //    // returns input on screen to show input in game
        //    startTouch = touch.position;
        //    Ray ray = Camera.main.ScreenPointToRay(touch.position);
        //    RaycastHit hit;

        //    if (Physics.Raycast(ray, out hit))
        //    {
        //        if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began)
        //        {
        //            GameObject sound = GameObject.Find("AudioManager");
        //            SoundManager buttonSound = sound.GetComponent<SoundManager>();

        //            //if player selects new shop powerup
        //            if (hit.collider.gameObject == newShopBtn)
        //            {
        //                buttonSound.MenuButtons();

        //                newShopBtn.renderer.enabled = true;
        //            }

                   
        //        }
        //    }
        //}
	}

    void OnGUI()
    {
        if (getMap.getSelectedMap())
        {
            if (shiokBar.shiokMode)
            {
                buttonOpenNewStall = true;
            }
            else if (shiokBar.lvl == 2)
            {
                GameObject goToNewStall = GameObject.Find("NewStallPowerup");
                NewStallPowerup newStallPowerup = goToNewStall.GetComponent<NewStallPowerup>();

                if (buttonOpenNewStall)
                {
                    newShopBtn.renderer.enabled = true;
                }
                else
                {
                    newShopBtn.renderer.enabled = false;
                }
            }
        }
    }
}
